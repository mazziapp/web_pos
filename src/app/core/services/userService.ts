import { Injectable } from '@angular/core';
import { BehaviorSubject, ReplaySubject } from 'rxjs';
import { map, distinctUntilChanged } from 'rxjs/operators';

import { Apollo } from 'apollo-angular';
import { loginQuery, registerQuery, getRoleQuery, getUserDataQuery, updateUserData, sendRecoveryRequestQuery, recoverChangePasswordQuery, getUniversityQuery, updateFcmQuery, getClassUserByIdQuery, logoutQuery, getTiersQuery, updateUserWithoutPassQuery, changeTierQuery, updatePasswordQuery } from '../queries/user.query';
import { User } from '../models/user.model';
import { JwtService } from './jwt.service';
// import decode from 'jwt-decode';
import { variable } from '@angular/compiler/src/output/output_ast';

@Injectable()
export class UserAuthService {
    private currentUserSubject = new BehaviorSubject<User>({} as User);
    public currentUser = this.currentUserSubject.asObservable().pipe(distinctUntilChanged());

    private isAuthenticatedSubject = new ReplaySubject<boolean>(1);
    public isAuthenticated = this.isAuthenticatedSubject.asObservable();

    constructor(
        private apollo: Apollo,
        private jwtService: JwtService
    ) {

    }

    login(loginCredentials) {
        return this.apollo.query<any>({
            query: loginQuery,
            variables: {
                emailOrPhone: loginCredentials.email,
                password: loginCredentials.password
            }
        }).pipe(map(
            response => {
                // console.log(response, 'asd wdw')
                if (response.data && !response.errors && response.data.login != null) {
                    // localStorage.setItem('currentUser', JSON.stringify(response.data.login))
                    this.setAuth(response.data.login);
                }
                return response;
            }

        ));

    }

    register(registerCredentials) {
        // console.log(registerCredentials.schoolId);
        this.apollo.getClient().cache.reset();
        return this.apollo.mutate({
            mutation: registerQuery,
            variables: {
                email: registerCredentials.email,
                password: registerCredentials.password,
                firstName: registerCredentials.firstName,
                lastName: registerCredentials.lastName,
                gender: registerCredentials.gender,
                roleId: registerCredentials.roleId,
                schoolId: registerCredentials.schoolId
            }
        }).pipe(map(
            response => {
                console.log(response);
                if (response.data && !response.errors && response.data.login != null) {
                    localStorage.setItem('currentUser', JSON.stringify(response.data.login))
                } else {
                    // console.log(response.data);
                    console.log("Error");
                }
            }
        ))
    }

    getRole() {
        return this.apollo.query<any>({
            query: getRoleQuery
        }).pipe(map(
            response => {
                return response;
            }
        ))
    }

    getUniversity() {
        // this.apollo.getClient().cache.reset();
        return this.apollo.query<any>({
            query: getUniversityQuery
        }).pipe(map(
            response => {
                return response;
            }
        ))
    }

    logout(userId) {
        // remove user from local storage to log user out
        // window.localStorage.removeItem('jwtToken');
        return this.apollo.mutate({
            mutation: logoutQuery,
            variables: {
                userId: userId
            }
        }).pipe(map(
            response => {
                return response;
            }
        ))

    }

    setAuth(user: User) {
        // console.log(user, '<-------------- user');
        // console.log(user.token, '<-------------- user token');
        // window.localStorage.removeItem('semId');
        // window.localStorage.removeItem('toggleVal');
        // Save JWT sent from server in localstorage
        this.jwtService.saveToken(user.token);
        // Set current user data into observable
        this.currentUserSubject.next(user);
        // Set isAuthenticated to true
        this.isAuthenticatedSubject.next(true);

        // console.log(this.currentUserSubject, this.isAuthenticatedSubject, 'COOOOOONNNNNSS');
    }

    getCurrentUser(): User {
        return this.currentUserSubject.value;
    }

    purgeAuth() {
        // Remove JWT from localstorage
        this.jwtService.destroyToken();
        // Set current user to an empty object
        this.currentUserSubject.next({} as User);
        // Set auth status to false
        this.isAuthenticatedSubject.next(false);
    }

    populate() {
        // If JWT detected, attempt to get & store user's info
        if (this.jwtService.getToken()) {
            let token = this.jwtService.getToken();
            this.apollo.getClient().cache.reset();
            this.apollo.query<any>({
                query: getUserDataQuery,
                variables: {
                    token: token
                }
            }).subscribe(
                response => {
                    // console.log(response.data.entry);
                    console.log('populate')
                    if (response.data.entry == null) {
                        // window.localStorage.removeItem('currentUser');
                        this.purgeAuth();
                    }
                    this.setAuth(response.data.entry);
                },
                err => this.purgeAuth()
            )
        } else {
            // Remove any potential remnants of previous auth states
            this.purgeAuth();
        }
    }

    updateInformation(user: User, newPassword: String, oldPassword: String) {
        return this.apollo.mutate({
            mutation: updateUserData,
            variables: {
                userId: user._id,
                email: user.email,
                lastName: user.lastname,
                firstName: user.nickname,
                newPassword: newPassword,
                oldPassword: oldPassword
            }
        })
    }

    updateUserWithoutPass(submissionCredentials, userId){
        return this.apollo.mutate({
            mutation: updateUserWithoutPassQuery,
            variables: {
                userId: userId,
                email: submissionCredentials.email,
                nickname: submissionCredentials.nickname,
                lastname: submissionCredentials.lastname,
                schoolId: submissionCredentials.schoolId
            }
        })
    }

    sendRecoveryRequest(email) {
        return this.apollo.mutate({
            mutation: sendRecoveryRequestQuery,
            variables: {
                email: email
            }
        })
    }

    changePassword(token, password) {
        return this.apollo.mutate({
            mutation: recoverChangePasswordQuery,
            variables: {
                token: token,
                password: password
            }
        })
    }

    updateFcm(userId, token) {
        return this.apollo.mutate({
            mutation: updateFcmQuery,
            variables: {
                userId: userId,
                token: token
            }
        })
    }

    getClassUserById(classUserId) {
        return this.apollo.query<any>({
            query: getClassUserByIdQuery,
            variables: {
                userId: classUserId
            }
        }).pipe(map(
            response => {
                return response;
            }
        ))
    }

    getTiers() {
        return this.apollo.query<any>({
            query: getTiersQuery
        }).pipe(map(
            response => {
                return response;
            }
        ))
    }

    changeTier(userId, tierId){
        return this.apollo.mutate({
            mutation: changeTierQuery,
            variables: {
                userId: userId,
                tierId: tierId
            }
        }).pipe(map(
            response => {
                return response;
            }
        ))
    }

    updatePassword(userId, submissionCredentials){
        return this.apollo.mutate({
            mutation: updatePasswordQuery,
            variables: {
                userId: userId,
                oldPassword: submissionCredentials.oldPassword,
                newPassword: submissionCredentials.newPassword
            }
        }).pipe(map(
            response => {
                return response;
            }
        ))
    }

}