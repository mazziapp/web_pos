import { Component, OnInit } from '@angular/core';
// import { DashboardSidebarComponent } from 'src/app/shared';
import { GlobalEventsManager } from 'src/app/core/services/global/global-event-manager.service';
import { Subscription } from 'rxjs';
import { Router, NavigationStart, ActivatedRoute } from '@angular/router';
import { MainService } from 'src/app/core/services/main.service';
import { User } from 'src/app/core/models/user.model';
import { UserAuthService } from 'src/app/core/services/userService';
import { CorporateTier, CorporateMemberList } from 'src/app/core/models/main.model';

export let browserRefresh = false;

@Component({
  selector: 'app-corporate',
  templateUrl: './corporate.component.html',
  styleUrls: ['./corporate.component.css']
})
export class CorporateComponent implements OnInit {
  currentUser: User;
  currentUserRole;
  currentCorporate: any;
  currentCorporateTier: CorporateTier;
  corporateMemberList: CorporateMemberList[] = [];

  currentCorporateId;

  // currentSideBarMenu: Menu[] = [];

  constructor(
    private userService: UserAuthService,
    private router: Router,
    private route: ActivatedRoute,
    private mainService: MainService,
    private globalEventManager: GlobalEventsManager
  ) {
    this.userService.currentUser.subscribe(
      (userData) => {
        this.currentUser = userData
      }
    );

    this.mainService.currentRole.subscribe(
      (userRole) => {
        this.currentUserRole = userRole;
        console.log(this.currentUserRole, 'currentUserRole')
      }
    )
  }

  ngOnInit() {
    this.currentCorporateId = this.route.snapshot.paramMap.get('_id');
    this.getCorporateMemberList(this.currentCorporateId);
    console.log(this.currentCorporateId, 'corpID')
    // this.getCorporate();
    this.getSingleCorporateMemberList();
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.globalEventManager.showSideMenu.emit([]);
    // this.subscription.unsubscribe();
  }

  getCorporateMemberList(corporateId) {
    this.mainService.getCorporateMemberList(this.currentUser._id, corporateId)
      .subscribe((response) => {
        if (response.data.corporateMemberList.error == false) {
          this.corporateMemberList = response.data.corporateMemberList.result;
        }
      })
  }

  getCorporate() {
    this.mainService.getCorporate(this.currentUser._id, this.currentCorporateId)
      .subscribe((response) => {
        if (response.data.corporate.error == false) {
          console.log(response, 'responsee')
          this.currentCorporate = response.data.corporate.result;
          // this.getCorporateTier(response.data.corporate.result.corporateTierId);
        }
      })
  }

  getSingleCorporateMemberList() {
    this.mainService.getSingleCorporateMemberList(this.currentUser._id, this.currentCorporateId)
      .subscribe((response) => {
        if (response.data.singleCorporateMemberList.error == false) {
          this.currentCorporate = response.data.singleCorporateMemberList.result;
          this.mainService.setRole(this.currentCorporate.corporateMemberPositionId.name);
          console.log(this.currentCorporate, 'currentCorporate')
          // this.getCorporateTier(response.data.singleCorporateMemberList.result.corporateId.corporateTierId);
          // console.log(response, 'response');
        }
      })
  }
  cu(cu: any) {
    throw new Error("Method not implemented.");
  }

  // getCorporateTier(corporateTierId) {
  //   this.currentSideBarMenu = [];
  //   this.mainService.getCorporateTier(corporateTierId)
  //     .subscribe((response) => {
  //       if (response.data.corporateTier.error == false) {
  //         this.currentCorporateTier = response.data.corporateTier.result;
  //         if (this.currentCorporateTier.access.mclass.access == true) {
  //           this.currentSideBarMenu.push({
  //             name: 'М Класс',
  //             icon: 'fa fa-square',
  //             routerLink: this.currentCorporateId + '/class',
  //             subMenu: []
  //           })
  //         }

  //         if (this.currentCorporateTier.access.pos.access == true) {
  //           this.currentSideBarMenu.push({
  //             name: 'ПОС Систем',
  //             icon: 'fa fa-square',
  //             routerLink: this.currentCorporateId + '/p',
  //             subMenu: [
  //               {
  //                 name: 'Эхлэл',
  //                 icon: '',
  //                 routerLink: this.currentCorporateId + '/p/home'
  //               },
  //               {
  //                 name: 'Бараа',
  //                 icon: '',
  //                 routerLink: this.currentCorporateId + '/p/products'
  //               },
  //               {
  //                 name: 'Барааны төрөл',
  //                 icon: '',
  //                 routerLink: this.currentCorporateId + '/p/category'
  //               },
  //               {
  //                 name: 'Гүйлгээ',
  //                 icon: '',
  //                 routerLink: this.currentCorporateId + '/p/bills'
  //               }
  //             ]
  //           })
  //         }

  //         this.currentSideBarMenu.push({
  //           name: 'Хэрэглэгчид',
  //           icon: 'fa fa-users',
  //           routerLink: this.currentCorporateId + '/members',
  //           subMenu: []
  //         })

  //         this.globalEventManager.showSideMenu.emit(this.currentSideBarMenu);
  //       }
  //     })
  // }

}