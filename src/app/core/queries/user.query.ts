import gql from 'graphql-tag';

//TeacherAuth

export const loginQuery = gql`query login($emailOrPhone: String!, $password: String!){
    login(emailOrPhone: $emailOrPhone, password: $password){
      _id
      region
      image
      nickname
      lastname
      surname
      email
      gender
      phone
      registerId
      state
      online_at
      created_at
      is_confirmed
      device_id
      fcm_token
      token
      subUser{
        _id
        userId
        schoolId
        storageLimit
        tierId
        path
        studentIdCode
        subscriptionEndDate
        roleId
        notification{
          mchatNotificationCount
          mclassNotificationCount
          lastDeleted
        }
      }
    }
  }
`;

export const logoutQuery = gql`mutation logout($userId: String!){
    logout(userId: $userId){
      error
      message
    }
  }
`;

export const registerQuery = gql`mutation register($email: String!, $password: String!, $firstName: String!, $lastName: String!, $gender: String!, $roleId: String!, $schoolId: String!){
    register(email: $email, password: $password, firstName: $firstName, lastName: $lastName, gender: $gender, roleId: $roleId, schoolId: $schoolId){
      error
      message
    }
  }
`;

export const getRoleQuery = gql`query role{
    role{
      _id
      name
      priority
    }
  }
`;

export const getUniversityQuery = gql`query school{
    school {
      _id
      name
    }
  }
`;

export const getUserDataQuery = gql`query entry($token: String!){
    entry(token: $token){
      _id
      region
      image
      nickname
      lastname
      surname
      email
      gender
      phone
      registerId
      state
      online_at
      created_at
      is_confirmed
      device_id
      fcm_token
      token
      subUser{
        _id
        userId
        schoolId
        storageLimit
        tierId
        path
        studentIdCode
        subscriptionEndDate
        roleId
        notification{
          mchatNotificationCount
          mclassNotificationCount
          lastDeleted
        }
      }
    }
  }
`;
////////////////////////////////////

export const updateUserData = gql`
mutation updateUserdata($userId : String! , $email : String , $firstName : String , $lastName : String , $newPassword : String , $oldPassword : String!){
  updateUser(userId : $userId , email : $email , firstName : $firstName , lastName : $lastName , newPassword : $newPassword , oldPassword : $oldPassword){
    _id
    email
    firstName
    lastName
    password
    gender
    roleId
  }
}
`;

export const updateUserWithoutPassQuery = gql`
  mutation updateUserWithoutPass($userId: String!, $email: String!, $nickname: String!, $lastname: String!, $schoolId: String!){
    updateUserWithoutPass(userId: $userId, email: $email, nickname: $nickname, lastname: $lastname, schoolId: $schoolId){
      error
      message
    }
  }
`;

export const sendRecoveryRequestQuery = gql`
 mutation recoverPasswordSendEmail($email : String!){
   recoverPasswordSendEmail(email : $email)
 }
`;

export const recoverChangePasswordQuery = gql`
 mutation recoverPasswordChangePassword($token : String! , $password : String!){
   recoverPasswordChangePassword(token : $token , password : $password)
 }
`;


export const updateFcmQuery = gql`
 mutation udpateFcm($userId: String!, $token: String!){
   updateFcm(userId: $userId, token: $token){
     error
     message
   }
 }
`;

// -------------------------------

export const getClassUserByIdQuery = gql`query mazziUserById($userId: String!){
    mazziUserById(userId: $userId){
      _id
      region
      image
      nickname
      lastname
      surname
      birthday
      email
      gender
      phone
      state
      online_at
      created_at 
    }
  }
`;

export const getTiersQuery = gql`query tiers{
  tiers{
    _id
    name
    key
    timeLimit
    price
    storageSpace
    module
  }
}
`;

export const changeTierQuery = gql`mutation changeTier($userId: String!, $tierId: String!){
    changeTier(userId: $userId, tierId: $tierId){
      error
      message
    }
  }
`;

export const updatePasswordQuery = gql`mutation updatePassword($userId: String!, $oldPassword: String!, $newPassword: String!){
  updatePassword(userId: $userId, oldPassword: $oldPassword, newPassword: $newPassword){
    error
    message
  }
}`