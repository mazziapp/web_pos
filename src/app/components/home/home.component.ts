import { Component, OnInit, ViewChild } from '@angular/core';
import { MainService } from 'src/app/core/services/main.service';
import { GlobalEventsManager } from 'src/app/core/services/global/global-event-manager.service';
import { User } from 'src/app/core/models/user.model';
import { UserAuthService } from 'src/app/core/services/userService';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  currentUser: User;
  corporates: any[] = [];

  showTierSection = false;


  constructor(
    private userService: UserAuthService,
    private mainService: MainService,
    private globalEventsManager: GlobalEventsManager
  ) {
    this.userService.currentUser.subscribe(
      (userData) => {
        this.currentUser = userData
      }
    );
  }

  ngOnInit() {
    
  }

  
}
