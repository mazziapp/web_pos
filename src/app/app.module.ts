import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GraphQLModule } from './graphql.module';
import { HttpClientModule } from '@angular/common/http';
import { ApolloModule, APOLLO_OPTIONS } from "apollo-angular";
import { HttpLinkModule, HttpLink } from "apollo-angular-link-http";
import { InMemoryCache } from "apollo-cache-inmemory";
import { PosItemComponent,CheckoutDialog, MazziCardDialog } from './components/pos-item/pos-item.component';
import { MatMenuModule, MatIconModule, MatButtonModule, MatCardModule, MatInputModule, MatFormFieldModule, MatGridListModule, MatTabsModule, MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatDialogModule, MatTable, MatTableModule, MatButtonToggleModule, MatPaginatorModule} from '@angular/material';
import { TicketComponent} from './components/ticket/ticket.component';
import { PosService } from './core/services/pos.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UserLoginComponent } from './components/auth/user-login/user-login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserAuthService } from './core/services/userService';
import { JwtService } from './core/services/jwt.service';
import { HomeComponent } from './components/home/home.component';
import { RouterModule } from '@angular/router';
import { NoAuthGuard } from './components/auth/no-auth-guard.service';
import { CorporateComponent } from './components/corporate/corporate.component';
import { MainService } from './core/services/main.service';
import { GlobalEventsManager } from './core/services/global/global-event-manager.service';
import { NotFoundComponent } from './shared/not-found/not-found/not-found.component';
import { DashboardHeaderComponent } from './shared/layouts/dashboard-header/dashboard-header.component';
import { DashboardFooterComponent } from './shared/layouts/dashboard-footer/dashboard-footer.component';
import * as Hammer from 'hammerjs';
import { HammerGestureConfig, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { AuthGuard } from './core/services/auth-guard.service';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { ApolloLink } from 'apollo-link';

export class MyHammerConfig extends HammerGestureConfig  {
  overrides = <any>{
      // override hammerjs default configuration
      'swipe': { direction: Hammer.DIRECTION_ALL  }
  }
}

@NgModule({
  declarations: [
    AppComponent,
    PosItemComponent,
    TicketComponent,
    UserLoginComponent,
    HomeComponent,
    CorporateComponent,
    NotFoundComponent,
    DashboardFooterComponent,
    DashboardHeaderComponent,
    CheckoutDialog,
    MazziCardDialog,
    WelcomeComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    AppRoutingModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatMenuModule,
    MatInputModule,
    MatFormFieldModule,
    MatGridListModule,
    MatTabsModule,
    MatDialogModule,
    MatTableModule,
    MatButtonToggleModule,
    MatPaginatorModule,
    MatButtonModule,
    GraphQLModule,
    HttpClientModule,
    ApolloModule,
    HttpLinkModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    SweetAlert2Module.forRoot({
      buttonsStyling: false,
      customClass: 'modal-content',
      confirmButtonClass: 'btn btn-primary',
      cancelButtonClass: 'btn'
  }),
    RouterModule.forRoot([
      {
        path: 'login',
        component: UserLoginComponent
      },
      {
        path: 'home',
        component: HomeComponent
      }
    ])
  ],
  
  providers: [
  {
    provide: HAMMER_GESTURE_CONFIG, 
    useClass: MyHammerConfig 
  },
  PosService,
  UserAuthService,
  JwtService,
  NoAuthGuard,
  AuthGuard,
  MainService,
  GlobalEventsManager],
  entryComponents: [CheckoutDialog, MazziCardDialog],
  bootstrap: [AppComponent]
})

export class AppModule { }
