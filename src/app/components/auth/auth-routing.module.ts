import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserLoginComponent } from './user-login/user-login.component';
import { NoAuthGuard } from './no-auth-guard.service';

const routes: Routes = [
    {
        path: 'login',
        component: UserLoginComponent,
        // canActivate: [NoAuthGuard]
    },
]

@NgModule({
    imports: [
      RouterModule.forChild(routes)
    ],
    exports: [RouterModule]
  })
export class AuthRoutingModule {}