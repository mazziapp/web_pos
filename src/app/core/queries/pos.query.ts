import gql from 'graphql-tag';

export const posItemsQuery = gql`
  query posItems($posItemTypeId:String!,$corporateId:String!){
    posItems(posItemTypeId:$posItemTypeId,corporateId:$corporateId) {
       error
       status
       message
         result {
           _id
           name
           price
           createdDate
           corporateId
           posItemTypeId
         }
     }
  }
`;

export const insertBillMutation = gql`
  mutation insertBill($userId:String!,$corporateId:String!,$inputBillItems:[InputBillItem],$purchaseType:String!,$subjectUserId:String!,$posId:String!){
    insertBill(userId:$userId, corporateId:$corporateId,InputBillItems:$inputBillItems,purchaseType:$purchaseType,subjectUserId:$subjectUserId,posId:$posId){
      error
      status
      message
      result{
        _id
        corporateId
        userId
        createdDate
        totalPrice
        html
      }
    }
  }`
  ;

  export const dailyInsertPosReportMutation = gql`
      mutation dailyInsertPosReport($userId:String!,$corporateId:String!,$posId:String!){
        dailyInsertPosReport(userId:$userId,corporateId:$corporateId,posId:$posId){
          error
          status
          message
          result{
            _id
            corporateId
            totalSoldPrice
            totalSoldItems
            posItems{
              posItem{
                posItemId
                name
                price
                posItemTypeId
              }
              quantity
              totalPrice
            }
            createdDate
            posId
          }
        }
      }
      `
      

  export const fullPosItemsQuery = gql`
    query fullPosItems($userId:String!,$corporateId:String!){
        fullPosItems(userId:$userId,corporateId:$corporateId){
            error
            status
            message
            result{
                _id
                name
                price
                createdDate
                corporateId
                posItemTypeId
            }
        }
    }
    `

  export const posItemTypesQuery = gql`
    query posItemTypes($corporateId:String!){
        posItemTypes(corporateId:$corporateId){
          error
          status
          message
          result{
            _id
            name
            corporateId
            createdDate
          }
        }
    }
    `

  export const assignedUsersPosItems = gql`
    query assignedUsersPosItems($userId:String!,$corporateId:String!){
      assignedUsersPosItems(userId:$userId,corporateId:$corporateId){
        error
        status
        message
        result{
          _id
          name
          price
          createdDate
          corporateId
          posId
          posItemTypeId
        }
      }
    }
  `

  export const assignedUsersPos = gql`
    query assignedUsersPos($userId:String!,$corporateId:String!){
      assignedUsersPos(userId:$userId,corporateId:$corporateId){
        error
        status
        message
        result{
          _id
          name
          corporateId
          membersId
          createdDate
          paymentTypes{
            card
            cash
            mazziCard
          }
        }
      }
    }
  `

  export const posesPosItems = gql`
    query posesPosItems($userId:String!,$posId:String!){
      posesPosItems(userId:$userId,posId:$posId){
        error
        status
        message
        result{
          _id
          name
          purchaseType
          price
          createdDate
          corporateId
          posId
          posItemTypeId
        }
      }
    }
  `

  export const corporateMemberListByCardId = gql`
    query corporateMemberListByCardId($cardId:String!){
      corporateMemberListByCardId(cardId:$cardId){
        error
        status
        message
        result{
          _id
          userId
          corporateMemberPositionId
          createdDate
          updatedDate
          corporateId
          validUntil
          cardId
        }
      }
    }
  `

    