import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { PosItem } from 'src/app/core/models/posItem';

@Injectable()
export class PosService {

  private posId = '';
  private posIdSource = new BehaviorSubject<string>(this.posId);

  private purchaseType;
  private purchaseTypeSource = new BehaviorSubject<any>(this.purchaseType);

  private ticket = TICKET;
  private ticketSource = new BehaviorSubject<PosItem[]>(this.ticket);

  private cartTotal = 0;
  private cartTotalSource = new BehaviorSubject<number>(this.cartTotal);

  private cartNumItems = 0;
  private cartNumSource = new BehaviorSubject<number>(this.cartNumItems);


  currentTicket = this.ticketSource.asObservable();
  currentTotal = this.cartTotalSource.asObservable();
  currentCartNum = this.cartNumSource.asObservable();

  currentPosId = this.posIdSource.asObservable();
  currentPurchaseType = this.purchaseTypeSource.asObservable();

  constructor() { }

  changeTicket(ticket: PosItem[]) {
    this.ticketSource.next(ticket);
  }

  updateTotal(total: number) {
    this.cartTotalSource.next(total);
  }

  updateNumItems(num: number) {
    this.cartNumSource.next(num);
  }

  updatePosId(id: string){
    this.posIdSource.next(id);
  }

  updatePurchaseType(purchaseType: any){
    this.purchaseTypeSource.next(purchaseType);
  }

}

// Demo content
const TICKET: PosItem[] = [
];
