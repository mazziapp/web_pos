import { Component, OnInit, EventEmitter, Output, Inject } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { PosItem } from 'src/app/core/models/posItem';
import { PosService } from 'src/app/core/services/pos.service';
import gql from 'graphql-tag';
import { MatTabsModule, MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { fullPosItemsQuery, posItemsQuery, posItemTypesQuery, dailyInsertPosReportMutation, insertBillMutation, assignedUsersPosItems, posesPosItems, corporateMemberListByCardId } from 'src/app/core/queries/pos.query';
import { PosItemType } from 'src/app/core/models/posItemType';
import { trigger, transition, animate, keyframes } from '@angular/animations';
import * as kf from './keyframes';
import { User } from 'src/app/core/models/user.model';
import { UserAuthService } from 'src/app/core/services/userService';
import Swal from 'sweetalert2';
import { Router, ActivatedRoute } from '@angular/router';
import { DialogData } from '../ticket/ticket.component';
import { ViewEncapsulation } from '@angular/compiler/src/core';
import { swalDefaultsProvider } from '@sweetalert2/ngx-sweetalert2/di';
declare var window: any;

export interface DialogData {
  type: any[];
}
@Component({
  selector: 'app-pos',
  templateUrl: './pos-item.component.html',
  styleUrls: ['./pos-item.component.css'],
  animations: [
    trigger('cardAnimator', [
      transition('* => swing', animate(1000, keyframes(kf.swing))),
    ])
  ],
  encapsulation: 2

})
export class PosItemComponent implements OnInit {
  currentUser: User;
  corporateId = "5ccaa08ab545031b446629af";
  userId = "5c4936057565dd0012a958f6";
  cardUserId = "";
  userCardId;
  posItems: PosItem[] = [];
  loading = true;
  error: any;
  products = [];
  productTypes: any[];
  currentProducts: any[];
  ticket: PosItem[];
  cartTotal = 0;
  cartNumItems = 0;
  posId;
  items;
  currentCorporateId;
  inputBillItems: any[] = [];
  type: any[] = ['cash', 'card', 'mazziCard'];
  paymentType: any;
  currentPaymentType: any[] = [true, true, true];
  alert = Swal;

  constructor(
    private apollo: Apollo,
    private ticketSync: PosService,
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserAuthService,
    public dialog: MatDialog,
  ) {
    this.userService.currentUser.subscribe(
      (userData) => {
        this.currentUser = userData;
      }
    );

    // this.ticketSync.currentPosId.subscribe(posId => this.posId = posId);
    this.ticketSync.currentPurchaseType.subscribe(paymentType => this.paymentType = paymentType);
    try {
      this.currentPaymentType[0] = this.paymentType.cash;
      this.currentPaymentType[1] = this.paymentType.card;
      this.currentPaymentType[2] = this.paymentType.mazziCard;
    } catch (error) {
      console.log(error);
    }

  }

  ngOnInit() {
    // this.userService.currentUser.subscribe(
    //   (userData) => {
    //     this.currentUser = userData;
    //   }
    // );
    this.ticketSync.currentTicket.subscribe(data => this.ticket = data);
    this.ticketSync.currentTotal.subscribe(total => this.cartTotal = total);
    this.ticketSync.currentCartNum.subscribe(num => this.cartNumItems);
    // this.ticketSync.currentPosId.subscribe(posId => this.posId = posId);
    this.getPosItemTypes();
    // this.getPosesPosItems();
    this.currentCorporateId = this.route.snapshot.paramMap.get('_id');
    this.posId = this.route.snapshot.paramMap.get('id2');

    window.onCalledCsharp = (data) => {
      this.alert.close();

      // alert('Data from c#: ' + data);
      this.userCardId = data;

      if (this.userCardId != null) {
        this.getCardUserId(data);
      }
      else {
        this.openCardDialog();
      }
    };
  }

  animationState: string;

  startAnimation(state) {
    console.log(state)
    if (!this.animationState) {
      this.animationState = state;
    }
  }

  resetAnimationState() {
    this.animationState = '';
  }

  nfcCall() {
    window.external.nfcConnect("hello c# i am web");
  }

  addToCheck(item: PosItem) {
    // If the item already exists, add 1 to quantity
    //jani commented
    // if (this.ticket.includes(item)) {
    //   this.ticket[this.ticket.indexOf(item)].quantity += 1;
    // } else {
    //   item.quantity = 1;
    //   this.ticket.push(item);
    // }

    //jani added
    if (this.ticket.length === 0) {
      item.quantity = 1;
      this.ticket.push(item);
    } else {
      if (this.includes(item)) {
        this.ticket[this.ticket.indexOf(item)].quantity += 1;
      } else {
        item.quantity = 1;
        this.ticket.push(item);
      }
    }
    this.calculateTotal();
  }

  includes(value) {
    var returnValue = false;
    var pos = this.ticket.indexOf(value);
    if (pos >= 0) {
      returnValue = true;
    }
    return returnValue;
  }

  getPosItems(itemType, index) {
    this.currentCorporateId = this.route.snapshot.paramMap.get('_id');

    this.apollo
      .query<any>({
        query: posItemsQuery,
        variables: {
          posItemTypeId: itemType,
          corporateId: this.currentCorporateId
        }
      })
      .subscribe(result => {
        this.products[index] = result.data && result.data.posItems.result;
        this.loading = result.loading;
        this.error = result.errors;
        this.currentProducts = this.products[0];
      });
  }

  getPosesPosItems() {
    console.log("currentPosId = " + this.posId);
    this.apollo
      .query<any>({
        query: posesPosItems,
        variables: {
          userId: this.currentUser._id,
          posId: this.posId
        }
      })
      .subscribe(result => {
        this.posItems = result.data && result.data.posesPosItems.result;
        console.log("posesPosItems = ", result.data);
        this.setProducts();
      })
  }

  getPosItemTypes() {
    this.currentCorporateId = this.route.snapshot.paramMap.get('_id');
    this.apollo
      .query<any>({
        query: posItemTypesQuery,
        variables: {
          corporateId: this.currentCorporateId
        }
      })
      .subscribe(({ data }) => {
        this.productTypes = data && data.posItemTypes.result;
        console.log('got data = ' + data);

        // this.loading = result.loading;
        // this.error = result.errors;
        // for(let i = 0; i < this.productTypes.length; i++){
        //   console.log('posItemType = ' + this.productTypes[i].name);
        //   this.getPosItems(this.productTypes[i]._id,i);
        // }
        // this.currentProducts = this.products[0];
        this.getPosesPosItems();
      })
  }

  setProducts() {
    for (let j = 0; j < this.productTypes.length; j++) {
      this.products[j] = [];
    }

    for (let i = 0; i < this.posItems.length; i++) {
      for (let j = 0; j < this.productTypes.length; j++) {
        if (this.posItems[i].posItemTypeId === this.productTypes[j]._id) {
          this.products[j].push(this.posItems[i]);
        }
      }
    }
    this.currentProducts = this.products[0];

  }

  dailyReport() {
    this.currentCorporateId = this.route.snapshot.paramMap.get('_id');
    console.log("corporateId = " + this.currentCorporateId + " userId = " + this.currentUser._id);
    this.apollo
      .mutate<any>({
        mutation: dailyInsertPosReportMutation,
        variables: {
          corporateId: this.currentCorporateId,
          userId: this.currentUser._id,
          posId: this.posId
        }
      })
      .subscribe(({ data }) => {
        console.log('dailyReport', data);
        Swal.fire('Гүйлгээ амжилттай хаагдлаа', '', 'success');
      }, (error) => {
        console.log('failed', error);
      });
  }

  logout() {
    console.log("logout called");
    this.clearCart();
    this.userService.logout(this.currentUser._id).subscribe(response => {
      if (response.data.logout.error == false) {
        this.userService.purgeAuth();
        this.router.navigateByUrl('/login');
      }
    })
  }

  calculateTotal() {
    let total = 0;
    let cartitems = 0;
    // Multiply item price by item quantity, add to total
    this.ticket.forEach(function (item: PosItem) {
      total += (item.price * item.quantity);
      cartitems += item.quantity;
    });
    this.cartTotal = total;
    this.cartNumItems = cartitems;
    // Sync total with ticketSync service.
    this.ticketSync.updateNumItems(this.cartNumItems);
    this.ticketSync.updateTotal(this.cartTotal);
  }

  // Remove all items from cart
  clearCart() {
    console.log('clear called');
    // this.inputBillItems.length = 0;
    this.inputBillItems = [];
    // Reduce back to initial quantity (1 vs 0 for re-add)
    for(let i = 0; i < this.ticket.length; i++) {
      this.ticket[i].quantity = 1;
    }
    // this.ticket.forEach(function (item: PosItem) {
    //   item.quantity = 1;
    // });
    // Empty local ticket variable then sync
    this.ticket = [];
    // this.ticketSync.changeTicket([]);
    // this.ticket.length = 0;
    this.syncTicket();
    this.calculateTotal();
  }

  syncTicket() {
    this.ticketSync.changeTicket(this.ticket);
    document.getElementById('clearTicket-1').click();
  }

  clearTicket() {
    console.log('cleared');
  }

  checkout(type, userId) {
    console.log("type=", type);
    console.log("userId=", userId);

    if (this.ticket.length > 0) {
      for (let i = 0; i < this.ticket.length; i++) {
        this.inputBillItems.push({ posItemId: this.ticket[i]._id, quantity: this.ticket[i].quantity });
      }
      this.insertBill(this.inputBillItems, type, userId);
    }
  }

  insertBill(inputBillItems, type, userId) {
    this.currentCorporateId = this.route.snapshot.paramMap.get('_id');

    this.apollo.mutate({
      mutation: insertBillMutation,
      variables: {
        userId: this.currentUser._id,
        corporateId: this.currentCorporateId,
        inputBillItems: inputBillItems,
        purchaseType: type,
        subjectUserId: userId,
        posId: this.posId
      }
    }).subscribe(({ data }) => {
      console.log('got dataaaaa ', data);
      if (data.insertBill.status == 'ok') {
        const Toast = Swal.mixin({
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 2500
        })

        Toast.fire({
          type: 'success',
          title: 'Амжилттай зарагдлаа'
        })

        this.clearCart();
        // Swal.fire('Амжилттай зарагдлаа','','success');
      }
      else {
        Swal.fire(data.insertBill.message, '', 'warning');
      }
    }, (error) => {
      console.log('failed', error);
    });
  }

  changeProducts(index) {
    console.log("changeProducts called");
    this.currentProducts = this.products[index];
  }

  getCardUserId(cardId) {
    this.apollo.query<any>({
      query: corporateMemberListByCardId,
      variables: {
        cardId: cardId
      }
    }).subscribe(({ data }) => {
      // cardUserId = data.result.userId;
      console.log('checkUserId ', data.corporateMemberListByCardId);
      if (data.corporateMemberListByCardId.result != null) {
        this.cardUserId = data.corporateMemberListByCardId.result.userId;
        this.checkout("mazziCard", this.cardUserId);
      }
      else {
        Swal.fire('Картын дугаар буруу байна!', '', 'warning');
      }

    }, (error) => {
      console.log('failed', error);
    });
  }

  showDialog() {
    this.alert.fire({
      title: 'Та картаа уншуулна уу',
      showConfirmButton: true
    })
    try {
      this.nfcCall();
    } catch{
      this.openCardDialog();
    }

    // let timerInterval
    //     Swal.fire({
    //       title: 'Та картаа уншуулна уу?',
    //       timer: 1500,
    //       onBeforeOpen: () => {
    //         Swal.showLoading()
    //         timerInterval = setInterval(() => {
    //           Swal.getContent().querySelector('strong')

    //         }, 100)
    //       },
    //       onClose: () => {
    //         clearInterval(timerInterval)
    //       }
    //     }).then((result) => {
    //       if (
    //         /* Read more about handling dismissals below */
    //         result.dismiss === Swal.DismissReason.timer
    //       ) {
    //         console.log('I was closed by the timer')

    //       }
    //     })
  }

  openDialog(): void {
    if(this.ticket.length > 0){
      const dialogRef = this.dialog.open(CheckoutDialog, {
        width: '500px',
        data: {
          type: this.type,
          paymentType: this.currentPaymentType
        }
      });
  
      dialogRef.afterClosed().subscribe(result => {
        if (result == 'mazziCard') {
          this.showDialog();
  
          // this.openCardDialog();
        }
        else {
          if (result != undefined) {
            this.checkout(result, "");
          }
        }
      });
    }

  }

  openCardDialog(): void {
    this.alert.close();
    console.log("mazziCardId called");
    const dialogRef = this.dialog.open(MazziCardDialog, {
      width: '500px',
      data: {
        type: this.type,
        paymentType: this.currentPaymentType
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log("mazziCardId = " + result);
      if (result != null) {
        this.getCardUserId(result);
      }
    });
  }

}


@Component({
  selector: 'app-checkout-dialog',
  templateUrl: './checkout.dialog.html',
})
export class CheckoutDialog implements OnInit {
  ngOnInit(): void {
    // throw new Error("Method not implemented.");
  }

  constructor(
    public dialogRef: MatDialogRef<CheckoutDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  onNoClick(): void {
    this.dialogRef.close();
    console.log('here!')
  }
}

@Component({
  selector: 'app-mazzicard-dialog',
  templateUrl: './mazzi_card.dialog.html',
})
export class MazziCardDialog {

  constructor(
    public dialogRef: MatDialogRef<MazziCardDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
