import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { UserLoginComponent } from './components/auth/user-login/user-login.component';
import { NoAuthGuard } from './components/auth/no-auth-guard.service';
import { NotFoundComponent } from './shared/not-found/not-found/not-found.component';
import { CanActivate } from '@angular/router/src/utils/preactivation';
import { AuthGuard } from './core/services/auth-guard.service';
import { WelcomeComponent } from './components/welcome/welcome.component';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: UserLoginComponent},
  { path: 'welcome', component: WelcomeComponent,canActivate:[AuthGuard]},
  // { path: 'home', component: HomeComponent, canActivate:[AuthGuard]},
  { path: 'not-found', component: NotFoundComponent },
  // { path: ':_id',component: HomeComponent},
  // { path: ':_id/id2', component: HomeComponent},
  { path: 'HomeComponent', component: HomeComponent},
  { path: '**', component: WelcomeComponent,canActivate:[AuthGuard] },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
