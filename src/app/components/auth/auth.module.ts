import { NgModule } from '@angular/core';
import { AuthRoutingModule } from './auth-routing.module';
import { NoAuthGuard } from './no-auth-guard.service';

@NgModule({
  imports: [
    AuthRoutingModule
  ],
  declarations: [
  ],

  providers: [
    NoAuthGuard
  ]
})
export class AuthModule {}