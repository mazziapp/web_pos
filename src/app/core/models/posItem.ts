export interface PosItem{
  _id?: string;
  name: string;
  purchaseType: string;
  price: number;
  createdDate: number;
  corporateId: string;
  posId: string;
  posItemTypeId: string;
  quantity?: number;
}
