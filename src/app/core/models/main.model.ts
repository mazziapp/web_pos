/* TIER, PLAN, PRICE MODEL */

export interface CorporateTier {
    _id: string,
    name: string,
    access: CorporateTierAccess,
    price: number,
    duration: number
}

export interface CorporateTierAccess {
    mclass: CorporateTierAccessMclass,
    pos: CorporateTierAccessPos
}

export interface CorporateTierAccessMclass {
    access: boolean,
    spec: CorporateTierAccessMclassSpec
}

export interface CorporateTierAccessPos {
    access: boolean
}

export interface CorporateTierAccessMclassSpec {
    classLimit: number
}

/*---------------------------------------*/

/* CORPORATE */

export interface Corporate {
    _id: string
    corporateCode: string
    ownerId: string
    corporateTierId: string
    corporateTierValidUntil: number
    name: string
    createdDate: number
}

export interface CorporateMemberList {
    _id: string
    userId: {
        _id: string
        region: string
        image: string
        nickname: string
        lastname: string
        surname: string
        birthday: number
        email: string
        gender: string
        phone: string
        state: number
        online_at: number
        created_at: number
    }
    corporateMemberPositionId: string
    createdDate: number
    updatedDate: number
    corporateId: string
}

export interface CorporateMemberPositions {
    _id: string
    name: string
    positionKey: string
}

/*---------------------------------------*/