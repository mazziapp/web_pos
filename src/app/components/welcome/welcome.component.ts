

import { Component, OnInit, OnChanges } from '@angular/core';
import { User } from 'src/app/core/models/user.model';
import { UserAuthService } from 'src/app/core/services/userService';
import { MainService } from 'src/app/core/services/main.service';
import { GlobalEventsManager } from 'src/app/core/services/global/global-event-manager.service';
import {Apollo} from 'apollo-angular';
import { assignedUsersPos } from 'src/app/core/queries/pos.query';
import { PosService } from 'src/app/core/services/pos.service';
import { Router } from '@angular/router';
import { HomeComponent } from '../home/home.component';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit, OnChanges {
  ngOnChanges(changes: import("@angular/core").SimpleChanges): void {
    throw new Error("Method not implemented.");
  }
  currentUser: User;
  corporates: any[] = [];
  poses: any[] = [];
  showTierSection = false;
  selectedCorporateName;
  selectedCorporateId;
  posId;
  purchaseType: any[];

  constructor(
    private apollo: Apollo,
    private posSync: PosService,
    private userService: UserAuthService,
    private mainService: MainService,
    private router: Router,
    private globalEventsManager: GlobalEventsManager
    ) {
      this.userService.currentUser.subscribe(
        (userData) => {
          this.currentUser = userData;
        }
      );

     }

  ngOnInit() {
    this.posSync.currentPosId.subscribe(posId => this.posId = posId);
    this.posSync.currentPurchaseType.subscribe(purchaseType => this.purchaseType = purchaseType);
    // this.getOwnerCorporate();
    this.getUsersCorporateMemberList();
  };

  selectCorporate(value) {
    let selectedCorporate = this.corporates.find(x => x.corporateId._id == value);
    this.getAssignedPoses(selectedCorporate.corporateId._id, selectedCorporate.corporateId.name);
    // console.log(selectedCorporate, 'selected')
  }

  getOwnerCorporate() {
    console.log("currentUserId = ", this.currentUser);
    this.mainService.getOwnedCorporate(this.currentUser._id)
      .subscribe(response => {
        if(response.data.ownedCorporates.error == false) {
          this.corporates = response.data.ownedCorporates.result;
          if(this.corporates.length == 0) {
            this.showTierSection = true;
          }
        }
      })
  }

  getUsersCorporateMemberList() {
    this.mainService.getUsersCorporateMemberList(this.currentUser._id)
      .subscribe(response => {
        if(response.data.usersCorporateMemberList.error == false) {
          this.corporates = response.data.usersCorporateMemberList.result;
          console.log(this.corporates, 'corporates')
          if(this.corporates.length == 0) {
            this.showTierSection = true;
          }
        }
      })
  }

  setRole(corporateId) {
    console.log(corporateId, 'corpId')
    let corporateIndex = this.corporates.findIndex(item => item.corporateId._id == corporateId);
    this.mainService.setRole(this.corporates[corporateIndex].corporateMemberPositionId.name);
  }

  passSelectedCorporate(corporate) {
    console.log(corporate, 'corp')
    this.globalEventsManager.passSelectedCorporate.emit(corporate);
  }


  getUpdatedCorporate(updatedCorporate) {
    let corporateIndex = this.corporates.findIndex(x => x._id == updatedCorporate._id);
    this.corporates[corporateIndex] = updatedCorporate;
  }

  getAssignedPoses(corporateId,corporateName){
    this.selectedCorporateName = corporateName;
    this.selectedCorporateId = corporateId;
    console.log("assignedPos called");
    this.apollo
      .query<any>({
        query:assignedUsersPos,
        variables:{
          userId:this.currentUser._id,
          corporateId:corporateId
        }
      })
      .subscribe(result => {
        this.poses = result.data && result.data.assignedUsersPos.result;
        console.log("assignedPos = ", result.data);
      })
  }

  syncPosId(posId,purchaseType){
    console.log("posId = " + posId + " purchaseType = " + purchaseType);
    this.posSync.updatePosId(posId);
    this.posSync.updatePurchaseType(purchaseType);
    this.router.navigate( ['HomeComponent',{_id: this.selectedCorporateId, id2: posId}]);
  }

}