import { ApolloModule, Apollo } from "apollo-angular";
import { HttpLinkModule, HttpLink } from "apollo-angular-link-http";
import { InMemoryCache } from "apollo-cache-inmemory";
import { resetCaches } from "graphql-tag";
import { setContext } from 'apollo-link-context'
import { NgModule } from '@angular/core';
import { HttpClientModule, HttpHeaders } from '@angular/common/http';

const uri = 'https://mclass.mazzi.mn/backend/graphql'; // <-- add the URL of the GraphQL server here

// export function createApollo(httpLink: HttpLink) {
//   return {
//     link: httpLink.create({uri}),
//     cache: new InMemoryCache(),
//   };
// }

@NgModule({
  exports: [HttpClientModule, ApolloModule, HttpLinkModule]
})
export class GraphQLModule {
  constructor(apollo: Apollo, httpLink: HttpLink) {
    const http = httpLink.create({uri: 'https://mclass.mazzi.mn/backend/graphql'});
    const auth = setContext((_, { headers }) => {
      // get the authentication token from local storage if it exists
      const token = localStorage.getItem('jwtToken');
      // return the headers to the context so httpLink can read them
      // in this example we assume headers property exists
      // and it is an instance of HttpHeaders
      if (!token) {
        return {};
      } else {
        return {
          headers: new HttpHeaders().set('x-access-token', token)
        };
      }
    });

    const cache = new InMemoryCache({
  

      // dataIdFromObject: () => // custom idGetter,
      // addTypename: true,
      // cacheResolvers: {},
      // fragmentMatcher: new IntrospectionFragmentMatcher({
      //   introspectionQueryResultData: yourData
      // }),
    });

    // apollo.create({
    //   link: auth.concat(http),
    //   cache: new InMemoryCache({
    //     dataIdFromObject: o => o.id
    //   })
    // });

    // const client = new ApolloClient({
    //   cache: new InMemoryCache(),
    //   link: auth.concat(http),
    // })
    apollo.create({
      link: auth.concat(http),
      cache,
      // cache: new InMemoryCache(),
    })     
  }
}
