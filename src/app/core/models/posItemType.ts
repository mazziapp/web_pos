export interface PosItemType{
    id?: string;
    name: string;
    corporateId: string;
    createdDate: number;
}