import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { UserAuthService } from 'src/app/core/services/userService';
declare var window: any;
@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.css']
})

export class UserLoginComponent implements OnInit {
  isAuthenticated: boolean;
  isSubmitting = false;
  err = false;
  
  // loginForm: FormGroup;

  loginForm = new FormGroup({
    email: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required, Validators.minLength(6)])
  });

  constructor(
    private userService: UserAuthService,
    // private fb: FormBuilder,
    private router: Router,
    private location: Location
  ) {
   
  }

  ngOnInit() {
    if(localStorage.getItem('jwtToken')){
      this.router.navigateByUrl('/welcome');
    }
    history.forward();
    localStorage.removeItem('jwtToken');


    // $( document ).ready( function(){
    //   history.pushState(null,  document.title, location.href);        
    //  });

    // this.userService.logout();
    console.log('is here')
    this.userService.isAuthenticated.subscribe(
      (authenticated) => {
        this.isAuthenticated = authenticated;

        // set the article list accordingly
        if (authenticated) {
          this.router.navigate(['/welcome']);
        } else {
          // this.router.navigate(['/teacher/login']);
        }
      }
    );
      // from c# works here
    window.onCalledCsharp = (data) => {
      alert('Data from c#: ' + data);
    };
  }

  get f() {
    return this.loginForm.controls;
  }



  nfcCall(){
    window.external.nfcConnect("hello c# i am web");
  }

  submitLoginForm() {
    this.isSubmitting = true;
    const loginCredentials = this.loginForm.value;

    if (this.loginForm.invalid) {
      return;
    }
//        <WebBrowser Name="webView" HorizontalAlignment="Left" Margin="0" Source="https://mazzi-mongolia.firebaseapp.com/login" Width="705"/>
    this.userService.login(loginCredentials)
      .subscribe(
        response => {
          console.log(response, 'reeesponse');
          if (response.data.login != null) {
            // this.updateFcm(response.data.login._id)

            // history.pushState(null, null, location.href);
            this.location.replaceState('/welcome');

            this.router.navigateByUrl('/welcome');

            
            this.err = false;
          } else {
            this.err = true;
            // document.getElementById('email').classList.remove('valid');
            // document.getElementById('password').classList.remove('valid');
          }
        },
        error => {
          this.isSubmitting = false;
          // console.log("ERROR");
        }
      )
  }

  // updateFcm(userId){
  //   this.userService.updateFcm(userId, fcmToken).subscribe(response => {

  //   })
  // }
  
  

  hideError() {
    if (this.isSubmitting == true && this.err == true) {
      this.isSubmitting = false;
      this.err = false;
      document.getElementById("passwordErrorLabel").style.display = 'none';
      document.getElementById("emailErrorLabel").style.display = 'none';
    }
  }
}
