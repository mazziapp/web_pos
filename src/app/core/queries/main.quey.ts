import gql from 'graphql-tag';

/* TIER, PLAN, PRICE */

export const getCorporateTiersQuery = gql`
    query corporateTiers {
        corporateTiers {
            error
            status
            message
            result {
                _id
                name
                access {
                    mclass {
                        access
                        spec {
                            classLimit
                        }
                    }
                    pos {
                        access
                    }
                }
                price
                duration
            }
        }
    }
`;

export const insertCorporateQuery = gql`
    mutation insertCorporate($userId: String!, $InputCorporate: InputCorporate!) {
        insertCorporate(userId: $userId, InputCorporate: $InputCorporate) {
            error
            status
            message
            result {
                _id
                corporateCode
                ownerId
                name
                createdDate
            }
        }
    }
`;

export const updateCorporateQuery = gql`
    mutation updateCorporate($userId: String!, $corporateId: String!, $UpdateCorporate: UpdateCorporate!) {
        updateCorporate(userId: $userId, corporateId: $corporateId, UpdateCorporate: $UpdateCorporate) {
            error
            status
            message
            result {
                _id
                corporateCode
                ownerId
                name
                createdDate
            }
        }
    }
`;

export const getUsersCorporateMemberListQuery = gql`
    query usersCorporateMemberList($userId: String!) {
        usersCorporateMemberList(userId: $userId) {
            error
            status
            message
            result {
                _id
                userId
                corporateMemberPositionId {
                    _id
                    name
                }
                createdDate
                updatedDate
                corporateId {
                    _id
                    corporateCode
                    ownerId
                    name
                    createdDate
                }
            }
        }
    }
`;

export const getSingleCorporateMemberListQuery = gql`
    query singleCorporateMemberList($userId: String!, $corporateId: String!) {
        singleCorporateMemberList(userId: $userId, corporateId: $corporateId) {
            error
            status
            message
            result {
                _id
                userId
                corporateMemberPositionId {
                    _id
                    name
                }
                createdDate
                updatedDate
                corporateId {
                    _id
                    corporateCode
                    ownerId
                    name
                    createdDate
                }
            }
        }
    }
`;

export const getOwnedCorporateQuery = gql`
    query ownedCorporates($userId: String!) {
        ownedCorporates(userId: $userId) {
            error
            status
            message
            result {
                _id
                corporateCode
                ownerId
                name
                createdDate
            }
        }
    }
`;

export const getCorporateQuery = gql`
    query corporate($userId: String!, $corporateId: String!) {
        corporate(userId: $userId, corporateId: $corporateId) {
            error
            status
            message
            result {
                _id
                corporateCode
                ownerId
                name
                createdDate                
            }
        }
    }
`;

export const getCorporateTierQuery = gql`
    query corporateTier($corporateTierId: String!) {
        corporateTier(corporateTierId: $corporateTierId) {
            error
            status
            message
            result {
                _id
                name
                access {
                    mclass {
                        access
                        spec {
                            classLimit
                        }
                    }
                    pos {
                        access
                    }
                }
                price
                duration
            }
        }
    }
`;

export const joinCorporateQuery = gql`
    mutation joinCorporate($userId: String!, $corporateCode: String!){
        joinCorporate(userId: $userId, corporateCode: $corporateCode) {
            error
            status
            message
        }
    }
`;

export const getCorporateMemberListQuery = gql`
    query corporateMemberList($userId: String!, $corporateId: String!) {
        corporateMemberList(userId: $userId, corporateId: $corporateId) {
            error
            status
            message
            result {
                _id
                userId {
                    _id
                    region
                    image
                    nickname
                    lastname
                    surname
                    birthday
                    email
                    gender
                    phone
                    state
                    online_at
                    created_at                    
                }
                corporateMemberPositionId
                createdDate
                updatedDate
                corporateId
            }
        }
    }
`;

export const getCorporateMemberPositionsQuery = gql`
    query corporateMemberPositions {
        corporateMemberPositions {
            error
            status
            message
            result {
                _id
                name
                positionKey
            }
        }
    }
`;

export const changeCorporateMemberPositionQuery = gql`
    mutation changeCorporateMemberPosition($userId: String!, $corporateMemberListId: String!, $corporateMemberPositionId: String!) {
        changeCorporateMemberPosition(userId: $userId, corporateMemberListId: $corporateMemberListId, corporateMemberPositionId: $corporateMemberPositionId) {
            error
            status
            message
            result {
                _id
                userId {
                    _id
                    region
                    image
                    nickname
                    lastname
                    surname
                    birthday
                    email
                    gender
                    phone
                    state
                    online_at
                    created_at
                }
                corporateMemberPositionId
                createdDate
                updatedDate
                corporateId
            }
        }
    }
`;

export const kickCorporateMemberListQuery = gql`
    mutation kickCorporateMemberList($userId: String!, $corporateMemberListId: String!) {
        kickCorporateMemberList(userId: $userId, corporateMemberListId: $corporateMemberListId) {
            error
            status
            message            
        }
    }
`;

export const searchUserByPhoneQuery = gql`
    query searchUserByPhone($userId: String!, $phone: String!) {
        searchUserByPhone(userId: $userId, phone: $phone) {
            error
            status
            message
            result {
                _id
                region
                image
                nickname
                lastname
                surname
                birthday
                email
                gender
                phone
                state
                online_at
                created_at                
            }
        }
    }
`;

export const inviteToCorporateQuery = gql`
    mutation inviteToCorporate($userId: String!, $corporateId: String!, $subjectUsersId: String!, $corporateMemberPositionId: String!) {
        inviteToCorporate(userId: $userId, corporateId: $corporateId, subjectUsersId: $subjectUsersId, corporateMemberPositionId: $corporateMemberPositionId) {
            error
            status
            message
            result {
                _id
                userId {
                    _id
                    region
                    image
                    nickname
                    lastname
                    surname
                    birthday
                    email
                    gender
                    phone
                    state
                    online_at
                    created_at
                }
                corporateMemberPositionId
                createdDate
                updatedDate
                corporateId
            }
        }
    }
`;