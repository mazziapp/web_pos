export interface User{
    _id: String,
    region: String,
    image: String,
    nickname: String,
    lastname: String,
    surname: String,
    birthday: Number,
    email: String,
    gender: String,
    phone: String,
    registerId: String,
    state: Number,
    online_at: Number,
    created_at: Number,
    is_confirmed: Boolean,
    device_id: String[],
    fcm_token: String,
    roleId: String,
    token: String,
    subUser: SubUser
    // emailAuthenticated: Boolean,
    // universityId: String
}

export interface SubUser{
    _id: string,
    userId: string,
    schoolId: string,
    storageLimit: number,
    tierId: string,
    path: string
    studentIdCode: string,
    subscriptionEndDate: number,
    roleId: string,
    notification: SubUserNotification
} 

export interface SubUserNotification {
    mchatNotificationCount: number,
    mclassNotificationCount: number,
    lastDeleted: number
}

export interface Role {
    _id: String,
    name: String,
    priority: String
}

export interface University {
    _id: String,
    name: String
}

export interface ClassUser { 
    _id: String,
    classCode: String,
    classId: String,
    classOwnerId: String,
    userId: PublicUserInfo,    
    roleId: String,
    classRole: String,
    className: String,
    state: String,
    joinedDate: Number
}

export interface PublicUserInfo {
    _id: string,
    region: string,
    image: string,
    nickname: string,
    lastname: string,
    surname: string,
    birthday: number,
    email: string,
    gender: string,
    phone: string,
    state: number,
    online_at: number,
    created_at: number
}

export interface Tier {
    _id: string,
    name: string,
    key: string,
    timeLimit: number,
    price: number,
    storageSpace: number,
    module: string[]
    
}