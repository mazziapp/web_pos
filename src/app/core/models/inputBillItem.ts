export interface InputBillItem{
    posItemId: string;
    quantity: number;
}