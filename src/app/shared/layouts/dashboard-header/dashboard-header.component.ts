import { Component, OnInit, OnChanges, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Content } from '@angular/compiler/src/render3/r3_ast';
import { User, PublicUserInfo } from 'src/app/core/models/user.model';
import { UserAuthService } from 'src/app/core/services/userService';
import { PosService } from 'src/app/core/services/pos.service';
import { PosItem } from 'src/app/core/models/posItem';

@Component({
  selector: 'app-dashboard-header',
  templateUrl: './dashboard-header.component.html',
  styleUrls: ['./dashboard-header.component.css'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class DashboardHeaderComponent implements OnInit, OnChanges {
  currentUser: User;
  notifications: Notification[] = [];
  firebaseNotif: any[] = [];
  message: any[] = [];
  newNotificationCount = 0;
  ticket: PosItem[];

  skip = 0; limit = 20;

  notification; classId;
  notificationCountSum = 0;
  // dateNow;
  allNotifications: AllNotification[] = [];


  constructor(
    private userService: UserAuthService,
    private router: Router,
    private route: ActivatedRoute,
    private cdRef: ChangeDetectorRef,
    private ticketSync: PosService
  ) {
    // this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.userService.currentUser.subscribe(
      (userData) => {
        this.currentUser = userData;
        // this.notificationCountSum = userData.subUser.notification.mclassNotificationCount;
      }
    );

    // this.getNotifications();
  }

  ngOnInit() {

    this.classId = this.route.snapshot.paramMap.get('_id')

    // this.newNotificationCount = this.currentUser.subUser.notification.mclassNotificationCount;

    console.log(this.currentUser._id, 'currentUserrrr')
    
  }

  refresh() {
    console.log('refresh', this.allNotifications)
    console.log('notification', this.notifications)
    // this.cdRef.detectChanges();
    // this.cdRef.markForCheck();
  }

  // clearNotification(){
  //   this.allNotifications = []
  // }

  ngOnChanges() {

  }

  ngAfterViewInit(): void {
    //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
    //Add 'implements AfterViewInit' to the class.
    this.cdRef.detectChanges();
  }

  ngAfterViewChecked() {
    // console.log("! changement de la date du composant !");
    // this.dateNow = new Date();
    this.cdRef.detectChanges();
  }
  logout() {
    console.log("logout called");
    this.ticket = [];
    this.ticketSync.changeTicket(this.ticket);

    this.userService.logout(this.currentUser._id).subscribe(response => {
      if (response.data.logout.error == false) {
        this.userService.purgeAuth();
        this.router.navigateByUrl('/login');
      }
    })
  }

  // showStudentIdModal() {
  //   console.log('eded')
  // }

}

export interface AllNotification {
  _id: String,
  type: String,
  content: String,
  ownerId: PublicUserInfo,
  nickname: String,
  lastname: String,
  image: String,
  classId: String,
  rootPostId: String,
  postId: String,
  show: String[],
  seen: String[],
  createdDate: Number,
  alreadySeen: Boolean,
}
