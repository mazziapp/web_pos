import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';

import { map ,  take } from 'rxjs/operators';
import { UserAuthService } from 'src/app/core/services/userService';

@Injectable()
export class NoAuthGuard implements CanActivate {
  localStorageVal;
  constructor(
    private router: Router,
    private userService: UserAuthService
  ) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean>{
    // this.userService.isAuthenticated.pipe(take(1), map(isAuth => !isAuth))
    //   .subscribe(response => {
    //     console.log(response, 'res')
    //     return response;
    //   })
    return this.userService.isAuthenticated.pipe(take(1), map(isAuth => !isAuth))
  }
}