import { Component } from '@angular/core';
import { UserAuthService } from './core/services/userService';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Маззи ПОС';

  constructor(
    private userService: UserAuthService,
    private router: Router,
  ) {
  }

  ngOnInit() {
    this.userService.populate();
  }
}
