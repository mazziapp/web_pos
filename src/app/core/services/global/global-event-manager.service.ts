import { Injectable, EventEmitter } from "@angular/core";

@Injectable()
export class GlobalEventsManager {
    public showSideMenu: EventEmitter<any> = new EventEmitter();
    public hideSideMenu: EventEmitter<any> = new EventEmitter();

    /* */

    public passSelectedCorporate: EventEmitter<any> = new EventEmitter();
}