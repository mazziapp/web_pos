(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _components_home_home_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/home/home.component */ "./src/app/components/home/home.component.ts");
/* harmony import */ var _components_auth_user_login_user_login_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/auth/user-login/user-login.component */ "./src/app/components/auth/user-login/user-login.component.ts");
/* harmony import */ var _shared_not_found_not_found_not_found_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./shared/not-found/not-found/not-found.component */ "./src/app/shared/not-found/not-found/not-found.component.ts");
/* harmony import */ var _core_services_auth_guard_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./core/services/auth-guard.service */ "./src/app/core/services/auth-guard.service.ts");
/* harmony import */ var _components_welcome_welcome_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./components/welcome/welcome.component */ "./src/app/components/welcome/welcome.component.ts");








var routes = [
    { path: '', redirectTo: '/login', pathMatch: 'full' },
    { path: 'login', component: _components_auth_user_login_user_login_component__WEBPACK_IMPORTED_MODULE_4__["UserLoginComponent"] },
    { path: 'welcome', component: _components_welcome_welcome_component__WEBPACK_IMPORTED_MODULE_7__["WelcomeComponent"], canActivate: [_core_services_auth_guard_service__WEBPACK_IMPORTED_MODULE_6__["AuthGuard"]] },
    // { path: 'home', component: HomeComponent, canActivate:[AuthGuard]},
    { path: 'not-found', component: _shared_not_found_not_found_not_found_component__WEBPACK_IMPORTED_MODULE_5__["NotFoundComponent"] },
    // { path: ':_id',component: HomeComponent},
    // { path: ':_id/id2', component: HomeComponent},
    { path: 'HomeComponent', component: _components_home_home_component__WEBPACK_IMPORTED_MODULE_3__["HomeComponent"] },
    { path: '**', component: _components_welcome_welcome_component__WEBPACK_IMPORTED_MODULE_7__["WelcomeComponent"], canActivate: [_core_services_auth_guard_service__WEBPACK_IMPORTED_MODULE_6__["AuthGuard"]] },
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n\r\n@import url('https://fonts.googleapis.com/css?family=Hind+Madurai:300|Lora:400i|Montserrat:700');\n/* // Font settigns */\n$main-font: 'Montserrat';\n$secondary-font: 'Lora';\n$body-font: 'Hind Madurai';\n$main-color: #3f51b5;\n.main{\r\n  display: flex;\r\n  flex-direction: row;\r\n  justify-content: center;\r\n  margin: 1rem;\r\n}\n.ticket {\r\n  min-width: 320px;\r\n\r\n}\n.pos {\r\n  min-width: 320px;\r\n  float: top;\r\n}\n.content {\r\n  flex-wrap: wrap;\r\n}\n@media screen and (min-width: 425px) {\r\n  .pos {\r\n    width: 92%;\r\n  }\r\n  .ticket {\r\n    width: 92%;\r\n  }\r\n  .bottom-content {\r\n    width: 92%;\r\n  }\r\n}\n@media screen and (min-width: 768px) {\r\n  .pos {\r\n    width: 45%;\r\n  }\r\n  .ticket {\r\n    width: 45%;\r\n  }\r\n  .bottom-content {\r\n    width: 92%;\r\n  }\r\n}\np {\r\n  font-family: $body-font;\r\n}\nh1 {\r\n  font-family: $main-font;\r\n  font-size: 2rem;\r\n}\nh3 {\r\n  font-family: $body-font;\r\n  font-size: 1.15rem;\r\n}\n.text-align-center {\r\n  text-align: center;\r\n}\na {\r\n  text-decoration: none;\r\n}\nli {\r\n  list-style: none;\r\n}\n/* // Colors */\n/* // Flex settings */\n.flex {\r\n  display: flex;\r\n}\n.flex-direction-row {\r\n  flex-direction: row;\r\n}\n.flex-direction-column {\r\n  flex-direction: column;\r\n}\n.flex-wrap {\r\n  flex-wrap: wrap;\r\n}\n.align-items-center {\r\n  align-items: center;\r\n}\n.align-content-center {\r\n  align-content: center;\r\n}\n.justify-center {\r\n  justify-content: center;\r\n}\n.justify-between {\r\n  justify-content: space-between;\r\n}\n.justify-around {\r\n  justify-content: space-around;\r\n}\n/* // Margin & Padding settings */\n.add-margin {\r\n  margin: 1rem;\r\n}\n.add-padding {\r\n  padding: 1rem;\r\n}\n.padding-horizontal {\r\n  padding-left: 1rem;\r\n  padding-right: 1rem;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXBwLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFFQSxnR0FBZ0c7QUFGaEcscUJBQXFCO0FBR3JCLHdCQUF3QjtBQUN4Qix1QkFBdUI7QUFDdkIsMEJBQTBCO0FBQzFCLG9CQUFvQjtBQUdwQjtFQUNFLGFBQWE7RUFDYixtQkFBbUI7RUFDbkIsdUJBQXVCO0VBQ3ZCLFlBQVk7QUFDZDtBQUVBO0VBQ0UsZ0JBQWdCOztBQUVsQjtBQUVBO0VBQ0UsZ0JBQWdCO0VBQ2hCLFVBQVU7QUFDWjtBQUVBO0VBQ0UsZUFBZTtBQUNqQjtBQUVBO0VBQ0U7SUFDRSxVQUFVO0VBQ1o7RUFDQTtJQUNFLFVBQVU7RUFDWjtFQUNBO0lBQ0UsVUFBVTtFQUNaO0FBQ0Y7QUFFQTtFQUNFO0lBQ0UsVUFBVTtFQUNaO0VBQ0E7SUFDRSxVQUFVO0VBQ1o7RUFDQTtJQUNFLFVBQVU7RUFDWjtBQUNGO0FBRUE7RUFDRSx1QkFBdUI7QUFDekI7QUFFQTtFQUNFLHVCQUF1QjtFQUN2QixlQUFlO0FBQ2pCO0FBRUE7RUFDRSx1QkFBdUI7RUFDdkIsa0JBQWtCO0FBQ3BCO0FBRUE7RUFDRSxrQkFBa0I7QUFDcEI7QUFFQTtFQUNFLHFCQUFxQjtBQUN2QjtBQUVBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBRUEsY0FBYztBQUdkLHFCQUFxQjtBQUVyQjtFQUNFLGFBQWE7QUFDZjtBQUVBO0VBQ0UsbUJBQW1CO0FBQ3JCO0FBRUE7RUFDRSxzQkFBc0I7QUFDeEI7QUFDQTtFQUNFLGVBQWU7QUFDakI7QUFFQTtFQUNFLG1CQUFtQjtBQUNyQjtBQUVBO0VBQ0UscUJBQXFCO0FBQ3ZCO0FBRUE7RUFDRSx1QkFBdUI7QUFDekI7QUFFQTtFQUNFLDhCQUE4QjtBQUNoQztBQUVBO0VBQ0UsNkJBQTZCO0FBQy9CO0FBRUEsaUNBQWlDO0FBRWpDO0VBQ0UsWUFBWTtBQUNkO0FBRUE7RUFDRSxhQUFhO0FBQ2Y7QUFFQTtFQUNFLGtCQUFrQjtFQUNsQixtQkFBbUI7QUFDckIiLCJmaWxlIjoic3JjL2FwcC9hcHAuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC8vIEZvbnQgc2V0dGlnbnMgKi9cclxuXHJcbkBpbXBvcnQgdXJsKCdodHRwczovL2ZvbnRzLmdvb2dsZWFwaXMuY29tL2Nzcz9mYW1pbHk9SGluZCtNYWR1cmFpOjMwMHxMb3JhOjQwMGl8TW9udHNlcnJhdDo3MDAnKTtcclxuJG1haW4tZm9udDogJ01vbnRzZXJyYXQnO1xyXG4kc2Vjb25kYXJ5LWZvbnQ6ICdMb3JhJztcclxuJGJvZHktZm9udDogJ0hpbmQgTWFkdXJhaSc7XHJcbiRtYWluLWNvbG9yOiAjM2Y1MWI1O1xyXG5cclxuXHJcbi5tYWlue1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICBtYXJnaW46IDFyZW07XHJcbn1cclxuXHJcbi50aWNrZXQge1xyXG4gIG1pbi13aWR0aDogMzIwcHg7XHJcblxyXG59XHJcblxyXG4ucG9zIHtcclxuICBtaW4td2lkdGg6IDMyMHB4O1xyXG4gIGZsb2F0OiB0b3A7XHJcbn1cclxuXHJcbi5jb250ZW50IHtcclxuICBmbGV4LXdyYXA6IHdyYXA7XHJcbn1cclxuXHJcbkBtZWRpYSBzY3JlZW4gYW5kIChtaW4td2lkdGg6IDQyNXB4KSB7XHJcbiAgLnBvcyB7XHJcbiAgICB3aWR0aDogOTIlO1xyXG4gIH1cclxuICAudGlja2V0IHtcclxuICAgIHdpZHRoOiA5MiU7XHJcbiAgfVxyXG4gIC5ib3R0b20tY29udGVudCB7XHJcbiAgICB3aWR0aDogOTIlO1xyXG4gIH1cclxufVxyXG5cclxuQG1lZGlhIHNjcmVlbiBhbmQgKG1pbi13aWR0aDogNzY4cHgpIHtcclxuICAucG9zIHtcclxuICAgIHdpZHRoOiA0NSU7XHJcbiAgfVxyXG4gIC50aWNrZXQge1xyXG4gICAgd2lkdGg6IDQ1JTtcclxuICB9XHJcbiAgLmJvdHRvbS1jb250ZW50IHtcclxuICAgIHdpZHRoOiA5MiU7XHJcbiAgfVxyXG59XHJcblxyXG5wIHtcclxuICBmb250LWZhbWlseTogJGJvZHktZm9udDtcclxufVxyXG5cclxuaDEge1xyXG4gIGZvbnQtZmFtaWx5OiAkbWFpbi1mb250O1xyXG4gIGZvbnQtc2l6ZTogMnJlbTtcclxufVxyXG5cclxuaDMge1xyXG4gIGZvbnQtZmFtaWx5OiAkYm9keS1mb250O1xyXG4gIGZvbnQtc2l6ZTogMS4xNXJlbTtcclxufVxyXG5cclxuLnRleHQtYWxpZ24tY2VudGVyIHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuXHJcbmEge1xyXG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxufVxyXG5cclxubGkge1xyXG4gIGxpc3Qtc3R5bGU6IG5vbmU7XHJcbn1cclxuXHJcbi8qIC8vIENvbG9ycyAqL1xyXG5cclxuXHJcbi8qIC8vIEZsZXggc2V0dGluZ3MgKi9cclxuXHJcbi5mbGV4IHtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG59XHJcblxyXG4uZmxleC1kaXJlY3Rpb24tcm93IHtcclxuICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG59XHJcblxyXG4uZmxleC1kaXJlY3Rpb24tY29sdW1uIHtcclxuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG59XHJcbi5mbGV4LXdyYXAge1xyXG4gIGZsZXgtd3JhcDogd3JhcDtcclxufVxyXG5cclxuLmFsaWduLWl0ZW1zLWNlbnRlciB7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxufVxyXG5cclxuLmFsaWduLWNvbnRlbnQtY2VudGVyIHtcclxuICBhbGlnbi1jb250ZW50OiBjZW50ZXI7XHJcbn1cclxuXHJcbi5qdXN0aWZ5LWNlbnRlciB7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbn1cclxuXHJcbi5qdXN0aWZ5LWJldHdlZW4ge1xyXG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxufVxyXG5cclxuLmp1c3RpZnktYXJvdW5kIHtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcclxufVxyXG5cclxuLyogLy8gTWFyZ2luICYgUGFkZGluZyBzZXR0aW5ncyAqL1xyXG5cclxuLmFkZC1tYXJnaW4ge1xyXG4gIG1hcmdpbjogMXJlbTtcclxufVxyXG5cclxuLmFkZC1wYWRkaW5nIHtcclxuICBwYWRkaW5nOiAxcmVtO1xyXG59XHJcblxyXG4ucGFkZGluZy1ob3Jpem9udGFsIHtcclxuICBwYWRkaW5nLWxlZnQ6IDFyZW07XHJcbiAgcGFkZGluZy1yaWdodDogMXJlbTtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n  <!-- <div class=\"ticket add-margin\">\r\n    <app-ticket></app-ticket>\r\n  </div>\r\n\r\n  <div class=\"pos add-margin\">\r\n    <app-pos></app-pos>\r\n  </div> -->\r\n  <!-- <app-user-login></app-user-login> -->\r\n  <router-outlet></router-outlet>\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _core_services_userService__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./core/services/userService */ "./src/app/core/services/userService.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");




var AppComponent = /** @class */ (function () {
    function AppComponent(userService, router) {
        this.userService = userService;
        this.router = router;
        this.title = 'aaaash chincha';
    }
    AppComponent.prototype.ngOnInit = function () {
        this.userService.populate();
    };
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_core_services_userService__WEBPACK_IMPORTED_MODULE_2__["UserAuthService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: MyHammerConfig, AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MyHammerConfig", function() { return MyHammerConfig; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _graphql_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./graphql.module */ "./src/app/graphql.module.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var apollo_angular__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! apollo-angular */ "./node_modules/apollo-angular/fesm5/ng.apollo.js");
/* harmony import */ var apollo_angular_link_http__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! apollo-angular-link-http */ "./node_modules/apollo-angular-link-http/fesm5/ng.apolloLink.http.js");
/* harmony import */ var _components_pos_item_pos_item_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./components/pos-item/pos-item.component */ "./src/app/components/pos-item/pos-item.component.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _components_ticket_ticket_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./components/ticket/ticket.component */ "./src/app/components/ticket/ticket.component.ts");
/* harmony import */ var _core_services_pos_service__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./core/services/pos.service */ "./src/app/core/services/pos.service.ts");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _components_auth_user_login_user_login_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./components/auth/user-login/user-login.component */ "./src/app/components/auth/user-login/user-login.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _core_services_userService__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./core/services/userService */ "./src/app/core/services/userService.ts");
/* harmony import */ var _core_services_jwt_service__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./core/services/jwt.service */ "./src/app/core/services/jwt.service.ts");
/* harmony import */ var _components_home_home_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./components/home/home.component */ "./src/app/components/home/home.component.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _components_auth_no_auth_guard_service__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./components/auth/no-auth-guard.service */ "./src/app/components/auth/no-auth-guard.service.ts");
/* harmony import */ var _components_corporate_corporate_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./components/corporate/corporate.component */ "./src/app/components/corporate/corporate.component.ts");
/* harmony import */ var _core_services_main_service__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./core/services/main.service */ "./src/app/core/services/main.service.ts");
/* harmony import */ var _core_services_global_global_event_manager_service__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./core/services/global/global-event-manager.service */ "./src/app/core/services/global/global-event-manager.service.ts");
/* harmony import */ var _shared_not_found_not_found_not_found_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./shared/not-found/not-found/not-found.component */ "./src/app/shared/not-found/not-found/not-found.component.ts");
/* harmony import */ var _shared_layouts_dashboard_header_dashboard_header_component__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./shared/layouts/dashboard-header/dashboard-header.component */ "./src/app/shared/layouts/dashboard-header/dashboard-header.component.ts");
/* harmony import */ var _shared_layouts_dashboard_footer_dashboard_footer_component__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./shared/layouts/dashboard-footer/dashboard-footer.component */ "./src/app/shared/layouts/dashboard-footer/dashboard-footer.component.ts");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! hammerjs */ "./node_modules/hammerjs/hammer.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_28___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_28__);
/* harmony import */ var _core_services_auth_guard_service__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ./core/services/auth-guard.service */ "./src/app/core/services/auth-guard.service.ts");
/* harmony import */ var _components_welcome_welcome_component__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! ./components/welcome/welcome.component */ "./src/app/components/welcome/welcome.component.ts");
/* harmony import */ var _sweetalert2_ngx_sweetalert2__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! @sweetalert2/ngx-sweetalert2 */ "./node_modules/@sweetalert2/ngx-sweetalert2/fesm5/sweetalert2-ngx-sweetalert2.js");

































var MyHammerConfig = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](MyHammerConfig, _super);
    function MyHammerConfig() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.overrides = {
            // override hammerjs default configuration
            'swipe': { direction: hammerjs__WEBPACK_IMPORTED_MODULE_28__["DIRECTION_ALL"] }
        };
        return _this;
    }
    return MyHammerConfig;
}(_angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["HammerGestureConfig"]));

var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"],
                _components_pos_item_pos_item_component__WEBPACK_IMPORTED_MODULE_10__["PosItemComponent"],
                _components_ticket_ticket_component__WEBPACK_IMPORTED_MODULE_12__["TicketComponent"],
                _components_auth_user_login_user_login_component__WEBPACK_IMPORTED_MODULE_15__["UserLoginComponent"],
                _components_home_home_component__WEBPACK_IMPORTED_MODULE_19__["HomeComponent"],
                _components_corporate_corporate_component__WEBPACK_IMPORTED_MODULE_22__["CorporateComponent"],
                _shared_not_found_not_found_not_found_component__WEBPACK_IMPORTED_MODULE_25__["NotFoundComponent"],
                _shared_layouts_dashboard_footer_dashboard_footer_component__WEBPACK_IMPORTED_MODULE_27__["DashboardFooterComponent"],
                _shared_layouts_dashboard_header_dashboard_header_component__WEBPACK_IMPORTED_MODULE_26__["DashboardHeaderComponent"],
                _components_pos_item_pos_item_component__WEBPACK_IMPORTED_MODULE_10__["CheckoutDialog"],
                _components_welcome_welcome_component__WEBPACK_IMPORTED_MODULE_30__["WelcomeComponent"]
            ],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"],
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_4__["AppRoutingModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatCardModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatMenuModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatFormFieldModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatGridListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatTabsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatDialogModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatButtonToggleModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatPaginatorModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_11__["MatButtonModule"],
                _graphql_module__WEBPACK_IMPORTED_MODULE_6__["GraphQLModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_7__["HttpClientModule"],
                apollo_angular__WEBPACK_IMPORTED_MODULE_8__["ApolloModule"],
                apollo_angular_link_http__WEBPACK_IMPORTED_MODULE_9__["HttpLinkModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_14__["BrowserAnimationsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_16__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_16__["ReactiveFormsModule"],
                _sweetalert2_ngx_sweetalert2__WEBPACK_IMPORTED_MODULE_31__["SweetAlert2Module"].forRoot({
                    buttonsStyling: false,
                    customClass: 'modal-content',
                    confirmButtonClass: 'btn btn-primary',
                    cancelButtonClass: 'btn'
                }),
                _angular_router__WEBPACK_IMPORTED_MODULE_20__["RouterModule"].forRoot([
                    {
                        path: 'login',
                        component: _components_auth_user_login_user_login_component__WEBPACK_IMPORTED_MODULE_15__["UserLoginComponent"]
                    },
                    {
                        path: 'home',
                        component: _components_home_home_component__WEBPACK_IMPORTED_MODULE_19__["HomeComponent"]
                    }
                ])
            ],
            providers: [
                //   {
                //   provide: APOLLO_OPTIONS,
                //   useFactory: (HttpLink: HttpLink) => {
                //     return {
                //       cache: new InMemoryCache(),
                //       link: HttpLink.create({
                //         uri: "http://192.168.0.105:10000/graphql"
                //       })
                //     }
                //   },
                //   deps: [HttpLink]
                // }
                ,
                {
                    provide: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["HAMMER_GESTURE_CONFIG"],
                    useClass: MyHammerConfig
                },
                _core_services_pos_service__WEBPACK_IMPORTED_MODULE_13__["PosService"],
                _core_services_userService__WEBPACK_IMPORTED_MODULE_17__["UserAuthService"],
                _core_services_jwt_service__WEBPACK_IMPORTED_MODULE_18__["JwtService"],
                _components_auth_no_auth_guard_service__WEBPACK_IMPORTED_MODULE_21__["NoAuthGuard"],
                _core_services_auth_guard_service__WEBPACK_IMPORTED_MODULE_29__["AuthGuard"],
                _core_services_main_service__WEBPACK_IMPORTED_MODULE_23__["MainService"],
                _core_services_global_global_event_manager_service__WEBPACK_IMPORTED_MODULE_24__["GlobalEventsManager"]
            ],
            entryComponents: [_components_pos_item_pos_item_component__WEBPACK_IMPORTED_MODULE_10__["CheckoutDialog"]],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/components/auth/no-auth-guard.service.ts":
/*!**********************************************************!*\
  !*** ./src/app/components/auth/no-auth-guard.service.ts ***!
  \**********************************************************/
/*! exports provided: NoAuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NoAuthGuard", function() { return NoAuthGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var src_app_core_services_userService__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/core/services/userService */ "./src/app/core/services/userService.ts");





var NoAuthGuard = /** @class */ (function () {
    function NoAuthGuard(router, userService) {
        this.router = router;
        this.userService = userService;
    }
    NoAuthGuard.prototype.canActivate = function (route, state) {
        // this.userService.isAuthenticated.pipe(take(1), map(isAuth => !isAuth))
        //   .subscribe(response => {
        //     console.log(response, 'res')
        //     return response;
        //   })
        return this.userService.isAuthenticated.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["take"])(1), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (isAuth) { return !isAuth; }));
    };
    NoAuthGuard = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            src_app_core_services_userService__WEBPACK_IMPORTED_MODULE_4__["UserAuthService"]])
    ], NoAuthGuard);
    return NoAuthGuard;
}());



/***/ }),

/***/ "./src/app/components/auth/user-login/user-login.component.css":
/*!*********************************************************************!*\
  !*** ./src/app/components/auth/user-login/user-login.component.css ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".btn-info.disabled, .btn-info:disabled {\r\n    color: #fff;\r\n    background-color: #b6e0ff;\r\n    border-color: #f6f7fb;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9hdXRoL3VzZXItbG9naW4vdXNlci1sb2dpbi5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksV0FBVztJQUNYLHlCQUF5QjtJQUN6QixxQkFBcUI7QUFDekIiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL2F1dGgvdXNlci1sb2dpbi91c2VyLWxvZ2luLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYnRuLWluZm8uZGlzYWJsZWQsIC5idG4taW5mbzpkaXNhYmxlZCB7XHJcbiAgICBjb2xvcjogI2ZmZjtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICNiNmUwZmY7XHJcbiAgICBib3JkZXItY29sb3I6ICNmNmY3ZmI7XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/components/auth/user-login/user-login.component.html":
/*!**********************************************************************!*\
  !*** ./src/app/components/auth/user-login/user-login.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"authentication\">\r\n  <div class=\"sign-in\">\r\n      <div class=\"row no-mrg-horizon\">\r\n          <div class=\"col-md-8 no-pdd-horizon d-none d-md-block\">\r\n              <div class=\"full-height bg\" style=\"background-image: url('assets/images/background.jpg')\">\r\n                  <div class=\"img-caption\">\r\n                      <!-- <h1 class=\"caption-title\">We make spectacular</h1>\r\n                      <p class=\"caption-text\">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p> -->\r\n                  </div>\r\n              </div>\r\n          </div>\r\n          <div class=\"col-md-4 no-pdd-horizon\">\r\n              <div class=\"full-height bg-white height-100\">\r\n                  <div class=\"vertical-align full-height pdd-horizon-70\">\r\n                      <div class=\"table-cell\">\r\n                          <div class=\"pdd-horizon-15\">\r\n                              <h2>Нэвтрэх</h2>\r\n                              <p class=\"mrg-btm-15 font-size-13\">M Class - д тавтай морилно уу</p>\r\n                              <form [formGroup]=\"loginForm\" (ngSubmit)=\"submitLoginForm()\">\r\n                                  <div class=\"form-group\">\r\n                                      <input type=\"email\" formControlName=\"email\" class=\"form-control\" (focus)=\"hideError()\"\r\n  \r\n                                          placeholder=\"Таны и-мэйл хаяг эсвэл утасны дугаар:\">\r\n                                      <div *ngIf=\"f.email.touched && f.email.invalid\">\r\n                                          <label *ngIf=\"f.email.errors.required\" class=\"error\">И-мэйл хаяг эсвэл утасны дугаар хоосон байна.</label>\r\n                                          <label *ngIf=\"f.email.errors.email\" class=\"error\">И-мэйл хаяг эсвэл утасны дугаар буруу байна.</label>\r\n                                      </div>\r\n                                      <label id='emailErrorLabel' *ngIf=\"isSubmitting && err\" class=\"error\">И-мэйл хаяг эсвэл утасны дугаар буруу байна.</label>\r\n                                  </div>\r\n                                  <div class=\"form-group\">\r\n                                      <input type=\"password\" formControlName=\"password\" class=\"form-control\" (focus)=\"hideError()\" \r\n                                      \r\n                                          placeholder=\"Таны нууц үг:\">\r\n                                      <div *ngIf=\"f.password.touched && f.password.invalid\">\r\n                                          <label *ngIf=\"f.password.errors.required\" class=\"error\">Нууц үг хоосон байна.</label>\r\n                                          <label *ngIf=\"f.password.errors.minlength\" class=\"error\">Нууц үг буруу байна.</label>\r\n                                      </div>\r\n                                      <label id='passwordErrorLabel' *ngIf=\"isSubmitting && err\" class=\"error\">Нууц үг буруу байна.</label>\r\n                                  </div>\r\n                                  <!-- <div class=\"checkbox font-size-12\">\r\n                                      <input id=\"agreement\" name=\"agreement\" type=\"checkbox\">\r\n                                      <label for=\"agreement\">Намайг сана</label>\r\n                                  </div> -->\r\n                                  <div class=\"pull-right\">\r\n                                      <a routerLink=\"/forgot-password\">Нууц үгээ мартсан уу?</a>\r\n                                  </div>\r\n                                  <div class=\"form-group mrg-top-20\">\r\n                                      <button type=\"submit\" class=\"btn btn-info\" [disabled]=\"!loginForm.valid\">Нэвтрэх</button>\r\n            \r\n                  <button (click)=\"nfcCall()\">Nfc call new2</button>\r\n         \r\n\r\n                                  </div>\r\n                              </form>\r\n                          </div>\r\n                      </div>\r\n                  </div>\r\n                  <!-- <div class=\"login-footer\">\r\n                      <img class=\"img-responsive inline-block\" src=\"assets/images/logo/logo.png\" width=\"100\" alt=\"\">\r\n                      <span class=\"font-size-14 pull-right pdd-top-10\">Та бүртгэлгүй бол <a routerLink=\"/register\">энд дарж</a> бүртгүүлнэ үү</span>\r\n                  </div> -->\r\n              </div>\r\n          </div>\r\n      </div>\r\n  </div>\r\n  </div>\r\n  "

/***/ }),

/***/ "./src/app/components/auth/user-login/user-login.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/components/auth/user-login/user-login.component.ts ***!
  \********************************************************************/
/*! exports provided: UserLoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserLoginComponent", function() { return UserLoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var src_app_core_services_userService__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/core/services/userService */ "./src/app/core/services/userService.ts");






var UserLoginComponent = /** @class */ (function () {
    function UserLoginComponent(userService, 
    // private fb: FormBuilder,
    router, location) {
        this.userService = userService;
        this.router = router;
        this.location = location;
        this.isSubmitting = false;
        this.err = false;
        // loginForm: FormGroup;
        this.loginForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]),
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(6)])
        });
    }
    UserLoginComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (localStorage.getItem('jwtToken')) {
            this.router.navigateByUrl('/welcome');
        }
        history.forward();
        localStorage.removeItem('jwtToken');
        // $( document ).ready( function(){
        //   history.pushState(null,  document.title, location.href);        
        //  });
        // this.userService.logout();
        console.log('is here');
        this.userService.isAuthenticated.subscribe(function (authenticated) {
            _this.isAuthenticated = authenticated;
            // set the article list accordingly
            if (authenticated) {
                _this.router.navigate(['/welcome']);
            }
            else {
                // this.router.navigate(['/teacher/login']);
            }
        });
        // from c# works here
        window.onCalledCsharp = function (data) {
            alert('Data from c#: ' + data);
        };
    };
    Object.defineProperty(UserLoginComponent.prototype, "f", {
        get: function () {
            return this.loginForm.controls;
        },
        enumerable: true,
        configurable: true
    });
    UserLoginComponent.prototype.nfcCall = function () {
        window.external.nfcConnect("hello c# i am web");
    };
    UserLoginComponent.prototype.submitLoginForm = function () {
        var _this = this;
        this.isSubmitting = true;
        var loginCredentials = this.loginForm.value;
        if (this.loginForm.invalid) {
            return;
        }
        //        <WebBrowser Name="webView" HorizontalAlignment="Left" Margin="0" Source="https://mazzi-mongolia.firebaseapp.com/login" Width="705"/>
        this.userService.login(loginCredentials)
            .subscribe(function (response) {
            console.log(response, 'reeesponse');
            if (response.data.login != null) {
                // this.updateFcm(response.data.login._id)
                // history.pushState(null, null, location.href);
                _this.location.replaceState('/welcome');
                _this.router.navigateByUrl('/welcome');
                _this.err = false;
            }
            else {
                _this.err = true;
                // document.getElementById('email').classList.remove('valid');
                // document.getElementById('password').classList.remove('valid');
            }
        }, function (error) {
            _this.isSubmitting = false;
            // console.log("ERROR");
        });
    };
    // updateFcm(userId){
    //   this.userService.updateFcm(userId, fcmToken).subscribe(response => {
    //   })
    // }
    UserLoginComponent.prototype.hideError = function () {
        if (this.isSubmitting == true && this.err == true) {
            this.isSubmitting = false;
            this.err = false;
            document.getElementById("passwordErrorLabel").style.display = 'none';
            document.getElementById("emailErrorLabel").style.display = 'none';
        }
    };
    UserLoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-user-login',
            template: __webpack_require__(/*! ./user-login.component.html */ "./src/app/components/auth/user-login/user-login.component.html"),
            styles: [__webpack_require__(/*! ./user-login.component.css */ "./src/app/components/auth/user-login/user-login.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_core_services_userService__WEBPACK_IMPORTED_MODULE_5__["UserAuthService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _angular_common__WEBPACK_IMPORTED_MODULE_4__["Location"]])
    ], UserLoginComponent);
    return UserLoginComponent;
}());



/***/ }),

/***/ "./src/app/components/corporate/corporate.component.css":
/*!**************************************************************!*\
  !*** ./src/app/components/corporate/corporate.component.css ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".jumbotron {\r\n    background-color: #efefef;\r\n    padding-top: 20px;\r\n    padding-bottom: 10px;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9jb3Jwb3JhdGUvY29ycG9yYXRlLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSx5QkFBeUI7SUFDekIsaUJBQWlCO0lBQ2pCLG9CQUFvQjtBQUN4QiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvY29ycG9yYXRlL2NvcnBvcmF0ZS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmp1bWJvdHJvbiB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWZlZmVmO1xyXG4gICAgcGFkZGluZy10b3A6IDIwcHg7XHJcbiAgICBwYWRkaW5nLWJvdHRvbTogMTBweDtcclxufSJdfQ== */"

/***/ }),

/***/ "./src/app/components/corporate/corporate.component.html":
/*!***************************************************************!*\
  !*** ./src/app/components/corporate/corporate.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-content\">\r\n    <div class=\"container-fluid\">\r\n      <div class=\"jumbotron\">\r\n        <div class=\"text-right mrg-btm-10\">\r\n          <span>Байгууллагын нэр:</span><span\r\n            class=\"text-bold text-dark mrg-left-5 mrg-right-15\">{{currentCorporate?.corporateId.name}}</span>\r\n          <span>Байгууллагын код:</span><span\r\n            class=\"text-bold text-dark mrg-left-5 mrg-right-15\">{{currentCorporate?.corporateId.corporateCode}}</span>\r\n          <!-- <span>Байгууллагын гишүүдийн тоо:</span><span\r\n            class=\"text-bold text-dark mrg-left-5 mrg-right-15\">{{corporateMemberList.length}}</span> -->\r\n        </div>\r\n      </div>\r\n\r\n      <router-outlet></router-outlet>\r\n    </div>\r\n  </div>"

/***/ }),

/***/ "./src/app/components/corporate/corporate.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/components/corporate/corporate.component.ts ***!
  \*************************************************************/
/*! exports provided: browserRefresh, CorporateComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "browserRefresh", function() { return browserRefresh; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CorporateComponent", function() { return CorporateComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_core_services_global_global_event_manager_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/core/services/global/global-event-manager.service */ "./src/app/core/services/global/global-event-manager.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_core_services_main_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/core/services/main.service */ "./src/app/core/services/main.service.ts");
/* harmony import */ var src_app_core_services_userService__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/core/services/userService */ "./src/app/core/services/userService.ts");


// import { DashboardSidebarComponent } from 'src/app/shared';




var browserRefresh = false;
var CorporateComponent = /** @class */ (function () {
    // currentSideBarMenu: Menu[] = [];
    function CorporateComponent(userService, router, route, mainService, globalEventManager) {
        var _this = this;
        this.userService = userService;
        this.router = router;
        this.route = route;
        this.mainService = mainService;
        this.globalEventManager = globalEventManager;
        this.corporateMemberList = [];
        this.userService.currentUser.subscribe(function (userData) {
            _this.currentUser = userData;
        });
        this.mainService.currentRole.subscribe(function (userRole) {
            _this.currentUserRole = userRole;
            console.log(_this.currentUserRole, 'currentUserRole');
        });
    }
    CorporateComponent.prototype.ngOnInit = function () {
        this.currentCorporateId = this.route.snapshot.paramMap.get('_id');
        this.getCorporateMemberList(this.currentCorporateId);
        console.log(this.currentCorporateId, 'corpID');
        // this.getCorporate();
        this.getSingleCorporateMemberList();
    };
    CorporateComponent.prototype.ngOnDestroy = function () {
        //Called once, before the instance is destroyed.
        //Add 'implements OnDestroy' to the class.
        this.globalEventManager.showSideMenu.emit([]);
        // this.subscription.unsubscribe();
    };
    CorporateComponent.prototype.getCorporateMemberList = function (corporateId) {
        var _this = this;
        this.mainService.getCorporateMemberList(this.currentUser._id, corporateId)
            .subscribe(function (response) {
            if (response.data.corporateMemberList.error == false) {
                _this.corporateMemberList = response.data.corporateMemberList.result;
            }
        });
    };
    CorporateComponent.prototype.getCorporate = function () {
        var _this = this;
        this.mainService.getCorporate(this.currentUser._id, this.currentCorporateId)
            .subscribe(function (response) {
            if (response.data.corporate.error == false) {
                console.log(response, 'responsee');
                _this.currentCorporate = response.data.corporate.result;
                // this.getCorporateTier(response.data.corporate.result.corporateTierId);
            }
        });
    };
    CorporateComponent.prototype.getSingleCorporateMemberList = function () {
        var _this = this;
        this.mainService.getSingleCorporateMemberList(this.currentUser._id, this.currentCorporateId)
            .subscribe(function (response) {
            if (response.data.singleCorporateMemberList.error == false) {
                _this.currentCorporate = response.data.singleCorporateMemberList.result;
                _this.mainService.setRole(_this.currentCorporate.corporateMemberPositionId.name);
                console.log(_this.currentCorporate, 'currentCorporate');
                // this.getCorporateTier(response.data.singleCorporateMemberList.result.corporateId.corporateTierId);
                // console.log(response, 'response');
            }
        });
    };
    CorporateComponent.prototype.cu = function (cu) {
        throw new Error("Method not implemented.");
    };
    CorporateComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-corporate',
            template: __webpack_require__(/*! ./corporate.component.html */ "./src/app/components/corporate/corporate.component.html"),
            styles: [__webpack_require__(/*! ./corporate.component.css */ "./src/app/components/corporate/corporate.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_core_services_userService__WEBPACK_IMPORTED_MODULE_5__["UserAuthService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"],
            src_app_core_services_main_service__WEBPACK_IMPORTED_MODULE_4__["MainService"],
            src_app_core_services_global_global_event_manager_service__WEBPACK_IMPORTED_MODULE_2__["GlobalEventsManager"]])
    ], CorporateComponent);
    return CorporateComponent;
}());



/***/ }),

/***/ "./src/app/components/home/home.component.css":
/*!****************************************************!*\
  !*** ./src/app/components/home/home.component.css ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n\r\n@import url('https://fonts.googleapis.com/css?family=Hind+Madurai:300|Lora:400i|Montserrat:700');\n/* // Font settigns */\n.main{\r\n  display: flex;\r\n  flex-direction: row;\r\n  justify-content: center;\r\n  margin: 1rem;\r\n}\n.ticket {\r\n  height: 100%;\r\n}\n.pos {\r\n  height: 100%;\r\n}\n.content {\r\n  flex-wrap: wrap;\r\n}\n@media screen and (max-width: 300px) {\r\n  .pos {\r\n    width: 100%;\r\n  }\r\n  .ticket {\r\n    width: 100%;\r\n  }\r\n  .bottom-content {\r\n    width: 92%;\r\n  }\r\n}\n@media screen and (min-width: 500px) {\r\n  .pos {\r\n    width: 50%;\r\n  }\r\n  .ticket {\r\n    width: 50%;\r\n  }\r\n  .bottom-content {\r\n    width: 100%;\r\n  }\r\n}\n.text-align-center {\r\n  text-align: center;\r\n}\na {\r\n  text-decoration: none;\r\n}\nli {\r\n  list-style: none;\r\n}\n/* // Colors */\n/* // Flex settings */\n.flex {\r\n  display: flex;\r\n}\n.flex-direction-row {\r\n  flex-direction: row;\r\n}\n.flex-direction-column {\r\n  flex-direction: column;\r\n}\n.flex-wrap {\r\n  flex-wrap: wrap;\r\n}\n.align-items-center {\r\n  align-items: center;\r\n}\n.align-content-center {\r\n  align-content: center;\r\n}\n.justify-center {\r\n  justify-content: center;\r\n}\n.justify-between {\r\n  justify-content: space-between;\r\n}\n.justify-around {\r\n  justify-content: space-around;\r\n}\n/* // Margin & Padding settings */\n.add-margin {\r\n  margin: 1rem;\r\n}\n.add-padding {\r\n  padding: 1rem;\r\n}\n.padding-horizontal {\r\n  padding-left: 1rem;\r\n  padding-right: 1rem;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9ob21lL2hvbWUuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUVBLGdHQUFnRztBQUZoRyxxQkFBcUI7QUFJckI7RUFDRSxhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLHVCQUF1QjtFQUN2QixZQUFZO0FBQ2Q7QUFFQTtFQUNFLFlBQVk7QUFDZDtBQUVBO0VBQ0UsWUFBWTtBQUNkO0FBRUE7RUFDRSxlQUFlO0FBQ2pCO0FBRUE7RUFDRTtJQUNFLFdBQVc7RUFDYjtFQUNBO0lBQ0UsV0FBVztFQUNiO0VBQ0E7SUFDRSxVQUFVO0VBQ1o7QUFDRjtBQUVBO0VBQ0U7SUFDRSxVQUFVO0VBQ1o7RUFDQTtJQUNFLFVBQVU7RUFDWjtFQUNBO0lBQ0UsV0FBVztFQUNiO0FBQ0Y7QUFFQTtFQUNFLGtCQUFrQjtBQUNwQjtBQUVBO0VBQ0UscUJBQXFCO0FBQ3ZCO0FBRUE7RUFDRSxnQkFBZ0I7QUFDbEI7QUFFQSxjQUFjO0FBR2QscUJBQXFCO0FBRXJCO0VBQ0UsYUFBYTtBQUNmO0FBRUE7RUFDRSxtQkFBbUI7QUFDckI7QUFFQTtFQUNFLHNCQUFzQjtBQUN4QjtBQUNBO0VBQ0UsZUFBZTtBQUNqQjtBQUVBO0VBQ0UsbUJBQW1CO0FBQ3JCO0FBRUE7RUFDRSxxQkFBcUI7QUFDdkI7QUFFQTtFQUNFLHVCQUF1QjtBQUN6QjtBQUVBO0VBQ0UsOEJBQThCO0FBQ2hDO0FBRUE7RUFDRSw2QkFBNkI7QUFDL0I7QUFFQSxpQ0FBaUM7QUFFakM7RUFDRSxZQUFZO0FBQ2Q7QUFFQTtFQUNFLGFBQWE7QUFDZjtBQUVBO0VBQ0Usa0JBQWtCO0VBQ2xCLG1CQUFtQjtBQUNyQiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvaG9tZS9ob21lLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvKiAvLyBGb250IHNldHRpZ25zICovXHJcblxyXG5AaW1wb3J0IHVybCgnaHR0cHM6Ly9mb250cy5nb29nbGVhcGlzLmNvbS9jc3M/ZmFtaWx5PUhpbmQrTWFkdXJhaTozMDB8TG9yYTo0MDBpfE1vbnRzZXJyYXQ6NzAwJyk7XHJcblxyXG4ubWFpbntcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgbWFyZ2luOiAxcmVtO1xyXG59XHJcblxyXG4udGlja2V0IHtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuXHJcbi5wb3Mge1xyXG4gIGhlaWdodDogMTAwJTtcclxufVxyXG5cclxuLmNvbnRlbnQge1xyXG4gIGZsZXgtd3JhcDogd3JhcDtcclxufVxyXG5cclxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogMzAwcHgpIHtcclxuICAucG9zIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gIH1cclxuICAudGlja2V0IHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gIH1cclxuICAuYm90dG9tLWNvbnRlbnQge1xyXG4gICAgd2lkdGg6IDkyJTtcclxuICB9XHJcbn1cclxuXHJcbkBtZWRpYSBzY3JlZW4gYW5kIChtaW4td2lkdGg6IDUwMHB4KSB7XHJcbiAgLnBvcyB7XHJcbiAgICB3aWR0aDogNTAlO1xyXG4gIH1cclxuICAudGlja2V0IHtcclxuICAgIHdpZHRoOiA1MCU7XHJcbiAgfVxyXG4gIC5ib3R0b20tY29udGVudCB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICB9XHJcbn1cclxuXHJcbi50ZXh0LWFsaWduLWNlbnRlciB7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcblxyXG5hIHtcclxuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbn1cclxuXHJcbmxpIHtcclxuICBsaXN0LXN0eWxlOiBub25lO1xyXG59XHJcblxyXG4vKiAvLyBDb2xvcnMgKi9cclxuXHJcblxyXG4vKiAvLyBGbGV4IHNldHRpbmdzICovXHJcblxyXG4uZmxleCB7XHJcbiAgZGlzcGxheTogZmxleDtcclxufVxyXG5cclxuLmZsZXgtZGlyZWN0aW9uLXJvdyB7XHJcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcclxufVxyXG5cclxuLmZsZXgtZGlyZWN0aW9uLWNvbHVtbiB7XHJcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxufVxyXG4uZmxleC13cmFwIHtcclxuICBmbGV4LXdyYXA6IHdyYXA7XHJcbn1cclxuXHJcbi5hbGlnbi1pdGVtcy1jZW50ZXIge1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbn1cclxuXHJcbi5hbGlnbi1jb250ZW50LWNlbnRlciB7XHJcbiAgYWxpZ24tY29udGVudDogY2VudGVyO1xyXG59XHJcblxyXG4uanVzdGlmeS1jZW50ZXIge1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG59XHJcblxyXG4uanVzdGlmeS1iZXR3ZWVuIHtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbn1cclxuXHJcbi5qdXN0aWZ5LWFyb3VuZCB7XHJcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XHJcbn1cclxuXHJcbi8qIC8vIE1hcmdpbiAmIFBhZGRpbmcgc2V0dGluZ3MgKi9cclxuXHJcbi5hZGQtbWFyZ2luIHtcclxuICBtYXJnaW46IDFyZW07XHJcbn1cclxuXHJcbi5hZGQtcGFkZGluZyB7XHJcbiAgcGFkZGluZzogMXJlbTtcclxufVxyXG5cclxuLnBhZGRpbmctaG9yaXpvbnRhbCB7XHJcbiAgcGFkZGluZy1sZWZ0OiAxcmVtO1xyXG4gIHBhZGRpbmctcmlnaHQ6IDFyZW07XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/components/home/home.component.html":
/*!*****************************************************!*\
  !*** ./src/app/components/home/home.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" /> \r\n<div id=\"page-container\">\r\n  <app-dashboard-header></app-dashboard-header>\r\n      <div id=\"content-wrap\">\r\n\r\n            <div class=\"flex flex-direction-row content\">\r\n                \r\n              <div class=\"ticket\">\r\n                <app-ticket></app-ticket>\r\n              </div>\r\n            \r\n              <div class=\"pos\">\r\n                <app-pos></app-pos>\r\n              </div> \r\n              \r\n              <router-outlet></router-outlet>\r\n            </div>\r\n    </div>\r\n   </div>\r\n"

/***/ }),

/***/ "./src/app/components/home/home.component.ts":
/*!***************************************************!*\
  !*** ./src/app/components/home/home.component.ts ***!
  \***************************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_core_services_main_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/core/services/main.service */ "./src/app/core/services/main.service.ts");
/* harmony import */ var src_app_core_services_global_global_event_manager_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/core/services/global/global-event-manager.service */ "./src/app/core/services/global/global-event-manager.service.ts");
/* harmony import */ var src_app_core_services_userService__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/core/services/userService */ "./src/app/core/services/userService.ts");





var HomeComponent = /** @class */ (function () {
    function HomeComponent(userService, mainService, globalEventsManager) {
        var _this = this;
        this.userService = userService;
        this.mainService = mainService;
        this.globalEventsManager = globalEventsManager;
        this.corporates = [];
        this.showTierSection = false;
        this.userService.currentUser.subscribe(function (userData) {
            _this.currentUser = userData;
        });
    }
    HomeComponent.prototype.ngOnInit = function () {
    };
    HomeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! ./home.component.html */ "./src/app/components/home/home.component.html"),
            styles: [__webpack_require__(/*! ./home.component.css */ "./src/app/components/home/home.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_core_services_userService__WEBPACK_IMPORTED_MODULE_4__["UserAuthService"],
            src_app_core_services_main_service__WEBPACK_IMPORTED_MODULE_2__["MainService"],
            src_app_core_services_global_global_event_manager_service__WEBPACK_IMPORTED_MODULE_3__["GlobalEventsManager"]])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/components/pos-item/checkout.dialog.html":
/*!**********************************************************!*\
  !*** ./src/app/components/pos-item/checkout.dialog.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 mat-dialog-title>Төлбөрийн хэрэгсэлээ сонгоно уу!</h1>\r\n<!-- <div mat-dialog-content>\r\n  <p>What's your favorite animal?</p>\r\n</div> -->\r\n<div mat-dialog-actions>\r\n  <button mat-button [mat-dialog-close]=\"data.type[0]\" *ngIf=\"data.paymentType[0] == true\">Бэлнээр</button>\r\n  <button mat-button [mat-dialog-close]=\"data.type[1]\" *ngIf=\"data.paymentType[1] == true\">Банкны карт</button>\r\n  <button mat-button [mat-dialog-close]=\"data.type[2]\" *ngIf=\"data.paymentType[2] == true\">Маззи данс</button>\r\n  <button mat-button (click)=\"onNoClick()\">Болих</button>\r\n</div>"

/***/ }),

/***/ "./src/app/components/pos-item/keyframes.ts":
/*!**************************************************!*\
  !*** ./src/app/components/pos-item/keyframes.ts ***!
  \**************************************************/
/*! exports provided: swing */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "swing", function() { return swing; });
/* harmony import */ var _angular_animations__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/animations */ "./node_modules/@angular/animations/fesm5/animations.js");

var swing = [
    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ transform: 'rotate3d(0, 0, 1, 15deg)', offset: .2 }),
    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ transform: 'rotate3d(0, 0, 1, -10deg)', offset: .4 }),
    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ transform: 'rotate3d(0, 0, 1, 5deg)', offset: .6 }),
    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ transform: 'rotate3d(0, 0, 1, -5deg)', offset: .8 }),
    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({ transform: 'none', offset: 1 })
];


/***/ }),

/***/ "./src/app/components/pos-item/pos-item.component.css":
/*!************************************************************!*\
  !*** ./src/app/components/pos-item/pos-item.component.css ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".pos-section {\r\n  height: 92vh;\r\n}\r\n\r\n.menu {\r\n  margin: 0.25rem;\r\n}\r\n\r\n.tile{\r\n  background-color: #b7e2f3;\r\n  border-radius: 10px;\r\n  cursor: pointer;\r\n}\r\n\r\n.item-menu{\r\n  margin-top: 10px;\r\n  border-radius: 10px;\r\n  cursor: pointer;\r\n  background-color: #499bc0;\r\n}\r\n\r\n.item-menu-top{\r\n  border-radius: 10px;\r\n  cursor: pointer;\r\n  background-color: #499bc0;\r\n}\r\n\r\n.item-logout{\r\n  margin-top: 10px;\r\n  border-radius: 10px;\r\n  cursor: pointer;\r\n  background-color: #f4c343;\r\n}\r\n\r\n.item-pay{\r\n  margin-top: 10px;\r\n  border-radius: 10px;\r\n  cursor: pointer;\r\n  background-color: #a5c536;\r\n}\r\n\r\n.item-clear{\r\n  margin-top: 10px;\r\n  border-radius: 10px;\r\n  cursor: pointer;\r\n  background-color: #ea5255;\r\n}\r\n\r\n.item {\r\n  border-radius: 10px;\r\n  cursor: pointer;\r\n  background-color: #b7e2f3;\r\n}\r\n\r\n.item:hover {\r\n  background-color: #304ffe;\r\n  color: white;\r\n  opacity: 0.6;\r\n}\r\n\r\n.item:active {\r\n  background-color: grey;\r\n  color: white;\r\n  opacity: 0.8;\r\n}\r\n\r\n.item-img {\r\n  height: 100px;\r\n  width: 122px;\r\n  background-size: cover;\r\n  background-position: center;\r\n  border-radius: 10px;\r\n  border-bottom-left-radius: 0;\r\n  border-bottom-right-radius: 0;\r\n}\r\n\r\n.item-name {\r\n  -webkit-text-size-adjust: 5px;\r\n     -moz-text-size-adjust: 5px;\r\n      -ms-text-size-adjust: 5px;\r\n          text-size-adjust: 5px;\r\n  text-align: center;\r\n  color: black;\r\n  word-break: keep-all;\r\n}\r\n\r\n.item-price {\r\n  text-align: center;\r\n  margin-top: 1.5vh;\r\n  color: black;\r\n}\r\n\r\n.flex {\r\n  display: flex;\r\n}\r\n\r\n.flex-direction-row {\r\n  flex-direction: row;\r\n}\r\n\r\n.flex-direction-column {\r\n  flex-direction: column;\r\n}\r\n\r\n.flex-wrap {\r\n  flex-wrap: wrap;\r\n}\r\n\r\n.align-items-center {\r\n  align-items: center;\r\n}\r\n\r\n.align-content-center {\r\n  align-content: center;\r\n}\r\n\r\n.justify-center {\r\n  justify-content: center;\r\n}\r\n\r\n.justify-between {\r\n  justify-content: space-between;\r\n}\r\n\r\n.justify-around {\r\n  justify-content: space-around;\r\n}\r\n\r\n.tab{\r\n  height: 65vh;\r\n  overflow: auto;\r\n}\r\n\r\n.menus{\r\n  overflow: auto;\r\n  height: 25vh;\r\n}\r\n\r\n.tab-items{\r\n  overflow: auto;\r\n  height: 55vh;\r\n}\r\n\r\n.tab-types{\r\n  overflow-y: auto;\r\n  overflow-x: auto;\r\n  height: 80px;\r\n}\r\n\r\n.grid-list1{\r\n  margin-top: 10px;\r\n}\r\n\r\n\r\n\r\n\r\n\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9wb3MtaXRlbS9wb3MtaXRlbS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsWUFBWTtBQUNkOztBQUVBO0VBQ0UsZUFBZTtBQUNqQjs7QUFFQTtFQUNFLHlCQUF5QjtFQUN6QixtQkFBbUI7RUFDbkIsZUFBZTtBQUNqQjs7QUFFQTtFQUNFLGdCQUFnQjtFQUNoQixtQkFBbUI7RUFDbkIsZUFBZTtFQUNmLHlCQUF5QjtBQUMzQjs7QUFFQTtFQUNFLG1CQUFtQjtFQUNuQixlQUFlO0VBQ2YseUJBQXlCO0FBQzNCOztBQUVBO0VBQ0UsZ0JBQWdCO0VBQ2hCLG1CQUFtQjtFQUNuQixlQUFlO0VBQ2YseUJBQXlCO0FBQzNCOztBQUVBO0VBQ0UsZ0JBQWdCO0VBQ2hCLG1CQUFtQjtFQUNuQixlQUFlO0VBQ2YseUJBQXlCO0FBQzNCOztBQUVBO0VBQ0UsZ0JBQWdCO0VBQ2hCLG1CQUFtQjtFQUNuQixlQUFlO0VBQ2YseUJBQXlCO0FBQzNCOztBQUVBO0VBQ0UsbUJBQW1CO0VBQ25CLGVBQWU7RUFDZix5QkFBeUI7QUFDM0I7O0FBQ0E7RUFDRSx5QkFBeUI7RUFDekIsWUFBWTtFQUNaLFlBQVk7QUFDZDs7QUFDQTtFQUNFLHNCQUFzQjtFQUN0QixZQUFZO0VBQ1osWUFBWTtBQUNkOztBQUVBO0VBQ0UsYUFBYTtFQUNiLFlBQVk7RUFDWixzQkFBc0I7RUFDdEIsMkJBQTJCO0VBQzNCLG1CQUFtQjtFQUNuQiw0QkFBNEI7RUFDNUIsNkJBQTZCO0FBQy9COztBQUVBO0VBQ0UsNkJBQXFCO0tBQXJCLDBCQUFxQjtNQUFyQix5QkFBcUI7VUFBckIscUJBQXFCO0VBQ3JCLGtCQUFrQjtFQUNsQixZQUFZO0VBQ1osb0JBQW9CO0FBQ3RCOztBQUVBO0VBQ0Usa0JBQWtCO0VBQ2xCLGlCQUFpQjtFQUNqQixZQUFZO0FBQ2Q7O0FBRUE7RUFDRSxhQUFhO0FBQ2Y7O0FBRUE7RUFDRSxtQkFBbUI7QUFDckI7O0FBRUE7RUFDRSxzQkFBc0I7QUFDeEI7O0FBQ0E7RUFDRSxlQUFlO0FBQ2pCOztBQUVBO0VBQ0UsbUJBQW1CO0FBQ3JCOztBQUVBO0VBQ0UscUJBQXFCO0FBQ3ZCOztBQUVBO0VBQ0UsdUJBQXVCO0FBQ3pCOztBQUVBO0VBQ0UsOEJBQThCO0FBQ2hDOztBQUVBO0VBQ0UsNkJBQTZCO0FBQy9COztBQUVBO0VBQ0UsWUFBWTtFQUNaLGNBQWM7QUFDaEI7O0FBRUE7RUFDRSxjQUFjO0VBQ2QsWUFBWTtBQUNkOztBQUVBO0VBQ0UsY0FBYztFQUNkLFlBQVk7QUFDZDs7QUFFQTtFQUNFLGdCQUFnQjtFQUNoQixnQkFBZ0I7RUFDaEIsWUFBWTtBQUNkOztBQUVBO0VBQ0UsZ0JBQWdCO0FBQ2xCIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9wb3MtaXRlbS9wb3MtaXRlbS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnBvcy1zZWN0aW9uIHtcclxuICBoZWlnaHQ6IDkydmg7XHJcbn1cclxuXHJcbi5tZW51IHtcclxuICBtYXJnaW46IDAuMjVyZW07XHJcbn1cclxuXHJcbi50aWxle1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNiN2UyZjM7XHJcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcclxuICBjdXJzb3I6IHBvaW50ZXI7XHJcbn1cclxuXHJcbi5pdGVtLW1lbnV7XHJcbiAgbWFyZ2luLXRvcDogMTBweDtcclxuICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4gIGN1cnNvcjogcG9pbnRlcjtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNDk5YmMwO1xyXG59XHJcblxyXG4uaXRlbS1tZW51LXRvcHtcclxuICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4gIGN1cnNvcjogcG9pbnRlcjtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNDk5YmMwO1xyXG59XHJcblxyXG4uaXRlbS1sb2dvdXR7XHJcbiAgbWFyZ2luLXRvcDogMTBweDtcclxuICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4gIGN1cnNvcjogcG9pbnRlcjtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjRjMzQzO1xyXG59XHJcblxyXG4uaXRlbS1wYXl7XHJcbiAgbWFyZ2luLXRvcDogMTBweDtcclxuICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4gIGN1cnNvcjogcG9pbnRlcjtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjYTVjNTM2O1xyXG59XHJcblxyXG4uaXRlbS1jbGVhcntcclxuICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgY3Vyc29yOiBwb2ludGVyO1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNlYTUyNTU7XHJcbn1cclxuXHJcbi5pdGVtIHtcclxuICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4gIGN1cnNvcjogcG9pbnRlcjtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjYjdlMmYzO1xyXG59XHJcbi5pdGVtOmhvdmVyIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzA0ZmZlO1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxuICBvcGFjaXR5OiAwLjY7XHJcbn1cclxuLml0ZW06YWN0aXZlIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiBncmV5O1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxuICBvcGFjaXR5OiAwLjg7XHJcbn1cclxuXHJcbi5pdGVtLWltZyB7XHJcbiAgaGVpZ2h0OiAxMDBweDtcclxuICB3aWR0aDogMTIycHg7XHJcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XHJcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcclxuICBib3JkZXItYm90dG9tLWxlZnQtcmFkaXVzOiAwO1xyXG4gIGJvcmRlci1ib3R0b20tcmlnaHQtcmFkaXVzOiAwO1xyXG59XHJcblxyXG4uaXRlbS1uYW1lIHtcclxuICB0ZXh0LXNpemUtYWRqdXN0OiA1cHg7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIGNvbG9yOiBibGFjaztcclxuICB3b3JkLWJyZWFrOiBrZWVwLWFsbDtcclxufVxyXG5cclxuLml0ZW0tcHJpY2Uge1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBtYXJnaW4tdG9wOiAxLjV2aDtcclxuICBjb2xvcjogYmxhY2s7XHJcbn1cclxuXHJcbi5mbGV4IHtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG59XHJcblxyXG4uZmxleC1kaXJlY3Rpb24tcm93IHtcclxuICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG59XHJcblxyXG4uZmxleC1kaXJlY3Rpb24tY29sdW1uIHtcclxuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG59XHJcbi5mbGV4LXdyYXAge1xyXG4gIGZsZXgtd3JhcDogd3JhcDtcclxufVxyXG5cclxuLmFsaWduLWl0ZW1zLWNlbnRlciB7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxufVxyXG5cclxuLmFsaWduLWNvbnRlbnQtY2VudGVyIHtcclxuICBhbGlnbi1jb250ZW50OiBjZW50ZXI7XHJcbn1cclxuXHJcbi5qdXN0aWZ5LWNlbnRlciB7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbn1cclxuXHJcbi5qdXN0aWZ5LWJldHdlZW4ge1xyXG4gIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxufVxyXG5cclxuLmp1c3RpZnktYXJvdW5kIHtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWFyb3VuZDtcclxufVxyXG5cclxuLnRhYntcclxuICBoZWlnaHQ6IDY1dmg7XHJcbiAgb3ZlcmZsb3c6IGF1dG87XHJcbn1cclxuXHJcbi5tZW51c3tcclxuICBvdmVyZmxvdzogYXV0bztcclxuICBoZWlnaHQ6IDI1dmg7XHJcbn1cclxuXHJcbi50YWItaXRlbXN7XHJcbiAgb3ZlcmZsb3c6IGF1dG87XHJcbiAgaGVpZ2h0OiA1NXZoO1xyXG59XHJcblxyXG4udGFiLXR5cGVze1xyXG4gIG92ZXJmbG93LXk6IGF1dG87XHJcbiAgb3ZlcmZsb3cteDogYXV0bztcclxuICBoZWlnaHQ6IDgwcHg7XHJcbn1cclxuXHJcbi5ncmlkLWxpc3Qxe1xyXG4gIG1hcmdpbi10b3A6IDEwcHg7XHJcbn1cclxuXHJcblxyXG5cclxuXHJcblxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/components/pos-item/pos-item.component.html":
/*!*************************************************************!*\
  !*** ./src/app/components/pos-item/pos-item.component.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n\r\n<mat-card class=\"pos-section\">\r\n  <!-- <mat-card-title class=\"add-padding heading\">Бараанууд</mat-card-title> -->\r\n  \r\n  <!-- <mat-tab-group class=\"tab\">\r\n    <mat-tab *ngFor=\"let type of products; let i = index\" label=\"{{productTypes[i].name}}\">\r\n          <mat-grid-list cols=\"6\" rowHeight=\"80\" class=\"grid-list\" [gutterSize]=\"'10px'\">\r\n            <mat-grid-tile *ngFor=\"let posItem of products[i]\" class=\"flex item\" (click)=\"addToCheck(posItem)\" (tap)=\"startAnimation('swing')\" >\r\n              <div class=\"flex-wrap justify-center\">\r\n                  <p class=\" item-name\">{{posItem.name}}</p>\r\n                  <p class=\" item-price\">{{posItem.price}}</p>\r\n              </div>\r\n            </mat-grid-tile>\r\n          </mat-grid-list>\r\n        </mat-tab>\r\n</mat-tab-group> -->\r\n\r\n\r\n<div class=\"tab\">\r\n\r\n  <div class=\"tab-types\">\r\n  <mat-grid-list cols=\"6\" rowHeight=\"80\" [gutterSize]=\"'10px'\">\r\n      <mat-grid-tile *ngFor=\"let type of products; let i = index\" class=\"flex item-menu-top\" (click)=\"changeProducts(i)\">\r\n        <div class=\"flex-wrap justify-center\">\r\n            <p class=\"flex item-name\">{{productTypes[i].name}}</p>\r\n        </div>   \r\n      </mat-grid-tile>\r\n    </mat-grid-list> \r\n  </div>\r\n\r\n  \r\n    <div class=\"tab-items\">\r\n    <mat-grid-list cols=\"6\" rowHeight=\"80\" class=\"grid-list1\" [gutterSize]=\"'10px'\">\r\n      <mat-grid-tile *ngFor=\"let posItem of currentProducts\" class=\"flex item\" (click)=\"addToCheck(posItem)\" (tap)=\"startAnimation('swing')\" >\r\n        <div class=\"flex-wrap justify-center\">\r\n            <p class=\" item-name\">{{posItem.name}}</p>\r\n            <p class=\" item-price\">{{posItem.price}}</p>\r\n        </div>\r\n      </mat-grid-tile>\r\n    </mat-grid-list>\r\n  </div>\r\n\r\n  </div>\r\n  \r\n  \r\n\r\n\r\n<div class=\"menus\">\r\n    <p>jani : {{jani}}</p>\r\n    <p>jani0 : {{jani0}}</p>\r\n    <p>jani1 : {{jani1}}</p>\r\n    <p>jani2 : {{jani2}}</p>\r\n    <p>jani3 : {{jani3}}</p>\r\n<mat-grid-list cols=\"6\" rowHeight=\"80\" class=\"grid-list\" [gutterSize]=\"'10px'\">\r\n    <mat-grid-tile class=\"flex item-menu\">\r\n      <h5 class=\"flex item-name\">Сүүлийн гүйлгээ</h5>\r\n      \r\n    </mat-grid-tile>\r\n    <mat-grid-tile class=\"flex item-menu\">\r\n      <h5 class=\"flex item-name\" (click)=\"dailyReport()\">Гүйлгээ хаах</h5>\r\n    </mat-grid-tile>\r\n    <mat-grid-tile class=\"flex item-menu\">\r\n      <h5 class=\"flex item-name\">Гүйлгээний түүх</h5>\r\n    </mat-grid-tile>\r\n       \r\n  </mat-grid-list>\r\n\r\n  <mat-grid-list cols=\"6\" rowHeight=\"100\" class=\"grid-list1\" [gutterSize]=\"'10px'\">\r\n    <mat-grid-tile class=\"flex item-logout\" (click)=\"logout()\">\r\n      <h5 class=\"flex item-name\">Хэрэглэгч солих</h5>\r\n    </mat-grid-tile>\r\n    <mat-grid-tile class=\"flex item-clear\" [colspan]=2 (click)=\"clearCart()\">\r\n      <h3 class=\"flex item-name\">Арилгах</h3>\r\n    </mat-grid-tile>\r\n    <mat-grid-tile class=\"flex item-pay\" [colspan]=3 (click)=\"openDialog()\">\r\n      <h3 class=\"flex item-name\">Зарах</h3>\r\n    </mat-grid-tile>\r\n       \r\n  </mat-grid-list>\r\n</div>\r\n\r\n\r\n  <!-- <div id=\"Home\" class=\"tabcontent\">\r\n      <h3>Home</h3>\r\n      <p>Home is where the heart is..</p>\r\n    </div>\r\n    \r\n    <div id=\"News\" class=\"tabcontent\">\r\n      <h3>News</h3>\r\n      <p>Some news this fine day!</p> \r\n    </div> --> \r\n\r\n</mat-card>\r\n\r\n<script>\r\n    function openPage(pageName) {\r\n      var i, tabcontent, tablinks;\r\n      tabcontent = document.getElementsByClassName(\"tabcontent\");\r\n      for (i = 0; i < tabcontent.length; i++) {\r\n        tabcontent[i].style.display = \"none\";\r\n      }\r\n     \r\n      document.getElementById(pageName).style.display = \"block\";\r\n    }\r\n    \r\n    // Get the element with id=\"defaultOpen\" and click on it\r\n    // document.getElementById(\"defaultOpen\").click();\r\n    </script>\r\n\r\n"

/***/ }),

/***/ "./src/app/components/pos-item/pos-item.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/components/pos-item/pos-item.component.ts ***!
  \***********************************************************/
/*! exports provided: PosItemComponent, CheckoutDialog */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PosItemComponent", function() { return PosItemComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CheckoutDialog", function() { return CheckoutDialog; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var apollo_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! apollo-angular */ "./node_modules/apollo-angular/fesm5/ng.apollo.js");
/* harmony import */ var src_app_core_services_pos_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/core/services/pos.service */ "./src/app/core/services/pos.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var src_app_core_queries_pos_query__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/core/queries/pos.query */ "./src/app/core/queries/pos.query.ts");
/* harmony import */ var _angular_animations__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/animations */ "./node_modules/@angular/animations/fesm5/animations.js");
/* harmony import */ var _keyframes__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./keyframes */ "./src/app/components/pos-item/keyframes.ts");
/* harmony import */ var src_app_core_services_userService__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/core/services/userService */ "./src/app/core/services/userService.ts");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");











var PosItemComponent = /** @class */ (function () {
    function PosItemComponent(apollo, ticketSync, route, router, userService, dialog) {
        var _this = this;
        this.apollo = apollo;
        this.ticketSync = ticketSync;
        this.route = route;
        this.router = router;
        this.userService = userService;
        this.dialog = dialog;
        this.corporateId = "5ccaa08ab545031b446629af";
        this.userId = "5c4936057565dd0012a958f6";
        this.posItems = [];
        this.loading = true;
        this.products = [];
        this.cartTotal = 0;
        this.cartNumItems = 0;
        this.inputBillItems = [];
        this.type = ['cash', 'card', 'mazziCard'];
        this.currentPaymentType = [true, true, true];
        this.jani = 0;
        this.jani0 = 0;
        this.jani1 = 0;
        this.jani2 = 0;
        this.jani3 = false;
        this.userService.currentUser.subscribe(function (userData) {
            _this.currentUser = userData;
        });
        // this.ticketSync.currentPosId.subscribe(posId => this.posId = posId);
        this.ticketSync.currentPurchaseType.subscribe(function (paymentType) { return _this.paymentType = paymentType; });
        try {
            this.currentPaymentType[0] = this.paymentType.cash;
            this.currentPaymentType[1] = this.paymentType.card;
            this.currentPaymentType[2] = this.paymentType.mazziCard;
        }
        catch (error) {
            console.log(error);
        }
    }
    PosItemComponent.prototype.ngOnInit = function () {
        var _this = this;
        // this.userService.currentUser.subscribe(
        //   (userData) => {
        //     this.currentUser = userData;
        //   }
        // );
        this.ticketSync.currentTicket.subscribe(function (data) { return _this.ticket = data; });
        this.ticketSync.currentTotal.subscribe(function (total) { return _this.cartTotal = total; });
        this.ticketSync.currentCartNum.subscribe(function (num) { return _this.cartNumItems; });
        // this.ticketSync.currentPosId.subscribe(posId => this.posId = posId);
        this.getPosItemTypes();
        // this.getPosesPosItems();
        this.currentCorporateId = this.route.snapshot.paramMap.get('_id');
        this.posId = this.route.snapshot.paramMap.get('id2');
        window.onCalledCsharp = function (data) {
            alert('Data from c#: ' + data);
        };
    };
    PosItemComponent.prototype.startAnimation = function (state) {
        console.log(state);
        if (!this.animationState) {
            this.animationState = state;
        }
    };
    PosItemComponent.prototype.resetAnimationState = function () {
        this.animationState = '';
    };
    PosItemComponent.prototype.nfcCall = function () {
        window.external.nfcConnect("hello c# i am web");
    };
    PosItemComponent.prototype.addToCheck = function (item) {
        // If the item already exists, add 1 to quantity
        //jani commented
        // if (this.ticket.includes(item)) {
        //   this.ticket[this.ticket.indexOf(item)].quantity += 1;
        // } else {
        //   item.quantity = 1;
        //   this.ticket.push(item);
        // }
        //jani added
        if (this.ticket.length === 0) {
            this.jani0 += 1;
            item.quantity = 1;
            this.ticket.push(item);
        }
        else {
            if (this.includes(item)) {
                this.jani1 += 1;
                this.ticket[this.ticket.indexOf(item)].quantity += 1;
            }
            else {
                this.jani2 += 1;
                item.quantity = 1;
                this.ticket.push(item);
            }
            // for (let i=0; i<this.ticket.length; i++){
            //   if (this.ticket[i]._id === item._id){
            //     this.ticket[i].quantity += 1;
            //     this.jani1 += 1;
            //   }else {
            //     this.jani2 += 1;
            //     item.quantity = 1;
            //     this.ticket.push(item);
            //   }
            // }
        }
        this.calculateTotal();
    };
    // includes(value){
    //   for (let i=0; i<this.ticket.length; i++){
    //     if (this.ticket[i]._id === value._id){
    //       return true;
    //     }else{
    //       return false;
    //     }
    //   }
    // }
    PosItemComponent.prototype.includes = function (value) {
        var returnValue = false;
        var pos = this.ticket.indexOf(value);
        if (pos >= 0) {
            returnValue = true;
        }
        return returnValue;
    };
    PosItemComponent.prototype.getPosItems = function (itemType, index) {
        var _this = this;
        this.currentCorporateId = this.route.snapshot.paramMap.get('_id');
        this.apollo
            .query({
            query: src_app_core_queries_pos_query__WEBPACK_IMPORTED_MODULE_5__["posItemsQuery"],
            variables: {
                posItemTypeId: itemType,
                corporateId: this.currentCorporateId
            }
        })
            .subscribe(function (result) {
            _this.products[index] = result.data && result.data.posItems.result;
            _this.loading = result.loading;
            _this.error = result.errors;
            _this.currentProducts = _this.products[0];
        });
    };
    PosItemComponent.prototype.getPosesPosItems = function () {
        var _this = this;
        console.log("currentPosId = " + this.posId);
        this.apollo
            .query({
            query: src_app_core_queries_pos_query__WEBPACK_IMPORTED_MODULE_5__["posesPosItems"],
            variables: {
                userId: this.currentUser._id,
                posId: this.posId
            }
        })
            .subscribe(function (result) {
            _this.posItems = result.data && result.data.posesPosItems.result;
            console.log("posesPosItems = ", result.data);
            _this.setProducts();
        });
    };
    PosItemComponent.prototype.getPosItemTypes = function () {
        var _this = this;
        this.currentCorporateId = this.route.snapshot.paramMap.get('_id');
        this.apollo
            .query({
            query: src_app_core_queries_pos_query__WEBPACK_IMPORTED_MODULE_5__["posItemTypesQuery"],
            variables: {
                corporateId: this.currentCorporateId
            }
        })
            .subscribe(function (_a) {
            var data = _a.data;
            _this.productTypes = data && data.posItemTypes.result;
            console.log('got data = ' + data);
            // this.loading = result.loading;
            // this.error = result.errors;
            // for(let i = 0; i < this.productTypes.length; i++){
            //   console.log('posItemType = ' + this.productTypes[i].name);
            //   this.getPosItems(this.productTypes[i]._id,i);
            // }
            // this.currentProducts = this.products[0];
            _this.getPosesPosItems();
        });
    };
    PosItemComponent.prototype.setProducts = function () {
        for (var j = 0; j < this.productTypes.length; j++) {
            this.products[j] = [];
        }
        for (var i = 0; i < this.posItems.length; i++) {
            for (var j = 0; j < this.productTypes.length; j++) {
                if (this.posItems[i].posItemTypeId === this.productTypes[j]._id) {
                    this.products[j].push(this.posItems[i]);
                }
            }
        }
        this.currentProducts = this.products[0];
    };
    PosItemComponent.prototype.dailyReport = function () {
        this.currentCorporateId = this.route.snapshot.paramMap.get('_id');
        console.log("corporateId = " + this.currentCorporateId + " userId = " + this.currentUser._id);
        this.apollo
            .mutate({
            mutation: src_app_core_queries_pos_query__WEBPACK_IMPORTED_MODULE_5__["dailyInsertPosReportMutation"],
            variables: {
                corporateId: this.currentCorporateId,
                userId: this.currentUser._id,
                posId: this.posId
            }
        })
            .subscribe(function (_a) {
            var data = _a.data;
            console.log('dailyReport', data);
            sweetalert2__WEBPACK_IMPORTED_MODULE_9___default.a.fire('Амжилттай', '', 'success');
        }, function (error) {
            console.log('failed', error);
        });
    };
    PosItemComponent.prototype.logout = function () {
        var _this = this;
        console.log("logout called");
        this.userService.logout(this.currentUser._id).subscribe(function (response) {
            if (response.data.logout.error == false) {
                _this.userService.purgeAuth();
                _this.router.navigateByUrl('/login');
            }
        });
    };
    PosItemComponent.prototype.calculateTotal = function () {
        var total = 0;
        var cartitems = 0;
        // Multiply item price by item quantity, add to total
        this.ticket.forEach(function (item) {
            total += (item.price * item.quantity);
            cartitems += item.quantity;
        });
        this.cartTotal = total;
        this.cartNumItems = cartitems;
        // Sync total with ticketSync service.
        this.ticketSync.updateNumItems(this.cartNumItems);
        this.ticketSync.updateTotal(this.cartTotal);
    };
    // Remove all items from cart
    PosItemComponent.prototype.clearCart = function () {
        console.log('clear called');
        this.inputBillItems = [];
        // Reduce back to initial quantity (1 vs 0 for re-add)
        this.ticket.forEach(function (item) {
            item.quantity = 1;
        });
        // Empty local ticket variable then sync
        this.ticket = [];
        this.syncTicket();
        this.calculateTotal();
    };
    PosItemComponent.prototype.syncTicket = function () {
        this.ticketSync.changeTicket(this.ticket);
    };
    PosItemComponent.prototype.checkout = function (type) {
        console.log("type=", type);
        if (this.ticket.length > 0) {
            for (var i = 0; i < this.ticket.length; i++) {
                this.inputBillItems.push({ posItemId: this.ticket[i]._id, quantity: this.ticket[i].quantity });
            }
            this.insertBill(this.inputBillItems, type);
            this.clearCart();
        }
    };
    PosItemComponent.prototype.insertBill = function (inputBillItems, type) {
        this.currentCorporateId = this.route.snapshot.paramMap.get('_id');
        this.apollo.mutate({
            mutation: src_app_core_queries_pos_query__WEBPACK_IMPORTED_MODULE_5__["insertBillMutation"],
            variables: {
                userId: this.currentUser._id,
                corporateId: this.currentCorporateId,
                inputBillItems: inputBillItems,
                purchaseType: type,
                subjectUserId: this.currentUser._id,
                posId: this.posId
            }
        }).subscribe(function (_a) {
            var data = _a.data;
            console.log('got dataaaaa ', data);
        }, function (error) {
            console.log('failed', error);
        });
    };
    PosItemComponent.prototype.changeProducts = function (index) {
        console.log("changeProducts called");
        this.currentProducts = this.products[index];
    };
    PosItemComponent.prototype.openDialog = function () {
        var _this = this;
        var dialogRef = this.dialog.open(CheckoutDialog, {
            width: '500px',
            data: { type: this.type,
                paymentType: this.currentPaymentType }
        });
        dialogRef.afterClosed().subscribe(function (result) {
            _this.checkout(result);
        });
    };
    PosItemComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-pos',
            template: __webpack_require__(/*! ./pos-item.component.html */ "./src/app/components/pos-item/pos-item.component.html"),
            animations: [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_6__["trigger"])('cardAnimator', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_6__["transition"])('* => swing', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_6__["animate"])(1000, Object(_angular_animations__WEBPACK_IMPORTED_MODULE_6__["keyframes"])(_keyframes__WEBPACK_IMPORTED_MODULE_7__["swing"]))),
                ])
            ],
            encapsulation: 2,
            styles: [__webpack_require__(/*! ./pos-item.component.css */ "./src/app/components/pos-item/pos-item.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [apollo_angular__WEBPACK_IMPORTED_MODULE_2__["Apollo"],
            src_app_core_services_pos_service__WEBPACK_IMPORTED_MODULE_3__["PosService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_10__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_10__["Router"],
            src_app_core_services_userService__WEBPACK_IMPORTED_MODULE_8__["UserAuthService"],
            _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatDialog"]])
    ], PosItemComponent);
    return PosItemComponent;
}());

var CheckoutDialog = /** @class */ (function () {
    function CheckoutDialog(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
    }
    CheckoutDialog.prototype.ngOnInit = function () {
        // throw new Error("Method not implemented.");
    };
    CheckoutDialog.prototype.onNoClick = function () {
        this.dialogRef.close();
    };
    CheckoutDialog = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-checkout-dialog',
            template: __webpack_require__(/*! ./checkout.dialog.html */ "./src/app/components/pos-item/checkout.dialog.html"),
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_4__["MAT_DIALOG_DATA"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_4__["MatDialogRef"], Object])
    ], CheckoutDialog);
    return CheckoutDialog;
}());



/***/ }),

/***/ "./src/app/components/ticket/ticket.component.css":
/*!********************************************************!*\
  !*** ./src/app/components/ticket/ticket.component.css ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n.ticket-section {\r\n  height: 92vh;\r\n}\r\n\r\n.heading {\r\n}\r\n\r\n.table-header {\r\n  text-align: left;\r\n  font-weight: 700;\r\n}\r\n\r\n.items {\r\n  width: 100%;\r\n}\r\n\r\n.pos-items{\r\n  height: 80vh;\r\n  overflow: auto;\r\n\r\n}\r\n\r\n.del {\r\n  color: grey;\r\n}\r\n\r\n.del:hover {\r\n  color: black;\r\n  cursor: pointer;\r\n}\r\n\r\n.summary {\r\n  height: 10vh;\r\n  margin-top: auto;\r\n}\r\n\r\n.total {\r\n  color: black;\r\n  font-weight: bold;\r\n  padding: 0.75rem;\r\n}\r\n\r\nth{\r\n  background-color: #e0e0e0;\r\n}\r\n\r\ntd, th {\r\n  padding: 0.5rem;\r\n  vertical-align: middle;\r\n}\r\n\r\ntr:nth-child(even) {\r\n  background-color: #f5f5f5;\r\n}\r\n\r\n.clear{\r\n  background-color: #ea5255;\r\n}\r\n\r\n.checkout {\r\n  margin-right: 0.75rem;\r\n  background-color: #a5c536;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy90aWNrZXQvdGlja2V0LmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUNBO0VBQ0UsWUFBWTtBQUNkOztBQUVBO0FBQ0E7O0FBRUE7RUFDRSxnQkFBZ0I7RUFDaEIsZ0JBQWdCO0FBQ2xCOztBQUVBO0VBQ0UsV0FBVztBQUNiOztBQUVBO0VBQ0UsWUFBWTtFQUNaLGNBQWM7O0FBRWhCOztBQUVBO0VBQ0UsV0FBVztBQUNiOztBQUNBO0VBQ0UsWUFBWTtFQUNaLGVBQWU7QUFDakI7O0FBR0E7RUFDRSxZQUFZO0VBQ1osZ0JBQWdCO0FBQ2xCOztBQUVBO0VBQ0UsWUFBWTtFQUNaLGlCQUFpQjtFQUNqQixnQkFBZ0I7QUFDbEI7O0FBR0E7RUFDRSx5QkFBeUI7QUFDM0I7O0FBRUE7RUFDRSxlQUFlO0VBQ2Ysc0JBQXNCO0FBQ3hCOztBQUVBO0VBQ0UseUJBQXlCO0FBQzNCOztBQUVBO0VBQ0UseUJBQXlCO0FBQzNCOztBQUVBO0VBQ0UscUJBQXFCO0VBQ3JCLHlCQUF5QjtBQUMzQiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvdGlja2V0L3RpY2tldC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiXHJcbi50aWNrZXQtc2VjdGlvbiB7XHJcbiAgaGVpZ2h0OiA5MnZoO1xyXG59XHJcblxyXG4uaGVhZGluZyB7XHJcbn1cclxuXHJcbi50YWJsZS1oZWFkZXIge1xyXG4gIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgZm9udC13ZWlnaHQ6IDcwMDtcclxufVxyXG5cclxuLml0ZW1zIHtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuLnBvcy1pdGVtc3tcclxuICBoZWlnaHQ6IDgwdmg7XHJcbiAgb3ZlcmZsb3c6IGF1dG87XHJcblxyXG59XHJcblxyXG4uZGVsIHtcclxuICBjb2xvcjogZ3JleTtcclxufVxyXG4uZGVsOmhvdmVyIHtcclxuICBjb2xvcjogYmxhY2s7XHJcbiAgY3Vyc29yOiBwb2ludGVyO1xyXG59XHJcblxyXG5cclxuLnN1bW1hcnkge1xyXG4gIGhlaWdodDogMTB2aDtcclxuICBtYXJnaW4tdG9wOiBhdXRvO1xyXG59XHJcblxyXG4udG90YWwge1xyXG4gIGNvbG9yOiBibGFjaztcclxuICBmb250LXdlaWdodDogYm9sZDtcclxuICBwYWRkaW5nOiAwLjc1cmVtO1xyXG59XHJcblxyXG5cclxudGh7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2UwZTBlMDtcclxufVxyXG5cclxudGQsIHRoIHtcclxuICBwYWRkaW5nOiAwLjVyZW07XHJcbiAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcclxufVxyXG5cclxudHI6bnRoLWNoaWxkKGV2ZW4pIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjVmNWY1O1xyXG59XHJcblxyXG4uY2xlYXJ7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2VhNTI1NTtcclxufVxyXG5cclxuLmNoZWNrb3V0IHtcclxuICBtYXJnaW4tcmlnaHQ6IDAuNzVyZW07XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2E1YzUzNjtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/components/ticket/ticket.component.html":
/*!*********************************************************!*\
  !*** ./src/app/components/ticket/ticket.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-card class=\"ticket-section flex flex-direction-column justify-between\">\r\n  <!-- <h6>Хэрэглэгчийн нэр: {{currentUser.nickname}}</h6> -->\r\n    <!-- <mat-card-title class=\"add-padding heading\">Захиалга</mat-card-title> -->\r\n  <div class=\"pos-items\">\r\n  <table class=\"items\">\r\n    <thead>\r\n    <tr class=\"table-header\">\r\n      <th>Тоо ширхэг</th>\r\n      <th>Бараа</th>\r\n      <th>Үнэ</th>\r\n      <th>Засах</th>\r\n    </tr>\r\n  </thead>\r\n\r\n    <tr *ngFor=\"let item of ticket\">\r\n      <td>{{item.quantity}}</td>\r\n      <td>{{item.name}}</td>\r\n      <td>₮{{item.price}}</td>\r\n      <td>\r\n        <mat-icon (click)=\"addItem(item)\" class=\"del\">control_point</mat-icon>\r\n        <mat-icon (click)=\"subtractOne(item)\" class=\"del\">indeterminate_check_box</mat-icon>\r\n        <mat-icon (click)=\"removeItem(item)\" class=\"del\">cancel</mat-icon></td>\r\n    </tr>\r\n  </table>\r\n\r\n  <!-- <table mat-table [dataSource]=\"dataSource\" class=\"items\">\r\n    <ng-container matColumnDef=\"quantity\">\r\n      <th mat-header-cell *matHeaderCellDef> Тоо ширхэг </th>\r\n      <td mat-cell *matCellDef=\"let item\"> {{item.quantity}} </td>\r\n    </ng-container>\r\n\r\n    <ng-container matColumnDef=\"name\">\r\n      <th mat-header-cell *matHeaderCellDef> Бараа </th>\r\n      <td mat-cell *matCellDef=\"let item\"> {{item.name}} </td>\r\n    </ng-container>\r\n\r\n    <ng-container matColumnDef=\"price\">\r\n      <th mat-header-cell *matHeaderCellDef> Үнэ </th>\r\n      <td mat-cell *matCellDef=\"let item\"> {{item.price}} </td>\r\n    </ng-container>\r\n\r\n    <tr mat-header-row *matHeaderRowDef=\"displayedColumns; sticky: true\"></tr>\r\n    <tr mat-row *matRowDef=\"let row; columns: displayedColumns;\"></tr>\r\n  </table> -->\r\n\r\n</div>\r\n\r\n  <mat-card-actions class=\"summary flex flex-direction-row align-items-center justify-between\">\r\n    <!-- <h3 class=\"total\">Total: {{cartTotal | currency:'USD':true}}</h3> -->\r\n    <h2 class=\"total\">Нийт үнэ:₮ {{cartTotal}}</h2>\r\n\r\n    <!-- <div class=\"flex flex-direction-row buttons\">\r\n      <button class=\"clear\" mat-raised-button (click)=\"clearCart()\">Арилгах</button>\r\n      <button class=\"checkout\" mat-raised-button (click)=\"openDialog()\">Зарах</button>\r\n    </div> -->\r\n  </mat-card-actions>\r\n</mat-card>\r\n"

/***/ }),

/***/ "./src/app/components/ticket/ticket.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/components/ticket/ticket.component.ts ***!
  \*******************************************************/
/*! exports provided: TicketComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TicketComponent", function() { return TicketComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var apollo_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! apollo-angular */ "./node_modules/apollo-angular/fesm5/ng.apollo.js");
/* harmony import */ var src_app_core_services_pos_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/core/services/pos.service */ "./src/app/core/services/pos.service.ts");
/* harmony import */ var src_app_core_queries_pos_query__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/core/queries/pos.query */ "./src/app/core/queries/pos.query.ts");
/* harmony import */ var src_app_core_services_userService__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/core/services/userService */ "./src/app/core/services/userService.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");








var TicketComponent = /** @class */ (function () {
    function TicketComponent(apollo, ticketSync, userService, dialog, route) {
        var _this = this;
        this.apollo = apollo;
        this.ticketSync = ticketSync;
        this.userService = userService;
        this.dialog = dialog;
        this.route = route;
        this.userId = '5c4936057565dd0012a958f6';
        this.corporateId = '5ccaa08ab545031b446629af';
        this.userName = "Davka";
        this.ticket = [];
        this.cartTotal = 0;
        this.cartNumItems = 0;
        this.inputBillItems = [];
        this.displayedColumns = ['quantity', 'name', 'price'];
        this.userService.currentUser.subscribe(function (userData) {
            _this.currentUser = userData;
        });
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatTableDataSource"]();
    }
    // Sync with ticketSync service on init
    TicketComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.ticketSync.currentTicket.subscribe(function (data) { return _this.ticket = data; });
        this.ticketSync.currentTotal.subscribe(function (total) { return _this.cartTotal = total; });
        this.ticketSync.currentCartNum.subscribe(function (num) { return _this.cartNumItems = num; });
        console.log("userId = ", this.currentUser._id);
    };
    // Add item to ticket.
    TicketComponent.prototype.addItem = function (item) {
        // If the item already exists, add 1 to quantity
        if (this.ticket.includes(item)) {
            this.ticket[this.ticket.indexOf(item)].quantity += 1;
        }
        else {
            this.ticket.push(item);
            this.dataSource.data.push(item);
        }
        this.syncTicket();
        this.calculateTotal();
    };
    // Remove item from ticket
    TicketComponent.prototype.removeItem = function (item) {
        // Check if item is in array
        if (this.ticket.includes(item)) {
            // Splice the element out of the array
            var index = this.ticket.indexOf(item);
            if (index > -1) {
                // Set item quantity back to 1 (thus when readded, quantity isn't 0)
                this.ticket[this.ticket.indexOf(item)].quantity = 1;
                this.ticket.splice(index, 1);
            }
        }
        this.syncTicket();
        this.calculateTotal();
    };
    // Reduce quantity by one
    TicketComponent.prototype.subtractOne = function (item) {
        // Check if last item, if so, use remove method
        if (this.ticket[this.ticket.indexOf(item)].quantity === 1) {
            this.removeItem(item);
        }
        else {
            this.ticket[this.ticket.indexOf(item)].quantity = this.ticket[this.ticket.indexOf(item)].quantity - 1;
        }
        this.syncTicket();
        this.calculateTotal();
    };
    // Calculate cart total
    TicketComponent.prototype.calculateTotal = function () {
        var total = 0;
        var cartitems = 0;
        // Multiply item price by item quantity, add to total
        this.ticket.forEach(function (item) {
            total += (item.price * item.quantity);
            cartitems += item.quantity;
        });
        this.cartTotal = total;
        this.cartNumItems = cartitems;
        // Sync total with ticketSync service.
        this.ticketSync.updateNumItems(this.cartNumItems);
        this.ticketSync.updateTotal(this.cartTotal);
    };
    // Remove all items from cart
    TicketComponent.prototype.clearCart = function () {
        console.log('clear called');
        this.inputBillItems = [];
        // Reduce back to initial quantity (1 vs 0 for re-add)
        this.ticket.forEach(function (item) {
            item.quantity = 1;
        });
        // Empty local ticket variable then sync
        this.ticket = [];
        this.syncTicket();
        this.calculateTotal();
    };
    TicketComponent.prototype.syncTicket = function () {
        this.ticketSync.changeTicket(this.ticket);
    };
    TicketComponent.prototype.checkout = function () {
        if (this.ticket.length > 0) {
            console.log('ticket length = ' + this.ticket.length);
            for (var i = 0; i < this.ticket.length; i++) {
                this.inputBillItems.push({ posItemId: this.ticket[i]._id, quantity: this.ticket[i].quantity });
                console.log('posItemId =', this.ticket[i]._id + " quantity =" + this.ticket[i].quantity);
            }
            this.insertBill(this.inputBillItems);
            this.clearCart();
        }
    };
    TicketComponent.prototype.insertBill = function (inputBillItems) {
        this.currentCorporateId = this.route.snapshot.paramMap.get('_id');
        this.apollo.mutate({
            mutation: src_app_core_queries_pos_query__WEBPACK_IMPORTED_MODULE_4__["insertBillMutation"],
            variables: {
                userId: this.currentUser._id,
                corporateId: this.currentCorporateId,
                inputBillItems: inputBillItems
            }
        }).subscribe(function (_a) {
            var data = _a.data;
            console.log('got dataaaaa ', data);
        }, function (error) {
            console.log('failed', error);
        });
    };
    TicketComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-ticket',
            template: __webpack_require__(/*! ./ticket.component.html */ "./src/app/components/ticket/ticket.component.html"),
            styles: [__webpack_require__(/*! ./ticket.component.css */ "./src/app/components/ticket/ticket.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [apollo_angular__WEBPACK_IMPORTED_MODULE_2__["Apollo"], src_app_core_services_pos_service__WEBPACK_IMPORTED_MODULE_3__["PosService"], src_app_core_services_userService__WEBPACK_IMPORTED_MODULE_5__["UserAuthService"], _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatDialog"], _angular_router__WEBPACK_IMPORTED_MODULE_7__["ActivatedRoute"]])
    ], TicketComponent);
    return TicketComponent;
}());



/***/ }),

/***/ "./src/app/components/welcome/welcome.component.css":
/*!**********************************************************!*\
  !*** ./src/app/components/welcome/welcome.component.css ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvd2VsY29tZS93ZWxjb21lLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/components/welcome/welcome.component.html":
/*!***********************************************************!*\
  !*** ./src/app/components/welcome/welcome.component.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\" /> \r\n<app-dashboard-header></app-dashboard-header>\r\n\r\n<div class=\"add-margin bottom-content\">\r\n          <!-- <div class=\"text-right\">\r\n              <button class=\"btn btn-info btn-sm\" (click)=\"showJoinToCorporateModal()\"><i\r\n                  class=\"fa fa-user-plus pdd-right-5\"></i><span>Байгууллагад\r\n                  элсэх</span></button>\r\n              <button class=\"btn btn-info btn-sm\"><i class=\"fa fa-plus pdd-right-5\"></i><span>Байгууллага үүсгэх</span></button>\r\n            </div> -->\r\n            <div class=\"card\" *ngIf=\"!showTierSection\">\r\n              <div class=\"card-body\">\r\n                <span class=\"text-bold text-dark\">Таны байгууллага:</span>\r\n                <ng-template ngFor let-corporate [ngForOf]=\"corporates\">\r\n                  <ng-container >\r\n                    <!-- <a class=\"mrg-left-10\" (click)=\"setRole(corporate.corporateId._id)\" routerLink=\"/{{corporate.corporateId._id}}\"> -->\r\n                      <a class=\"mrg-left-10\" (click)=\"getAssignedPoses(corporate.corporateId._id,corporate.corporateId.name)\">\r\n\r\n                      <span class=\"title\">{{corporate.corporateId.name}}</span>\r\n                    </a>\r\n                    <button class=\"btn btn-info btn-xs btn-rounded mrg-left-10 no-mrg-btm\" type=\"button\"\r\n                      (click)=\"updateCorporate(corporate)\">\r\n                      <i class=\"fa fa-pencil font-size-10\"></i>\r\n                    </button>\r\n                  </ng-container>\r\n                  <ng-template #secondaryCorporate>\r\n                    <!-- <a class=\"mrg-left-10\" (click)=\"getAssignedPoses(corporate.corporateId._id)\" routerLink=\"/{{corporate.corporateId._id}}\"> -->\r\n                      <a class=\"mrg-left-10\" (click)=\"getAssignedPoses(corporate.corporateId._id,corporate.corporateId.name)\">\r\n\r\n                      <span class=\"title\">{{corporate.corporateId.name}}</span>\r\n                    </a>\r\n                  \r\n                  </ng-template>\r\n                </ng-template>\r\n        \r\n              </div>\r\n            </div>\r\n\r\n            <div class=\"card\">\r\n              <div class=\"card-body\">\r\n                <h4 class=\"card-title\">\r\n                  <span>ПОС жагсаалт: {{selectedCorporateName}}</span>\r\n                </h4>\r\n                <div class=\"row\">\r\n                  <!-- <div id=\"board3\" class=\"col-md-3\" *ngFor=\"let pos of poses\" (click)=\"syncPosId(pos._id,pos.paymentTypes)\" routerLink=\"/{{selectedCorporateId}}\"> -->\r\n                    <div id=\"board3\" class=\"col-md-3\" *ngFor=\"let pos of poses\" (click)=\"syncPosId(pos._id,pos.paymentTypes)\">\r\n\r\n                    <div class=\"card padding-15 draggable-item\" >\r\n                      <p class=\"font-size-15\">\r\n                        <b>{{pos.name}}</b>\r\n                      </p>\r\n                      <!-- <div class=\"portlet\">\r\n                         <ul class=\"portlet-item navbar\">\r\n                          <li>\r\n                            <a href=\"javascript:void(0);\" (click)=\"showPosUpdateModal(pos, $event);\"\r\n                              class=\"btn btn-icon btn-flat btn-rounded\">\r\n                              <i class=\"fa fa-pencil\"></i>\r\n                            </a>\r\n                          </li>\r\n                          <li>\r\n                            <a href=\"javascript:void(0);\" id=\"cardDelete\" (click)=\"deletePos(pos, $event);\"\r\n                              class=\"btn btn-icon btn-flat btn-rounded\" data-toggle=\"card-delete\">\r\n                              <i class=\"fa fa-trash\"></i>\r\n                            </a>\r\n                          </li>\r\n                        </ul> \r\n                      </div> -->\r\n                      <div class=\"mrg-top-20\">\r\n      \r\n                        <ul class=\"list-members list-inline float-right\">\r\n                          <li *ngFor=\"let member of pos.members\">\r\n                            <a href=\"javascript:void(0)\">\r\n                              <img src=\"{{mazziFileServerUrl + member.image}}\" alt=\"\">\r\n                            </a>\r\n                          </li>\r\n                        </ul>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n      </div>"

/***/ }),

/***/ "./src/app/components/welcome/welcome.component.ts":
/*!*********************************************************!*\
  !*** ./src/app/components/welcome/welcome.component.ts ***!
  \*********************************************************/
/*! exports provided: WelcomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WelcomeComponent", function() { return WelcomeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_core_services_userService__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/core/services/userService */ "./src/app/core/services/userService.ts");
/* harmony import */ var src_app_core_services_main_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/core/services/main.service */ "./src/app/core/services/main.service.ts");
/* harmony import */ var src_app_core_services_global_global_event_manager_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/core/services/global/global-event-manager.service */ "./src/app/core/services/global/global-event-manager.service.ts");
/* harmony import */ var apollo_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! apollo-angular */ "./node_modules/apollo-angular/fesm5/ng.apollo.js");
/* harmony import */ var src_app_core_queries_pos_query__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/core/queries/pos.query */ "./src/app/core/queries/pos.query.ts");
/* harmony import */ var src_app_core_services_pos_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/core/services/pos.service */ "./src/app/core/services/pos.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");









var WelcomeComponent = /** @class */ (function () {
    function WelcomeComponent(apollo, posSync, userService, mainService, router, globalEventsManager) {
        var _this = this;
        this.apollo = apollo;
        this.posSync = posSync;
        this.userService = userService;
        this.mainService = mainService;
        this.router = router;
        this.globalEventsManager = globalEventsManager;
        this.corporates = [];
        this.poses = [];
        this.showTierSection = false;
        this.userService.currentUser.subscribe(function (userData) {
            _this.currentUser = userData;
        });
    }
    WelcomeComponent.prototype.ngOnChanges = function (changes) {
        throw new Error("Method not implemented.");
    };
    WelcomeComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.posSync.currentPosId.subscribe(function (posId) { return _this.posId = posId; });
        this.posSync.currentPurchaseType.subscribe(function (purchaseType) { return _this.purchaseType = purchaseType; });
        this.getOwnerCorporate();
        this.getUsersCorporateMemberList();
    };
    ;
    WelcomeComponent.prototype.getOwnerCorporate = function () {
        var _this = this;
        console.log("currentUserId = ", this.currentUser);
        this.mainService.getOwnedCorporate(this.currentUser._id)
            .subscribe(function (response) {
            if (response.data.ownedCorporates.error == false) {
                _this.corporates = response.data.ownedCorporates.result;
                if (_this.corporates.length == 0) {
                    _this.showTierSection = true;
                }
            }
        });
    };
    WelcomeComponent.prototype.getUsersCorporateMemberList = function () {
        var _this = this;
        this.mainService.getUsersCorporateMemberList(this.currentUser._id)
            .subscribe(function (response) {
            if (response.data.usersCorporateMemberList.error == false) {
                _this.corporates = response.data.usersCorporateMemberList.result;
                console.log(_this.corporates, 'corporates');
                if (_this.corporates.length == 0) {
                    _this.showTierSection = true;
                }
            }
        });
    };
    WelcomeComponent.prototype.setRole = function (corporateId) {
        console.log(corporateId, 'corpId');
        var corporateIndex = this.corporates.findIndex(function (item) { return item.corporateId._id == corporateId; });
        this.mainService.setRole(this.corporates[corporateIndex].corporateMemberPositionId.name);
    };
    WelcomeComponent.prototype.passSelectedCorporate = function (corporate) {
        console.log(corporate, 'corp');
        this.globalEventsManager.passSelectedCorporate.emit(corporate);
    };
    WelcomeComponent.prototype.getUpdatedCorporate = function (updatedCorporate) {
        var corporateIndex = this.corporates.findIndex(function (x) { return x._id == updatedCorporate._id; });
        this.corporates[corporateIndex] = updatedCorporate;
    };
    WelcomeComponent.prototype.getAssignedPoses = function (corporateId, corporateName) {
        var _this = this;
        this.selectedCorporateName = corporateName;
        this.selectedCorporateId = corporateId;
        console.log("assignedPos called");
        this.apollo
            .query({
            query: src_app_core_queries_pos_query__WEBPACK_IMPORTED_MODULE_6__["assignedUsersPos"],
            variables: {
                userId: this.currentUser._id,
                corporateId: corporateId
            }
        })
            .subscribe(function (result) {
            _this.poses = result.data && result.data.assignedUsersPos.result;
            console.log("assignedPos = ", result.data);
        });
    };
    WelcomeComponent.prototype.syncPosId = function (posId, purchaseType) {
        console.log("posId = " + posId + " purchaseType = " + purchaseType);
        this.posSync.updatePosId(posId);
        this.posSync.updatePurchaseType(purchaseType);
        this.router.navigate(['HomeComponent', { _id: this.selectedCorporateId, id2: posId }]);
    };
    WelcomeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-welcome',
            template: __webpack_require__(/*! ./welcome.component.html */ "./src/app/components/welcome/welcome.component.html"),
            styles: [__webpack_require__(/*! ./welcome.component.css */ "./src/app/components/welcome/welcome.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [apollo_angular__WEBPACK_IMPORTED_MODULE_5__["Apollo"],
            src_app_core_services_pos_service__WEBPACK_IMPORTED_MODULE_7__["PosService"],
            src_app_core_services_userService__WEBPACK_IMPORTED_MODULE_2__["UserAuthService"],
            src_app_core_services_main_service__WEBPACK_IMPORTED_MODULE_3__["MainService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_8__["Router"],
            src_app_core_services_global_global_event_manager_service__WEBPACK_IMPORTED_MODULE_4__["GlobalEventsManager"]])
    ], WelcomeComponent);
    return WelcomeComponent;
}());



/***/ }),

/***/ "./src/app/core/queries/main.quey.ts":
/*!*******************************************!*\
  !*** ./src/app/core/queries/main.quey.ts ***!
  \*******************************************/
/*! exports provided: getCorporateTiersQuery, insertCorporateQuery, updateCorporateQuery, getUsersCorporateMemberListQuery, getSingleCorporateMemberListQuery, getOwnedCorporateQuery, getCorporateQuery, getCorporateTierQuery, joinCorporateQuery, getCorporateMemberListQuery, getCorporateMemberPositionsQuery, changeCorporateMemberPositionQuery, kickCorporateMemberListQuery, searchUserByPhoneQuery, inviteToCorporateQuery */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getCorporateTiersQuery", function() { return getCorporateTiersQuery; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "insertCorporateQuery", function() { return insertCorporateQuery; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "updateCorporateQuery", function() { return updateCorporateQuery; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getUsersCorporateMemberListQuery", function() { return getUsersCorporateMemberListQuery; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getSingleCorporateMemberListQuery", function() { return getSingleCorporateMemberListQuery; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getOwnedCorporateQuery", function() { return getOwnedCorporateQuery; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getCorporateQuery", function() { return getCorporateQuery; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getCorporateTierQuery", function() { return getCorporateTierQuery; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "joinCorporateQuery", function() { return joinCorporateQuery; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getCorporateMemberListQuery", function() { return getCorporateMemberListQuery; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getCorporateMemberPositionsQuery", function() { return getCorporateMemberPositionsQuery; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "changeCorporateMemberPositionQuery", function() { return changeCorporateMemberPositionQuery; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "kickCorporateMemberListQuery", function() { return kickCorporateMemberListQuery; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "searchUserByPhoneQuery", function() { return searchUserByPhoneQuery; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "inviteToCorporateQuery", function() { return inviteToCorporateQuery; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var graphql_tag__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! graphql-tag */ "./node_modules/graphql-tag/src/index.js");
/* harmony import */ var graphql_tag__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(graphql_tag__WEBPACK_IMPORTED_MODULE_1__);


/* TIER, PLAN, PRICE */
var getCorporateTiersQuery = graphql_tag__WEBPACK_IMPORTED_MODULE_1___default()(templateObject_1 || (templateObject_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__makeTemplateObject"](["\n    query corporateTiers {\n        corporateTiers {\n            error\n            status\n            message\n            result {\n                _id\n                name\n                access {\n                    mclass {\n                        access\n                        spec {\n                            classLimit\n                        }\n                    }\n                    pos {\n                        access\n                    }\n                }\n                price\n                duration\n            }\n        }\n    }\n"], ["\n    query corporateTiers {\n        corporateTiers {\n            error\n            status\n            message\n            result {\n                _id\n                name\n                access {\n                    mclass {\n                        access\n                        spec {\n                            classLimit\n                        }\n                    }\n                    pos {\n                        access\n                    }\n                }\n                price\n                duration\n            }\n        }\n    }\n"])));
var insertCorporateQuery = graphql_tag__WEBPACK_IMPORTED_MODULE_1___default()(templateObject_2 || (templateObject_2 = tslib__WEBPACK_IMPORTED_MODULE_0__["__makeTemplateObject"](["\n    mutation insertCorporate($userId: String!, $InputCorporate: InputCorporate!) {\n        insertCorporate(userId: $userId, InputCorporate: $InputCorporate) {\n            error\n            status\n            message\n            result {\n                _id\n                corporateCode\n                ownerId\n                name\n                createdDate\n            }\n        }\n    }\n"], ["\n    mutation insertCorporate($userId: String!, $InputCorporate: InputCorporate!) {\n        insertCorporate(userId: $userId, InputCorporate: $InputCorporate) {\n            error\n            status\n            message\n            result {\n                _id\n                corporateCode\n                ownerId\n                name\n                createdDate\n            }\n        }\n    }\n"])));
var updateCorporateQuery = graphql_tag__WEBPACK_IMPORTED_MODULE_1___default()(templateObject_3 || (templateObject_3 = tslib__WEBPACK_IMPORTED_MODULE_0__["__makeTemplateObject"](["\n    mutation updateCorporate($userId: String!, $corporateId: String!, $UpdateCorporate: UpdateCorporate!) {\n        updateCorporate(userId: $userId, corporateId: $corporateId, UpdateCorporate: $UpdateCorporate) {\n            error\n            status\n            message\n            result {\n                _id\n                corporateCode\n                ownerId\n                name\n                createdDate\n            }\n        }\n    }\n"], ["\n    mutation updateCorporate($userId: String!, $corporateId: String!, $UpdateCorporate: UpdateCorporate!) {\n        updateCorporate(userId: $userId, corporateId: $corporateId, UpdateCorporate: $UpdateCorporate) {\n            error\n            status\n            message\n            result {\n                _id\n                corporateCode\n                ownerId\n                name\n                createdDate\n            }\n        }\n    }\n"])));
var getUsersCorporateMemberListQuery = graphql_tag__WEBPACK_IMPORTED_MODULE_1___default()(templateObject_4 || (templateObject_4 = tslib__WEBPACK_IMPORTED_MODULE_0__["__makeTemplateObject"](["\n    query usersCorporateMemberList($userId: String!) {\n        usersCorporateMemberList(userId: $userId) {\n            error\n            status\n            message\n            result {\n                _id\n                userId\n                corporateMemberPositionId {\n                    _id\n                    name\n                }\n                createdDate\n                updatedDate\n                corporateId {\n                    _id\n                    corporateCode\n                    ownerId\n                    name\n                    createdDate\n                }\n            }\n        }\n    }\n"], ["\n    query usersCorporateMemberList($userId: String!) {\n        usersCorporateMemberList(userId: $userId) {\n            error\n            status\n            message\n            result {\n                _id\n                userId\n                corporateMemberPositionId {\n                    _id\n                    name\n                }\n                createdDate\n                updatedDate\n                corporateId {\n                    _id\n                    corporateCode\n                    ownerId\n                    name\n                    createdDate\n                }\n            }\n        }\n    }\n"])));
var getSingleCorporateMemberListQuery = graphql_tag__WEBPACK_IMPORTED_MODULE_1___default()(templateObject_5 || (templateObject_5 = tslib__WEBPACK_IMPORTED_MODULE_0__["__makeTemplateObject"](["\n    query singleCorporateMemberList($userId: String!, $corporateId: String!) {\n        singleCorporateMemberList(userId: $userId, corporateId: $corporateId) {\n            error\n            status\n            message\n            result {\n                _id\n                userId\n                corporateMemberPositionId {\n                    _id\n                    name\n                }\n                createdDate\n                updatedDate\n                corporateId {\n                    _id\n                    corporateCode\n                    ownerId\n                    name\n                    createdDate\n                }\n            }\n        }\n    }\n"], ["\n    query singleCorporateMemberList($userId: String!, $corporateId: String!) {\n        singleCorporateMemberList(userId: $userId, corporateId: $corporateId) {\n            error\n            status\n            message\n            result {\n                _id\n                userId\n                corporateMemberPositionId {\n                    _id\n                    name\n                }\n                createdDate\n                updatedDate\n                corporateId {\n                    _id\n                    corporateCode\n                    ownerId\n                    name\n                    createdDate\n                }\n            }\n        }\n    }\n"])));
var getOwnedCorporateQuery = graphql_tag__WEBPACK_IMPORTED_MODULE_1___default()(templateObject_6 || (templateObject_6 = tslib__WEBPACK_IMPORTED_MODULE_0__["__makeTemplateObject"](["\n    query ownedCorporates($userId: String!) {\n        ownedCorporates(userId: $userId) {\n            error\n            status\n            message\n            result {\n                _id\n                corporateCode\n                ownerId\n                name\n                createdDate\n            }\n        }\n    }\n"], ["\n    query ownedCorporates($userId: String!) {\n        ownedCorporates(userId: $userId) {\n            error\n            status\n            message\n            result {\n                _id\n                corporateCode\n                ownerId\n                name\n                createdDate\n            }\n        }\n    }\n"])));
var getCorporateQuery = graphql_tag__WEBPACK_IMPORTED_MODULE_1___default()(templateObject_7 || (templateObject_7 = tslib__WEBPACK_IMPORTED_MODULE_0__["__makeTemplateObject"](["\n    query corporate($userId: String!, $corporateId: String!) {\n        corporate(userId: $userId, corporateId: $corporateId) {\n            error\n            status\n            message\n            result {\n                _id\n                corporateCode\n                ownerId\n                name\n                createdDate                \n            }\n        }\n    }\n"], ["\n    query corporate($userId: String!, $corporateId: String!) {\n        corporate(userId: $userId, corporateId: $corporateId) {\n            error\n            status\n            message\n            result {\n                _id\n                corporateCode\n                ownerId\n                name\n                createdDate                \n            }\n        }\n    }\n"])));
var getCorporateTierQuery = graphql_tag__WEBPACK_IMPORTED_MODULE_1___default()(templateObject_8 || (templateObject_8 = tslib__WEBPACK_IMPORTED_MODULE_0__["__makeTemplateObject"](["\n    query corporateTier($corporateTierId: String!) {\n        corporateTier(corporateTierId: $corporateTierId) {\n            error\n            status\n            message\n            result {\n                _id\n                name\n                access {\n                    mclass {\n                        access\n                        spec {\n                            classLimit\n                        }\n                    }\n                    pos {\n                        access\n                    }\n                }\n                price\n                duration\n            }\n        }\n    }\n"], ["\n    query corporateTier($corporateTierId: String!) {\n        corporateTier(corporateTierId: $corporateTierId) {\n            error\n            status\n            message\n            result {\n                _id\n                name\n                access {\n                    mclass {\n                        access\n                        spec {\n                            classLimit\n                        }\n                    }\n                    pos {\n                        access\n                    }\n                }\n                price\n                duration\n            }\n        }\n    }\n"])));
var joinCorporateQuery = graphql_tag__WEBPACK_IMPORTED_MODULE_1___default()(templateObject_9 || (templateObject_9 = tslib__WEBPACK_IMPORTED_MODULE_0__["__makeTemplateObject"](["\n    mutation joinCorporate($userId: String!, $corporateCode: String!){\n        joinCorporate(userId: $userId, corporateCode: $corporateCode) {\n            error\n            status\n            message\n        }\n    }\n"], ["\n    mutation joinCorporate($userId: String!, $corporateCode: String!){\n        joinCorporate(userId: $userId, corporateCode: $corporateCode) {\n            error\n            status\n            message\n        }\n    }\n"])));
var getCorporateMemberListQuery = graphql_tag__WEBPACK_IMPORTED_MODULE_1___default()(templateObject_10 || (templateObject_10 = tslib__WEBPACK_IMPORTED_MODULE_0__["__makeTemplateObject"](["\n    query corporateMemberList($userId: String!, $corporateId: String!) {\n        corporateMemberList(userId: $userId, corporateId: $corporateId) {\n            error\n            status\n            message\n            result {\n                _id\n                userId {\n                    _id\n                    region\n                    image\n                    nickname\n                    lastname\n                    surname\n                    birthday\n                    email\n                    gender\n                    phone\n                    state\n                    online_at\n                    created_at                    \n                }\n                corporateMemberPositionId\n                createdDate\n                updatedDate\n                corporateId\n            }\n        }\n    }\n"], ["\n    query corporateMemberList($userId: String!, $corporateId: String!) {\n        corporateMemberList(userId: $userId, corporateId: $corporateId) {\n            error\n            status\n            message\n            result {\n                _id\n                userId {\n                    _id\n                    region\n                    image\n                    nickname\n                    lastname\n                    surname\n                    birthday\n                    email\n                    gender\n                    phone\n                    state\n                    online_at\n                    created_at                    \n                }\n                corporateMemberPositionId\n                createdDate\n                updatedDate\n                corporateId\n            }\n        }\n    }\n"])));
var getCorporateMemberPositionsQuery = graphql_tag__WEBPACK_IMPORTED_MODULE_1___default()(templateObject_11 || (templateObject_11 = tslib__WEBPACK_IMPORTED_MODULE_0__["__makeTemplateObject"](["\n    query corporateMemberPositions {\n        corporateMemberPositions {\n            error\n            status\n            message\n            result {\n                _id\n                name\n                positionKey\n            }\n        }\n    }\n"], ["\n    query corporateMemberPositions {\n        corporateMemberPositions {\n            error\n            status\n            message\n            result {\n                _id\n                name\n                positionKey\n            }\n        }\n    }\n"])));
var changeCorporateMemberPositionQuery = graphql_tag__WEBPACK_IMPORTED_MODULE_1___default()(templateObject_12 || (templateObject_12 = tslib__WEBPACK_IMPORTED_MODULE_0__["__makeTemplateObject"](["\n    mutation changeCorporateMemberPosition($userId: String!, $corporateMemberListId: String!, $corporateMemberPositionId: String!) {\n        changeCorporateMemberPosition(userId: $userId, corporateMemberListId: $corporateMemberListId, corporateMemberPositionId: $corporateMemberPositionId) {\n            error\n            status\n            message\n            result {\n                _id\n                userId {\n                    _id\n                    region\n                    image\n                    nickname\n                    lastname\n                    surname\n                    birthday\n                    email\n                    gender\n                    phone\n                    state\n                    online_at\n                    created_at\n                }\n                corporateMemberPositionId\n                createdDate\n                updatedDate\n                corporateId\n            }\n        }\n    }\n"], ["\n    mutation changeCorporateMemberPosition($userId: String!, $corporateMemberListId: String!, $corporateMemberPositionId: String!) {\n        changeCorporateMemberPosition(userId: $userId, corporateMemberListId: $corporateMemberListId, corporateMemberPositionId: $corporateMemberPositionId) {\n            error\n            status\n            message\n            result {\n                _id\n                userId {\n                    _id\n                    region\n                    image\n                    nickname\n                    lastname\n                    surname\n                    birthday\n                    email\n                    gender\n                    phone\n                    state\n                    online_at\n                    created_at\n                }\n                corporateMemberPositionId\n                createdDate\n                updatedDate\n                corporateId\n            }\n        }\n    }\n"])));
var kickCorporateMemberListQuery = graphql_tag__WEBPACK_IMPORTED_MODULE_1___default()(templateObject_13 || (templateObject_13 = tslib__WEBPACK_IMPORTED_MODULE_0__["__makeTemplateObject"](["\n    mutation kickCorporateMemberList($userId: String!, $corporateMemberListId: String!) {\n        kickCorporateMemberList(userId: $userId, corporateMemberListId: $corporateMemberListId) {\n            error\n            status\n            message            \n        }\n    }\n"], ["\n    mutation kickCorporateMemberList($userId: String!, $corporateMemberListId: String!) {\n        kickCorporateMemberList(userId: $userId, corporateMemberListId: $corporateMemberListId) {\n            error\n            status\n            message            \n        }\n    }\n"])));
var searchUserByPhoneQuery = graphql_tag__WEBPACK_IMPORTED_MODULE_1___default()(templateObject_14 || (templateObject_14 = tslib__WEBPACK_IMPORTED_MODULE_0__["__makeTemplateObject"](["\n    query searchUserByPhone($userId: String!, $phone: String!) {\n        searchUserByPhone(userId: $userId, phone: $phone) {\n            error\n            status\n            message\n            result {\n                _id\n                region\n                image\n                nickname\n                lastname\n                surname\n                birthday\n                email\n                gender\n                phone\n                state\n                online_at\n                created_at                \n            }\n        }\n    }\n"], ["\n    query searchUserByPhone($userId: String!, $phone: String!) {\n        searchUserByPhone(userId: $userId, phone: $phone) {\n            error\n            status\n            message\n            result {\n                _id\n                region\n                image\n                nickname\n                lastname\n                surname\n                birthday\n                email\n                gender\n                phone\n                state\n                online_at\n                created_at                \n            }\n        }\n    }\n"])));
var inviteToCorporateQuery = graphql_tag__WEBPACK_IMPORTED_MODULE_1___default()(templateObject_15 || (templateObject_15 = tslib__WEBPACK_IMPORTED_MODULE_0__["__makeTemplateObject"](["\n    mutation inviteToCorporate($userId: String!, $corporateId: String!, $subjectUsersId: String!, $corporateMemberPositionId: String!) {\n        inviteToCorporate(userId: $userId, corporateId: $corporateId, subjectUsersId: $subjectUsersId, corporateMemberPositionId: $corporateMemberPositionId) {\n            error\n            status\n            message\n            result {\n                _id\n                userId {\n                    _id\n                    region\n                    image\n                    nickname\n                    lastname\n                    surname\n                    birthday\n                    email\n                    gender\n                    phone\n                    state\n                    online_at\n                    created_at\n                }\n                corporateMemberPositionId\n                createdDate\n                updatedDate\n                corporateId\n            }\n        }\n    }\n"], ["\n    mutation inviteToCorporate($userId: String!, $corporateId: String!, $subjectUsersId: String!, $corporateMemberPositionId: String!) {\n        inviteToCorporate(userId: $userId, corporateId: $corporateId, subjectUsersId: $subjectUsersId, corporateMemberPositionId: $corporateMemberPositionId) {\n            error\n            status\n            message\n            result {\n                _id\n                userId {\n                    _id\n                    region\n                    image\n                    nickname\n                    lastname\n                    surname\n                    birthday\n                    email\n                    gender\n                    phone\n                    state\n                    online_at\n                    created_at\n                }\n                corporateMemberPositionId\n                createdDate\n                updatedDate\n                corporateId\n            }\n        }\n    }\n"])));
var templateObject_1, templateObject_2, templateObject_3, templateObject_4, templateObject_5, templateObject_6, templateObject_7, templateObject_8, templateObject_9, templateObject_10, templateObject_11, templateObject_12, templateObject_13, templateObject_14, templateObject_15;


/***/ }),

/***/ "./src/app/core/queries/pos.query.ts":
/*!*******************************************!*\
  !*** ./src/app/core/queries/pos.query.ts ***!
  \*******************************************/
/*! exports provided: posItemsQuery, insertBillMutation, dailyInsertPosReportMutation, fullPosItemsQuery, posItemTypesQuery, assignedUsersPosItems, assignedUsersPos, posesPosItems */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "posItemsQuery", function() { return posItemsQuery; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "insertBillMutation", function() { return insertBillMutation; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "dailyInsertPosReportMutation", function() { return dailyInsertPosReportMutation; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fullPosItemsQuery", function() { return fullPosItemsQuery; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "posItemTypesQuery", function() { return posItemTypesQuery; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "assignedUsersPosItems", function() { return assignedUsersPosItems; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "assignedUsersPos", function() { return assignedUsersPos; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "posesPosItems", function() { return posesPosItems; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var graphql_tag__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! graphql-tag */ "./node_modules/graphql-tag/src/index.js");
/* harmony import */ var graphql_tag__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(graphql_tag__WEBPACK_IMPORTED_MODULE_1__);


var posItemsQuery = graphql_tag__WEBPACK_IMPORTED_MODULE_1___default()(templateObject_1 || (templateObject_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__makeTemplateObject"](["\n  query posItems($posItemTypeId:String!,$corporateId:String!){\n    posItems(posItemTypeId:$posItemTypeId,corporateId:$corporateId) {\n       error\n       status\n       message\n         result {\n           _id\n           name\n           price\n           createdDate\n           corporateId\n           posItemTypeId\n         }\n     }\n  }\n"], ["\n  query posItems($posItemTypeId:String!,$corporateId:String!){\n    posItems(posItemTypeId:$posItemTypeId,corporateId:$corporateId) {\n       error\n       status\n       message\n         result {\n           _id\n           name\n           price\n           createdDate\n           corporateId\n           posItemTypeId\n         }\n     }\n  }\n"])));
var insertBillMutation = graphql_tag__WEBPACK_IMPORTED_MODULE_1___default()(templateObject_2 || (templateObject_2 = tslib__WEBPACK_IMPORTED_MODULE_0__["__makeTemplateObject"](["\n  mutation insertBill($userId:String!,$corporateId:String!,$inputBillItems:[InputBillItem],$purchaseType:String!,$subjectUserId:String!,$posId:String!){\n    insertBill(userId:$userId, corporateId:$corporateId,InputBillItems:$inputBillItems,purchaseType:$purchaseType,subjectUserId:$subjectUserId,posId:$posId){\n      error\n      status\n      message\n      result{\n        _id\n        corporateId\n        userId\n        createdDate\n        totalPrice\n        html\n      }\n    }\n  }"], ["\n  mutation insertBill($userId:String!,$corporateId:String!,$inputBillItems:[InputBillItem],$purchaseType:String!,$subjectUserId:String!,$posId:String!){\n    insertBill(userId:$userId, corporateId:$corporateId,InputBillItems:$inputBillItems,purchaseType:$purchaseType,subjectUserId:$subjectUserId,posId:$posId){\n      error\n      status\n      message\n      result{\n        _id\n        corporateId\n        userId\n        createdDate\n        totalPrice\n        html\n      }\n    }\n  }"])));
var dailyInsertPosReportMutation = graphql_tag__WEBPACK_IMPORTED_MODULE_1___default()(templateObject_3 || (templateObject_3 = tslib__WEBPACK_IMPORTED_MODULE_0__["__makeTemplateObject"](["\n      mutation dailyInsertPosReport($userId:String!,$corporateId:String!,$posId:String!){\n        dailyInsertPosReport(userId:$userId,corporateId:$corporateId,posId:$posId){\n          error\n          status\n          message\n          result{\n            _id\n            corporateId\n            totalSoldPrice\n            totalSoldItems\n            posItems{\n              posItem{\n                posItemId\n                name\n                price\n                posItemTypeId\n              }\n              quantity\n              totalPrice\n            }\n            createdDate\n            posId\n          }\n        }\n      }\n      "], ["\n      mutation dailyInsertPosReport($userId:String!,$corporateId:String!,$posId:String!){\n        dailyInsertPosReport(userId:$userId,corporateId:$corporateId,posId:$posId){\n          error\n          status\n          message\n          result{\n            _id\n            corporateId\n            totalSoldPrice\n            totalSoldItems\n            posItems{\n              posItem{\n                posItemId\n                name\n                price\n                posItemTypeId\n              }\n              quantity\n              totalPrice\n            }\n            createdDate\n            posId\n          }\n        }\n      }\n      "])));
var fullPosItemsQuery = graphql_tag__WEBPACK_IMPORTED_MODULE_1___default()(templateObject_4 || (templateObject_4 = tslib__WEBPACK_IMPORTED_MODULE_0__["__makeTemplateObject"](["\n    query fullPosItems($userId:String!,$corporateId:String!){\n        fullPosItems(userId:$userId,corporateId:$corporateId){\n            error\n            status\n            message\n            result{\n                _id\n                name\n                price\n                createdDate\n                corporateId\n                posItemTypeId\n            }\n        }\n    }\n    "], ["\n    query fullPosItems($userId:String!,$corporateId:String!){\n        fullPosItems(userId:$userId,corporateId:$corporateId){\n            error\n            status\n            message\n            result{\n                _id\n                name\n                price\n                createdDate\n                corporateId\n                posItemTypeId\n            }\n        }\n    }\n    "])));
var posItemTypesQuery = graphql_tag__WEBPACK_IMPORTED_MODULE_1___default()(templateObject_5 || (templateObject_5 = tslib__WEBPACK_IMPORTED_MODULE_0__["__makeTemplateObject"](["\n    query posItemTypes($corporateId:String!){\n        posItemTypes(corporateId:$corporateId){\n          error\n          status\n          message\n          result{\n            _id\n            name\n            corporateId\n            createdDate\n          }\n        }\n    }\n    "], ["\n    query posItemTypes($corporateId:String!){\n        posItemTypes(corporateId:$corporateId){\n          error\n          status\n          message\n          result{\n            _id\n            name\n            corporateId\n            createdDate\n          }\n        }\n    }\n    "])));
var assignedUsersPosItems = graphql_tag__WEBPACK_IMPORTED_MODULE_1___default()(templateObject_6 || (templateObject_6 = tslib__WEBPACK_IMPORTED_MODULE_0__["__makeTemplateObject"](["\n    query assignedUsersPosItems($userId:String!,$corporateId:String!){\n      assignedUsersPosItems(userId:$userId,corporateId:$corporateId){\n        error\n        status\n        message\n        result{\n          _id\n          name\n          price\n          createdDate\n          corporateId\n          posId\n          posItemTypeId\n        }\n      }\n    }\n  "], ["\n    query assignedUsersPosItems($userId:String!,$corporateId:String!){\n      assignedUsersPosItems(userId:$userId,corporateId:$corporateId){\n        error\n        status\n        message\n        result{\n          _id\n          name\n          price\n          createdDate\n          corporateId\n          posId\n          posItemTypeId\n        }\n      }\n    }\n  "])));
var assignedUsersPos = graphql_tag__WEBPACK_IMPORTED_MODULE_1___default()(templateObject_7 || (templateObject_7 = tslib__WEBPACK_IMPORTED_MODULE_0__["__makeTemplateObject"](["\n    query assignedUsersPos($userId:String!,$corporateId:String!){\n      assignedUsersPos(userId:$userId,corporateId:$corporateId){\n        error\n        status\n        message\n        result{\n          _id\n          name\n          corporateId\n          membersId\n          createdDate\n          paymentTypes{\n            card\n            cash\n            mazziCard\n          }\n        }\n      }\n    }\n  "], ["\n    query assignedUsersPos($userId:String!,$corporateId:String!){\n      assignedUsersPos(userId:$userId,corporateId:$corporateId){\n        error\n        status\n        message\n        result{\n          _id\n          name\n          corporateId\n          membersId\n          createdDate\n          paymentTypes{\n            card\n            cash\n            mazziCard\n          }\n        }\n      }\n    }\n  "])));
var posesPosItems = graphql_tag__WEBPACK_IMPORTED_MODULE_1___default()(templateObject_8 || (templateObject_8 = tslib__WEBPACK_IMPORTED_MODULE_0__["__makeTemplateObject"](["\n    query posesPosItems($userId:String!,$posId:String!){\n      posesPosItems(userId:$userId,posId:$posId){\n        error\n        status\n        message\n        result{\n          _id\n          name\n          purchaseType\n          price\n          createdDate\n          corporateId\n          posId\n          posItemTypeId\n        }\n      }\n    }\n  "], ["\n    query posesPosItems($userId:String!,$posId:String!){\n      posesPosItems(userId:$userId,posId:$posId){\n        error\n        status\n        message\n        result{\n          _id\n          name\n          purchaseType\n          price\n          createdDate\n          corporateId\n          posId\n          posItemTypeId\n        }\n      }\n    }\n  "])));
var templateObject_1, templateObject_2, templateObject_3, templateObject_4, templateObject_5, templateObject_6, templateObject_7, templateObject_8;


/***/ }),

/***/ "./src/app/core/queries/user.query.ts":
/*!********************************************!*\
  !*** ./src/app/core/queries/user.query.ts ***!
  \********************************************/
/*! exports provided: loginQuery, logoutQuery, registerQuery, getRoleQuery, getUniversityQuery, getUserDataQuery, updateUserData, updateUserWithoutPassQuery, sendRecoveryRequestQuery, recoverChangePasswordQuery, updateFcmQuery, getClassUserByIdQuery, getTiersQuery, changeTierQuery, updatePasswordQuery */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "loginQuery", function() { return loginQuery; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "logoutQuery", function() { return logoutQuery; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "registerQuery", function() { return registerQuery; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getRoleQuery", function() { return getRoleQuery; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getUniversityQuery", function() { return getUniversityQuery; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getUserDataQuery", function() { return getUserDataQuery; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "updateUserData", function() { return updateUserData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "updateUserWithoutPassQuery", function() { return updateUserWithoutPassQuery; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "sendRecoveryRequestQuery", function() { return sendRecoveryRequestQuery; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "recoverChangePasswordQuery", function() { return recoverChangePasswordQuery; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "updateFcmQuery", function() { return updateFcmQuery; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getClassUserByIdQuery", function() { return getClassUserByIdQuery; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getTiersQuery", function() { return getTiersQuery; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "changeTierQuery", function() { return changeTierQuery; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "updatePasswordQuery", function() { return updatePasswordQuery; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var graphql_tag__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! graphql-tag */ "./node_modules/graphql-tag/src/index.js");
/* harmony import */ var graphql_tag__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(graphql_tag__WEBPACK_IMPORTED_MODULE_1__);


//TeacherAuth
var loginQuery = graphql_tag__WEBPACK_IMPORTED_MODULE_1___default()(templateObject_1 || (templateObject_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__makeTemplateObject"](["query login($emailOrPhone: String!, $password: String!){\n    login(emailOrPhone: $emailOrPhone, password: $password){\n      _id\n      region\n      image\n      nickname\n      lastname\n      surname\n      email\n      gender\n      phone\n      registerId\n      state\n      online_at\n      created_at\n      is_confirmed\n      device_id\n      fcm_token\n      token\n      subUser{\n        _id\n        userId\n        schoolId\n        storageLimit\n        tierId\n        path\n        studentIdCode\n        subscriptionEndDate\n        roleId\n        notification{\n          mchatNotificationCount\n          mclassNotificationCount\n          lastDeleted\n        }\n      }\n    }\n  }\n"], ["query login($emailOrPhone: String!, $password: String!){\n    login(emailOrPhone: $emailOrPhone, password: $password){\n      _id\n      region\n      image\n      nickname\n      lastname\n      surname\n      email\n      gender\n      phone\n      registerId\n      state\n      online_at\n      created_at\n      is_confirmed\n      device_id\n      fcm_token\n      token\n      subUser{\n        _id\n        userId\n        schoolId\n        storageLimit\n        tierId\n        path\n        studentIdCode\n        subscriptionEndDate\n        roleId\n        notification{\n          mchatNotificationCount\n          mclassNotificationCount\n          lastDeleted\n        }\n      }\n    }\n  }\n"])));
var logoutQuery = graphql_tag__WEBPACK_IMPORTED_MODULE_1___default()(templateObject_2 || (templateObject_2 = tslib__WEBPACK_IMPORTED_MODULE_0__["__makeTemplateObject"](["mutation logout($userId: String!){\n    logout(userId: $userId){\n      error\n      message\n    }\n  }\n"], ["mutation logout($userId: String!){\n    logout(userId: $userId){\n      error\n      message\n    }\n  }\n"])));
var registerQuery = graphql_tag__WEBPACK_IMPORTED_MODULE_1___default()(templateObject_3 || (templateObject_3 = tslib__WEBPACK_IMPORTED_MODULE_0__["__makeTemplateObject"](["mutation register($email: String!, $password: String!, $firstName: String!, $lastName: String!, $gender: String!, $roleId: String!, $schoolId: String!){\n    register(email: $email, password: $password, firstName: $firstName, lastName: $lastName, gender: $gender, roleId: $roleId, schoolId: $schoolId){\n      error\n      message\n    }\n  }\n"], ["mutation register($email: String!, $password: String!, $firstName: String!, $lastName: String!, $gender: String!, $roleId: String!, $schoolId: String!){\n    register(email: $email, password: $password, firstName: $firstName, lastName: $lastName, gender: $gender, roleId: $roleId, schoolId: $schoolId){\n      error\n      message\n    }\n  }\n"])));
var getRoleQuery = graphql_tag__WEBPACK_IMPORTED_MODULE_1___default()(templateObject_4 || (templateObject_4 = tslib__WEBPACK_IMPORTED_MODULE_0__["__makeTemplateObject"](["query role{\n    role{\n      _id\n      name\n      priority\n    }\n  }\n"], ["query role{\n    role{\n      _id\n      name\n      priority\n    }\n  }\n"])));
var getUniversityQuery = graphql_tag__WEBPACK_IMPORTED_MODULE_1___default()(templateObject_5 || (templateObject_5 = tslib__WEBPACK_IMPORTED_MODULE_0__["__makeTemplateObject"](["query school{\n    school {\n      _id\n      name\n    }\n  }\n"], ["query school{\n    school {\n      _id\n      name\n    }\n  }\n"])));
var getUserDataQuery = graphql_tag__WEBPACK_IMPORTED_MODULE_1___default()(templateObject_6 || (templateObject_6 = tslib__WEBPACK_IMPORTED_MODULE_0__["__makeTemplateObject"](["query entry($token: String!){\n    entry(token: $token){\n      _id\n      region\n      image\n      nickname\n      lastname\n      surname\n      email\n      gender\n      phone\n      registerId\n      state\n      online_at\n      created_at\n      is_confirmed\n      device_id\n      fcm_token\n      token\n      subUser{\n        _id\n        userId\n        schoolId\n        storageLimit\n        tierId\n        path\n        studentIdCode\n        subscriptionEndDate\n        roleId\n        notification{\n          mchatNotificationCount\n          mclassNotificationCount\n          lastDeleted\n        }\n      }\n    }\n  }\n"], ["query entry($token: String!){\n    entry(token: $token){\n      _id\n      region\n      image\n      nickname\n      lastname\n      surname\n      email\n      gender\n      phone\n      registerId\n      state\n      online_at\n      created_at\n      is_confirmed\n      device_id\n      fcm_token\n      token\n      subUser{\n        _id\n        userId\n        schoolId\n        storageLimit\n        tierId\n        path\n        studentIdCode\n        subscriptionEndDate\n        roleId\n        notification{\n          mchatNotificationCount\n          mclassNotificationCount\n          lastDeleted\n        }\n      }\n    }\n  }\n"])));
////////////////////////////////////
var updateUserData = graphql_tag__WEBPACK_IMPORTED_MODULE_1___default()(templateObject_7 || (templateObject_7 = tslib__WEBPACK_IMPORTED_MODULE_0__["__makeTemplateObject"](["\nmutation updateUserdata($userId : String! , $email : String , $firstName : String , $lastName : String , $newPassword : String , $oldPassword : String!){\n  updateUser(userId : $userId , email : $email , firstName : $firstName , lastName : $lastName , newPassword : $newPassword , oldPassword : $oldPassword){\n    _id\n    email\n    firstName\n    lastName\n    password\n    gender\n    roleId\n  }\n}\n"], ["\nmutation updateUserdata($userId : String! , $email : String , $firstName : String , $lastName : String , $newPassword : String , $oldPassword : String!){\n  updateUser(userId : $userId , email : $email , firstName : $firstName , lastName : $lastName , newPassword : $newPassword , oldPassword : $oldPassword){\n    _id\n    email\n    firstName\n    lastName\n    password\n    gender\n    roleId\n  }\n}\n"])));
var updateUserWithoutPassQuery = graphql_tag__WEBPACK_IMPORTED_MODULE_1___default()(templateObject_8 || (templateObject_8 = tslib__WEBPACK_IMPORTED_MODULE_0__["__makeTemplateObject"](["\n  mutation updateUserWithoutPass($userId: String!, $email: String!, $nickname: String!, $lastname: String!, $schoolId: String!){\n    updateUserWithoutPass(userId: $userId, email: $email, nickname: $nickname, lastname: $lastname, schoolId: $schoolId){\n      error\n      message\n    }\n  }\n"], ["\n  mutation updateUserWithoutPass($userId: String!, $email: String!, $nickname: String!, $lastname: String!, $schoolId: String!){\n    updateUserWithoutPass(userId: $userId, email: $email, nickname: $nickname, lastname: $lastname, schoolId: $schoolId){\n      error\n      message\n    }\n  }\n"])));
var sendRecoveryRequestQuery = graphql_tag__WEBPACK_IMPORTED_MODULE_1___default()(templateObject_9 || (templateObject_9 = tslib__WEBPACK_IMPORTED_MODULE_0__["__makeTemplateObject"](["\n mutation recoverPasswordSendEmail($email : String!){\n   recoverPasswordSendEmail(email : $email)\n }\n"], ["\n mutation recoverPasswordSendEmail($email : String!){\n   recoverPasswordSendEmail(email : $email)\n }\n"])));
var recoverChangePasswordQuery = graphql_tag__WEBPACK_IMPORTED_MODULE_1___default()(templateObject_10 || (templateObject_10 = tslib__WEBPACK_IMPORTED_MODULE_0__["__makeTemplateObject"](["\n mutation recoverPasswordChangePassword($token : String! , $password : String!){\n   recoverPasswordChangePassword(token : $token , password : $password)\n }\n"], ["\n mutation recoverPasswordChangePassword($token : String! , $password : String!){\n   recoverPasswordChangePassword(token : $token , password : $password)\n }\n"])));
var updateFcmQuery = graphql_tag__WEBPACK_IMPORTED_MODULE_1___default()(templateObject_11 || (templateObject_11 = tslib__WEBPACK_IMPORTED_MODULE_0__["__makeTemplateObject"](["\n mutation udpateFcm($userId: String!, $token: String!){\n   updateFcm(userId: $userId, token: $token){\n     error\n     message\n   }\n }\n"], ["\n mutation udpateFcm($userId: String!, $token: String!){\n   updateFcm(userId: $userId, token: $token){\n     error\n     message\n   }\n }\n"])));
// -------------------------------
var getClassUserByIdQuery = graphql_tag__WEBPACK_IMPORTED_MODULE_1___default()(templateObject_12 || (templateObject_12 = tslib__WEBPACK_IMPORTED_MODULE_0__["__makeTemplateObject"](["query mazziUserById($userId: String!){\n    mazziUserById(userId: $userId){\n      _id\n      region\n      image\n      nickname\n      lastname\n      surname\n      birthday\n      email\n      gender\n      phone\n      state\n      online_at\n      created_at \n    }\n  }\n"], ["query mazziUserById($userId: String!){\n    mazziUserById(userId: $userId){\n      _id\n      region\n      image\n      nickname\n      lastname\n      surname\n      birthday\n      email\n      gender\n      phone\n      state\n      online_at\n      created_at \n    }\n  }\n"])));
var getTiersQuery = graphql_tag__WEBPACK_IMPORTED_MODULE_1___default()(templateObject_13 || (templateObject_13 = tslib__WEBPACK_IMPORTED_MODULE_0__["__makeTemplateObject"](["query tiers{\n  tiers{\n    _id\n    name\n    key\n    timeLimit\n    price\n    storageSpace\n    module\n  }\n}\n"], ["query tiers{\n  tiers{\n    _id\n    name\n    key\n    timeLimit\n    price\n    storageSpace\n    module\n  }\n}\n"])));
var changeTierQuery = graphql_tag__WEBPACK_IMPORTED_MODULE_1___default()(templateObject_14 || (templateObject_14 = tslib__WEBPACK_IMPORTED_MODULE_0__["__makeTemplateObject"](["mutation changeTier($userId: String!, $tierId: String!){\n    changeTier(userId: $userId, tierId: $tierId){\n      error\n      message\n    }\n  }\n"], ["mutation changeTier($userId: String!, $tierId: String!){\n    changeTier(userId: $userId, tierId: $tierId){\n      error\n      message\n    }\n  }\n"])));
var updatePasswordQuery = graphql_tag__WEBPACK_IMPORTED_MODULE_1___default()(templateObject_15 || (templateObject_15 = tslib__WEBPACK_IMPORTED_MODULE_0__["__makeTemplateObject"](["mutation updatePassword($userId: String!, $oldPassword: String!, $newPassword: String!){\n  updatePassword(userId: $userId, oldPassword: $oldPassword, newPassword: $newPassword){\n    error\n    message\n  }\n}"], ["mutation updatePassword($userId: String!, $oldPassword: String!, $newPassword: String!){\n  updatePassword(userId: $userId, oldPassword: $oldPassword, newPassword: $newPassword){\n    error\n    message\n  }\n}"])));
var templateObject_1, templateObject_2, templateObject_3, templateObject_4, templateObject_5, templateObject_6, templateObject_7, templateObject_8, templateObject_9, templateObject_10, templateObject_11, templateObject_12, templateObject_13, templateObject_14, templateObject_15;


/***/ }),

/***/ "./src/app/core/services/auth-guard.service.ts":
/*!*****************************************************!*\
  !*** ./src/app/core/services/auth-guard.service.ts ***!
  \*****************************************************/
/*! exports provided: AuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return AuthGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _userService__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./userService */ "./src/app/core/services/userService.ts");





var AuthGuard = /** @class */ (function () {
    function AuthGuard(router, userService) {
        this.router = router;
        this.userService = userService;
    }
    AuthGuard.prototype.canActivate = function (route, state) {
        if (localStorage.getItem('jwtToken')) {
            console.log('guard');
            return this.userService.isAuthenticated.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["take"])(1));
        }
        this.router.navigateByUrl('/login');
    };
    AuthGuard = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _userService__WEBPACK_IMPORTED_MODULE_4__["UserAuthService"]])
    ], AuthGuard);
    return AuthGuard;
}());



/***/ }),

/***/ "./src/app/core/services/global/global-event-manager.service.ts":
/*!**********************************************************************!*\
  !*** ./src/app/core/services/global/global-event-manager.service.ts ***!
  \**********************************************************************/
/*! exports provided: GlobalEventsManager */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GlobalEventsManager", function() { return GlobalEventsManager; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var GlobalEventsManager = /** @class */ (function () {
    function GlobalEventsManager() {
        this.showSideMenu = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.hideSideMenu = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        /* */
        this.passSelectedCorporate = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
    }
    GlobalEventsManager = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
    ], GlobalEventsManager);
    return GlobalEventsManager;
}());



/***/ }),

/***/ "./src/app/core/services/jwt.service.ts":
/*!**********************************************!*\
  !*** ./src/app/core/services/jwt.service.ts ***!
  \**********************************************/
/*! exports provided: JwtService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JwtService", function() { return JwtService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var JwtService = /** @class */ (function () {
    function JwtService() {
    }
    JwtService.prototype.getToken = function () {
        return window.localStorage['jwtToken'];
    };
    JwtService.prototype.saveToken = function (token) {
        window.localStorage['jwtToken'] = token;
    };
    JwtService.prototype.destroyToken = function () {
        window.localStorage.removeItem('jwtToken');
    };
    JwtService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()
    ], JwtService);
    return JwtService;
}());



/***/ }),

/***/ "./src/app/core/services/main.service.ts":
/*!***********************************************!*\
  !*** ./src/app/core/services/main.service.ts ***!
  \***********************************************/
/*! exports provided: MainService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainService", function() { return MainService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var apollo_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! apollo-angular */ "./node_modules/apollo-angular/fesm5/ng.apollo.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _queries_main_quey__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../queries/main.quey */ "./src/app/core/queries/main.quey.ts");






var MainService = /** @class */ (function () {
    function MainService(apollo) {
        this.apollo = apollo;
        this.currentUserRole = new rxjs__WEBPACK_IMPORTED_MODULE_3__["BehaviorSubject"]({});
        this.currentRole = this.currentUserRole.asObservable().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["distinctUntilChanged"])());
    }
    MainService.prototype.searchUserByPhone = function (userId, phone) {
        return this.apollo.query({
            query: _queries_main_quey__WEBPACK_IMPORTED_MODULE_5__["searchUserByPhoneQuery"],
            variables: {
                userId: userId,
                phone: phone
            }
        });
    };
    MainService.prototype.inviteToCorporate = function (userId, corporateId, subjectUsersId, corporateMemberPositionId) {
        return this.apollo.mutate({
            mutation: _queries_main_quey__WEBPACK_IMPORTED_MODULE_5__["inviteToCorporateQuery"],
            variables: {
                userId: userId,
                corporateId: corporateId,
                subjectUsersId: subjectUsersId,
                corporateMemberPositionId: corporateMemberPositionId
            }
        });
    };
    MainService.prototype.setRole = function (role) {
        // Set current user data into observable
        this.currentUserRole.next(role);
    };
    MainService.prototype.getCorporateTiers = function () {
        this.apollo.getClient().cache.reset();
        return this.apollo.query({
            query: _queries_main_quey__WEBPACK_IMPORTED_MODULE_5__["getCorporateTiersQuery"],
        });
    };
    MainService.prototype.insertCorporate = function (userId, InputCorporate) {
        return this.apollo.mutate({
            mutation: _queries_main_quey__WEBPACK_IMPORTED_MODULE_5__["insertCorporateQuery"],
            variables: {
                userId: userId,
                InputCorporate: InputCorporate
            }
        });
    };
    MainService.prototype.updateCorporate = function (userId, corporateId, UpdateCorporate) {
        return this.apollo.mutate({
            mutation: _queries_main_quey__WEBPACK_IMPORTED_MODULE_5__["updateCorporateQuery"],
            variables: {
                userId: userId,
                corporateId: corporateId,
                UpdateCorporate: UpdateCorporate
            }
        });
    };
    MainService.prototype.getOwnedCorporate = function (userId) {
        return this.apollo.query({
            query: _queries_main_quey__WEBPACK_IMPORTED_MODULE_5__["getOwnedCorporateQuery"],
            variables: {
                userId: userId
            }
        });
    };
    MainService.prototype.getUsersCorporateMemberList = function (userId) {
        return this.apollo.query({
            query: _queries_main_quey__WEBPACK_IMPORTED_MODULE_5__["getUsersCorporateMemberListQuery"],
            variables: {
                userId: userId
            }
        });
    };
    MainService.prototype.getCorporate = function (userId, corporateId) {
        return this.apollo.query({
            query: _queries_main_quey__WEBPACK_IMPORTED_MODULE_5__["getCorporateQuery"],
            variables: {
                userId: userId,
                corporateId: corporateId
            }
        });
    };
    MainService.prototype.getSingleCorporateMemberList = function (userId, corporateId) {
        return this.apollo.query({
            query: _queries_main_quey__WEBPACK_IMPORTED_MODULE_5__["getSingleCorporateMemberListQuery"],
            variables: {
                userId: userId,
                corporateId: corporateId
            }
        });
    };
    MainService.prototype.getCorporateTier = function (corporateTierId) {
        return this.apollo.query({
            query: _queries_main_quey__WEBPACK_IMPORTED_MODULE_5__["getCorporateTierQuery"],
            variables: {
                corporateTierId: corporateTierId
            }
        });
    };
    MainService.prototype.joinCorporate = function (joinCorporateValues) {
        return this.apollo.mutate({
            mutation: _queries_main_quey__WEBPACK_IMPORTED_MODULE_5__["joinCorporateQuery"],
            variables: {
                userId: joinCorporateValues.userId,
                corporateCode: joinCorporateValues.corporateCode
            }
        });
    };
    MainService.prototype.getCorporateMemberList = function (userId, corporateId) {
        return this.apollo.query({
            query: _queries_main_quey__WEBPACK_IMPORTED_MODULE_5__["getCorporateMemberListQuery"],
            variables: {
                userId: userId,
                corporateId: corporateId
            }
        });
    };
    MainService.prototype.getCorporateMemberPositions = function () {
        return this.apollo.query({
            query: _queries_main_quey__WEBPACK_IMPORTED_MODULE_5__["getCorporateMemberPositionsQuery"],
        });
    };
    MainService.prototype.changeCorporateMemberPosition = function (userId, corporateMemberListId, corporateMemberPositionId) {
        return this.apollo.mutate({
            mutation: _queries_main_quey__WEBPACK_IMPORTED_MODULE_5__["changeCorporateMemberPositionQuery"],
            variables: {
                userId: userId,
                corporateMemberListId: corporateMemberListId,
                corporateMemberPositionId: corporateMemberPositionId
            }
        });
    };
    MainService.prototype.kickCorporateMemberList = function (userId, corporateMemberListId) {
        return this.apollo.mutate({
            mutation: _queries_main_quey__WEBPACK_IMPORTED_MODULE_5__["kickCorporateMemberListQuery"],
            variables: {
                userId: userId,
                corporateMemberListId: corporateMemberListId,
            }
        });
    };
    MainService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [apollo_angular__WEBPACK_IMPORTED_MODULE_2__["Apollo"]])
    ], MainService);
    return MainService;
}());



/***/ }),

/***/ "./src/app/core/services/pos.service.ts":
/*!**********************************************!*\
  !*** ./src/app/core/services/pos.service.ts ***!
  \**********************************************/
/*! exports provided: PosService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PosService", function() { return PosService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_BehaviorSubject__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/BehaviorSubject */ "./node_modules/rxjs-compat/_esm5/BehaviorSubject.js");



var PosService = /** @class */ (function () {
    function PosService() {
        this.posId = '';
        this.posIdSource = new rxjs_BehaviorSubject__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"](this.posId);
        this.purchaseTypeSource = new rxjs_BehaviorSubject__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"](this.purchaseType);
        this.ticket = TICKET;
        this.ticketSource = new rxjs_BehaviorSubject__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"](this.ticket);
        this.cartTotal = 0;
        this.cartTotalSource = new rxjs_BehaviorSubject__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"](this.cartTotal);
        this.cartNumItems = 0;
        this.cartNumSource = new rxjs_BehaviorSubject__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"](this.cartNumItems);
        this.currentTicket = this.ticketSource.asObservable();
        this.currentTotal = this.cartTotalSource.asObservable();
        this.currentCartNum = this.cartNumSource.asObservable();
        this.currentPosId = this.posIdSource.asObservable();
        this.currentPurchaseType = this.purchaseTypeSource.asObservable();
    }
    PosService.prototype.changeTicket = function (ticket) {
        this.ticketSource.next(ticket);
    };
    PosService.prototype.updateTotal = function (total) {
        this.cartTotalSource.next(total);
    };
    PosService.prototype.updateNumItems = function (num) {
        this.cartNumSource.next(num);
    };
    PosService.prototype.updatePosId = function (id) {
        this.posIdSource.next(id);
    };
    PosService.prototype.updatePurchaseType = function (purchaseType) {
        this.purchaseTypeSource.next(purchaseType);
    };
    PosService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], PosService);
    return PosService;
}());

// Demo content
var TICKET = [];


/***/ }),

/***/ "./src/app/core/services/userService.ts":
/*!**********************************************!*\
  !*** ./src/app/core/services/userService.ts ***!
  \**********************************************/
/*! exports provided: UserAuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserAuthService", function() { return UserAuthService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var apollo_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! apollo-angular */ "./node_modules/apollo-angular/fesm5/ng.apollo.js");
/* harmony import */ var _queries_user_query__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../queries/user.query */ "./src/app/core/queries/user.query.ts");
/* harmony import */ var _jwt_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./jwt.service */ "./src/app/core/services/jwt.service.ts");







var UserAuthService = /** @class */ (function () {
    function UserAuthService(apollo, jwtService) {
        this.apollo = apollo;
        this.jwtService = jwtService;
        this.currentUserSubject = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"]({});
        this.currentUser = this.currentUserSubject.asObservable().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["distinctUntilChanged"])());
        this.isAuthenticatedSubject = new rxjs__WEBPACK_IMPORTED_MODULE_2__["ReplaySubject"](1);
        this.isAuthenticated = this.isAuthenticatedSubject.asObservable();
    }
    UserAuthService.prototype.login = function (loginCredentials) {
        var _this = this;
        return this.apollo.query({
            query: _queries_user_query__WEBPACK_IMPORTED_MODULE_5__["loginQuery"],
            variables: {
                emailOrPhone: loginCredentials.email,
                password: loginCredentials.password
            }
        }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (response) {
            // console.log(response, 'asd wdw')
            if (response.data && !response.errors && response.data.login != null) {
                // localStorage.setItem('currentUser', JSON.stringify(response.data.login))
                _this.setAuth(response.data.login);
            }
            return response;
        }));
    };
    UserAuthService.prototype.register = function (registerCredentials) {
        // console.log(registerCredentials.schoolId);
        this.apollo.getClient().cache.reset();
        return this.apollo.mutate({
            mutation: _queries_user_query__WEBPACK_IMPORTED_MODULE_5__["registerQuery"],
            variables: {
                email: registerCredentials.email,
                password: registerCredentials.password,
                firstName: registerCredentials.firstName,
                lastName: registerCredentials.lastName,
                gender: registerCredentials.gender,
                roleId: registerCredentials.roleId,
                schoolId: registerCredentials.schoolId
            }
        }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (response) {
            console.log(response);
            if (response.data && !response.errors && response.data.login != null) {
                localStorage.setItem('currentUser', JSON.stringify(response.data.login));
            }
            else {
                // console.log(response.data);
                console.log("Error");
            }
        }));
    };
    UserAuthService.prototype.getRole = function () {
        return this.apollo.query({
            query: _queries_user_query__WEBPACK_IMPORTED_MODULE_5__["getRoleQuery"]
        }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (response) {
            return response;
        }));
    };
    UserAuthService.prototype.getUniversity = function () {
        // this.apollo.getClient().cache.reset();
        return this.apollo.query({
            query: _queries_user_query__WEBPACK_IMPORTED_MODULE_5__["getUniversityQuery"]
        }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (response) {
            return response;
        }));
    };
    UserAuthService.prototype.logout = function (userId) {
        // remove user from local storage to log user out
        // window.localStorage.removeItem('jwtToken');
        return this.apollo.mutate({
            mutation: _queries_user_query__WEBPACK_IMPORTED_MODULE_5__["logoutQuery"],
            variables: {
                userId: userId
            }
        }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (response) {
            return response;
        }));
    };
    UserAuthService.prototype.setAuth = function (user) {
        // console.log(user, '<-------------- user');
        // console.log(user.token, '<-------------- user token');
        // window.localStorage.removeItem('semId');
        // window.localStorage.removeItem('toggleVal');
        // Save JWT sent from server in localstorage
        this.jwtService.saveToken(user.token);
        // Set current user data into observable
        this.currentUserSubject.next(user);
        // Set isAuthenticated to true
        this.isAuthenticatedSubject.next(true);
        // console.log(this.currentUserSubject, this.isAuthenticatedSubject, 'COOOOOONNNNNSS');
    };
    UserAuthService.prototype.getCurrentUser = function () {
        return this.currentUserSubject.value;
    };
    UserAuthService.prototype.purgeAuth = function () {
        // Remove JWT from localstorage
        this.jwtService.destroyToken();
        // Set current user to an empty object
        this.currentUserSubject.next({});
        // Set auth status to false
        this.isAuthenticatedSubject.next(false);
    };
    UserAuthService.prototype.populate = function () {
        var _this = this;
        // If JWT detected, attempt to get & store user's info
        if (this.jwtService.getToken()) {
            var token = this.jwtService.getToken();
            this.apollo.getClient().cache.reset();
            this.apollo.query({
                query: _queries_user_query__WEBPACK_IMPORTED_MODULE_5__["getUserDataQuery"],
                variables: {
                    token: token
                }
            }).subscribe(function (response) {
                // console.log(response.data.entry);
                console.log('populate');
                if (response.data.entry == null) {
                    // window.localStorage.removeItem('currentUser');
                    _this.purgeAuth();
                }
                _this.setAuth(response.data.entry);
            }, function (err) { return _this.purgeAuth(); });
        }
        else {
            // Remove any potential remnants of previous auth states
            this.purgeAuth();
        }
    };
    UserAuthService.prototype.updateInformation = function (user, newPassword, oldPassword) {
        return this.apollo.mutate({
            mutation: _queries_user_query__WEBPACK_IMPORTED_MODULE_5__["updateUserData"],
            variables: {
                userId: user._id,
                email: user.email,
                lastName: user.lastname,
                firstName: user.nickname,
                newPassword: newPassword,
                oldPassword: oldPassword
            }
        });
    };
    UserAuthService.prototype.updateUserWithoutPass = function (submissionCredentials, userId) {
        return this.apollo.mutate({
            mutation: _queries_user_query__WEBPACK_IMPORTED_MODULE_5__["updateUserWithoutPassQuery"],
            variables: {
                userId: userId,
                email: submissionCredentials.email,
                nickname: submissionCredentials.nickname,
                lastname: submissionCredentials.lastname,
                schoolId: submissionCredentials.schoolId
            }
        });
    };
    UserAuthService.prototype.sendRecoveryRequest = function (email) {
        return this.apollo.mutate({
            mutation: _queries_user_query__WEBPACK_IMPORTED_MODULE_5__["sendRecoveryRequestQuery"],
            variables: {
                email: email
            }
        });
    };
    UserAuthService.prototype.changePassword = function (token, password) {
        return this.apollo.mutate({
            mutation: _queries_user_query__WEBPACK_IMPORTED_MODULE_5__["recoverChangePasswordQuery"],
            variables: {
                token: token,
                password: password
            }
        });
    };
    UserAuthService.prototype.updateFcm = function (userId, token) {
        return this.apollo.mutate({
            mutation: _queries_user_query__WEBPACK_IMPORTED_MODULE_5__["updateFcmQuery"],
            variables: {
                userId: userId,
                token: token
            }
        });
    };
    UserAuthService.prototype.getClassUserById = function (classUserId) {
        return this.apollo.query({
            query: _queries_user_query__WEBPACK_IMPORTED_MODULE_5__["getClassUserByIdQuery"],
            variables: {
                userId: classUserId
            }
        }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (response) {
            return response;
        }));
    };
    UserAuthService.prototype.getTiers = function () {
        return this.apollo.query({
            query: _queries_user_query__WEBPACK_IMPORTED_MODULE_5__["getTiersQuery"]
        }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (response) {
            return response;
        }));
    };
    UserAuthService.prototype.changeTier = function (userId, tierId) {
        return this.apollo.mutate({
            mutation: _queries_user_query__WEBPACK_IMPORTED_MODULE_5__["changeTierQuery"],
            variables: {
                userId: userId,
                tierId: tierId
            }
        }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (response) {
            return response;
        }));
    };
    UserAuthService.prototype.updatePassword = function (userId, submissionCredentials) {
        return this.apollo.mutate({
            mutation: _queries_user_query__WEBPACK_IMPORTED_MODULE_5__["updatePasswordQuery"],
            variables: {
                userId: userId,
                oldPassword: submissionCredentials.oldPassword,
                newPassword: submissionCredentials.newPassword
            }
        }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (response) {
            return response;
        }));
    };
    UserAuthService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [apollo_angular__WEBPACK_IMPORTED_MODULE_4__["Apollo"],
            _jwt_service__WEBPACK_IMPORTED_MODULE_6__["JwtService"]])
    ], UserAuthService);
    return UserAuthService;
}());



/***/ }),

/***/ "./src/app/graphql.module.ts":
/*!***********************************!*\
  !*** ./src/app/graphql.module.ts ***!
  \***********************************/
/*! exports provided: GraphQLModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GraphQLModule", function() { return GraphQLModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var apollo_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! apollo-angular */ "./node_modules/apollo-angular/fesm5/ng.apollo.js");
/* harmony import */ var apollo_angular_link_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! apollo-angular-link-http */ "./node_modules/apollo-angular-link-http/fesm5/ng.apolloLink.http.js");
/* harmony import */ var apollo_cache_inmemory__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! apollo-cache-inmemory */ "./node_modules/apollo-cache-inmemory/lib/bundle.esm.js");
/* harmony import */ var apollo_link_context__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! apollo-link-context */ "./node_modules/apollo-link-context/lib/bundle.esm.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");







var uri = 'https://mclass.mazzi.mn/backend/graphql'; // <-- add the URL of the GraphQL server here
<<<<<<< HEAD
=======
// https://mclass.mazzi.mn/backend/graphql
// http://www.mazzi.mn:10000/graphql
>>>>>>> cc45d24ef8cf9b8f0c92be78e8f9388edb594c73
// export function createApollo(httpLink: HttpLink) {
//   return {
//     link: httpLink.create({uri}),
//     cache: new InMemoryCache(),
//   };
// }
var GraphQLModule = /** @class */ (function () {
    function GraphQLModule(apollo, httpLink) {
        var http = httpLink.create({ uri: 'https://mclass.mazzi.mn/backend/graphql' });
        var auth = Object(apollo_link_context__WEBPACK_IMPORTED_MODULE_4__["setContext"])(function (_, _a) {
            var headers = _a.headers;
            // get the authentication token from local storage if it exists
            var token = localStorage.getItem('jwtToken');
            // return the headers to the context so httpLink can read them
            // in this example we assume headers property exists
            // and it is an instance of HttpHeaders
            if (!token) {
                return {};
            }
            else {
                return {
                    headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpHeaders"]().set('x-access-token', token)
                };
            }
        });
        var cache = new apollo_cache_inmemory__WEBPACK_IMPORTED_MODULE_3__["InMemoryCache"]({
        // dataIdFromObject: () => // custom idGetter,
        // addTypename: true,
        // cacheResolvers: {},
        // fragmentMatcher: new IntrospectionFragmentMatcher({
        //   introspectionQueryResultData: yourData
        // }),
        });
        // apollo.create({
        //   link: auth.concat(http),
        //   cache: new InMemoryCache({
        //     dataIdFromObject: o => o.id
        //   })
        // });
        // const client = new ApolloClient({
        //   cache: new InMemoryCache(),
        //   link: auth.concat(http),
        // })
        apollo.create({
            link: auth.concat(http),
            cache: cache,
        });
    }
    GraphQLModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_5__["NgModule"])({
            exports: [_angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClientModule"], apollo_angular__WEBPACK_IMPORTED_MODULE_1__["ApolloModule"], apollo_angular_link_http__WEBPACK_IMPORTED_MODULE_2__["HttpLinkModule"]]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [apollo_angular__WEBPACK_IMPORTED_MODULE_1__["Apollo"], apollo_angular_link_http__WEBPACK_IMPORTED_MODULE_2__["HttpLink"]])
    ], GraphQLModule);
    return GraphQLModule;
}());



/***/ }),

/***/ "./src/app/shared/layouts/dashboard-footer/dashboard-footer.component.css":
/*!********************************************************************************!*\
  !*** ./src/app/shared/layouts/dashboard-footer/dashboard-footer.component.css ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9sYXlvdXRzL2Rhc2hib2FyZC1mb290ZXIvZGFzaGJvYXJkLWZvb3Rlci5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/shared/layouts/dashboard-footer/dashboard-footer.component.html":
/*!*********************************************************************************!*\
  !*** ./src/app/shared/layouts/dashboard-footer/dashboard-footer.component.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<footer class=\"content-footer\">\r\n    <div class=\"footer\">\r\n        <div class=\"copyright\">\r\n            <span>Зохиогчийн эрх хуулиар хамгаалагдсан ©2018 <b class=\"text-dark\">Маззи</b></span>\r\n            <span class=\"go-right\">\r\n  <!-- <a href=\"\" class=\"text-gray mrg-right-15\">Term &amp; Conditions</a>\r\n  <a href=\"\" class=\"text-gray\">Privacy &amp; Policy</a> -->\r\n</span>\r\n        </div>\r\n    </div>\r\n</footer>"

/***/ }),

/***/ "./src/app/shared/layouts/dashboard-footer/dashboard-footer.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/shared/layouts/dashboard-footer/dashboard-footer.component.ts ***!
  \*******************************************************************************/
/*! exports provided: DashboardFooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardFooterComponent", function() { return DashboardFooterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var DashboardFooterComponent = /** @class */ (function () {
    function DashboardFooterComponent() {
    }
    DashboardFooterComponent.prototype.ngOnInit = function () {
    };
    DashboardFooterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-dashboard-footer',
            template: __webpack_require__(/*! ./dashboard-footer.component.html */ "./src/app/shared/layouts/dashboard-footer/dashboard-footer.component.html"),
            styles: [__webpack_require__(/*! ./dashboard-footer.component.css */ "./src/app/shared/layouts/dashboard-footer/dashboard-footer.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], DashboardFooterComponent);
    return DashboardFooterComponent;
}());



/***/ }),

/***/ "./src/app/shared/layouts/dashboard-header/dashboard-header.component.css":
/*!********************************************************************************!*\
  !*** ./src/app/shared/layouts/dashboard-header/dashboard-header.component.css ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".navbar {\r\n    min-height: 3rem;\r\n    height: 8vh;\r\n    background-color: #212121;\r\n    justify-content: space-between;\r\n  }\r\n  \r\n  .title {\r\n    color: white;\r\n    margin-right: 0.25rem;\r\n    font-size: 1.25rem;\r\n    cursor: pointer;\r\n  }\r\n  \r\n  .subtitle {\r\n    color: gray;\r\n  }\r\n  \r\n  .menu {\r\n    color: white;\r\n  }\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkL2xheW91dHMvZGFzaGJvYXJkLWhlYWRlci9kYXNoYm9hcmQtaGVhZGVyLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxnQkFBZ0I7SUFDaEIsV0FBVztJQUNYLHlCQUF5QjtJQUN6Qiw4QkFBOEI7RUFDaEM7O0VBRUE7SUFDRSxZQUFZO0lBQ1oscUJBQXFCO0lBQ3JCLGtCQUFrQjtJQUNsQixlQUFlO0VBQ2pCOztFQUVBO0lBQ0UsV0FBVztFQUNiOztFQUVBO0lBQ0UsWUFBWTtFQUNkIiwiZmlsZSI6InNyYy9hcHAvc2hhcmVkL2xheW91dHMvZGFzaGJvYXJkLWhlYWRlci9kYXNoYm9hcmQtaGVhZGVyLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubmF2YmFyIHtcclxuICAgIG1pbi1oZWlnaHQ6IDNyZW07XHJcbiAgICBoZWlnaHQ6IDh2aDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICMyMTIxMjE7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgfVxyXG4gIFxyXG4gIC50aXRsZSB7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDAuMjVyZW07XHJcbiAgICBmb250LXNpemU6IDEuMjVyZW07XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgfVxyXG4gIFxyXG4gIC5zdWJ0aXRsZSB7XHJcbiAgICBjb2xvcjogZ3JheTtcclxuICB9XHJcbiAgXHJcbiAgLm1lbnUge1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gIH0iXX0= */"

/***/ }),

/***/ "./src/app/shared/layouts/dashboard-header/dashboard-header.component.html":
/*!*********************************************************************************!*\
  !*** ./src/app/shared/layouts/dashboard-header/dashboard-header.component.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div color=\"primary\" class=\"navbar flex flex-direction-row align-items-center\">\r\n        <div class=\"flex flex-direction-column titles\">\r\n          <h4 routerLink=\"/welcome\" class=\"title add-margin\">Mazzi POS</h4>\r\n          <h6 class=\"subtitle add-margin\">{{ currentUser.lastname }} {{ currentUser.nickname }}</h6>\r\n        </div>\r\n      \r\n        <button mat-icon-button [matMenuTriggerFor]=\"menu\" class=\"menu\">\r\n          <mat-icon>more_vert</mat-icon>\r\n        </button>\r\n      </div>\r\n      \r\n      <!-- Mobile Menu -->\r\n      <mat-menu #menu=\"matMenu\">\r\n        <!-- <button mat-menu-item routerLink=\"/home\">\r\n          <mat-icon>shopping_cart</mat-icon>\r\n          <span>Point of Sale</span>\r\n        </button> -->\r\n        <!-- <button mat-menu-item routerLink=\"/admin/items\">\r\n          <mat-icon>perm_identity</mat-icon>\r\n          <span>Admin</span>\r\n        </button>\r\n        <button mat-menu-item routerLink=\"/reports\">\r\n          <mat-icon>multiline_chart</mat-icon>\r\n          <span>Reports</span>\r\n        </button> -->\r\n        <button mat-menu-item (click)=\"logout()\">\r\n          <mat-icon>exit_to_app</mat-icon>\r\n          <span>Logout</span>\r\n        </button>\r\n      </mat-menu>"

/***/ }),

/***/ "./src/app/shared/layouts/dashboard-header/dashboard-header.component.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/shared/layouts/dashboard-header/dashboard-header.component.ts ***!
  \*******************************************************************************/
/*! exports provided: DashboardHeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardHeaderComponent", function() { return DashboardHeaderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_core_services_userService__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/core/services/userService */ "./src/app/core/services/userService.ts");




var DashboardHeaderComponent = /** @class */ (function () {
    function DashboardHeaderComponent(userService, router, route, cdRef) {
        var _this = this;
        this.userService = userService;
        this.router = router;
        this.route = route;
        this.cdRef = cdRef;
        this.notifications = [];
        this.firebaseNotif = [];
        this.message = [];
        this.newNotificationCount = 0;
        this.skip = 0;
        this.limit = 20;
        this.notificationCountSum = 0;
        // dateNow;
        this.allNotifications = [];
        // this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.userService.currentUser.subscribe(function (userData) {
            _this.currentUser = userData;
            // this.notificationCountSum = userData.subUser.notification.mclassNotificationCount;
        });
        // this.getNotifications();
    }
    DashboardHeaderComponent.prototype.ngOnInit = function () {
        this.classId = this.route.snapshot.paramMap.get('_id');
        // this.newNotificationCount = this.currentUser.subUser.notification.mclassNotificationCount;
        console.log(this.currentUser._id, 'currentUserrrr');
    };
    DashboardHeaderComponent.prototype.refresh = function () {
        console.log('refresh', this.allNotifications);
        console.log('notification', this.notifications);
        // this.cdRef.detectChanges();
        // this.cdRef.markForCheck();
    };
    // clearNotification(){
    //   this.allNotifications = []
    // }
    DashboardHeaderComponent.prototype.ngOnChanges = function () {
    };
    DashboardHeaderComponent.prototype.ngAfterViewInit = function () {
        //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
        //Add 'implements AfterViewInit' to the class.
        this.cdRef.detectChanges();
    };
    DashboardHeaderComponent.prototype.ngAfterViewChecked = function () {
        // console.log("! changement de la date du composant !");
        // this.dateNow = new Date();
        this.cdRef.detectChanges();
    };
    DashboardHeaderComponent.prototype.logout = function () {
        var _this = this;
        console.log("logout called");
        this.userService.logout(this.currentUser._id).subscribe(function (response) {
            if (response.data.logout.error == false) {
                _this.userService.purgeAuth();
                _this.router.navigateByUrl('/login');
            }
        });
    };
    DashboardHeaderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-dashboard-header',
            template: __webpack_require__(/*! ./dashboard-header.component.html */ "./src/app/shared/layouts/dashboard-header/dashboard-header.component.html"),
            styles: [__webpack_require__(/*! ./dashboard-header.component.css */ "./src/app/shared/layouts/dashboard-header/dashboard-header.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_core_services_userService__WEBPACK_IMPORTED_MODULE_3__["UserAuthService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"]])
    ], DashboardHeaderComponent);
    return DashboardHeaderComponent;
}());



/***/ }),

/***/ "./src/app/shared/not-found/not-found/not-found.component.css":
/*!********************************************************************!*\
  !*** ./src/app/shared/not-found/not-found/not-found.component.css ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9ub3QtZm91bmQvbm90LWZvdW5kL25vdC1mb3VuZC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/shared/not-found/not-found/not-found.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/shared/not-found/not-found/not-found.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"authentication\">\r\n    <div class=\"page-404 container\">\r\n      <div class=\"row\">\r\n        <div class=\"col-md-1\">\r\n          <div class=\"full-height\">\r\n            <div class=\"vertical-align full-height pdd-horizon-10\">\r\n              <div class=\"table-cell\">\r\n                <h1 class=\"text-dark font-size-80 text-light\">404!</h1>\r\n                <p class=\"lead lh-1-8\">Таны бичсэн хаяг байхгүй байна.<br></p>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"col-md-9 ml-auto hidden-sm hidden-xs\">\r\n          <div class=\"full-height height-100\">\r\n            <div class=\"vertical-align full-height\">\r\n              <div class=\"table-cell\">\r\n                <img class=\"img-responsive\" src=\"assets/images/404.png\" alt=\"\">\r\n              </div>\r\n            </div>\r\n          </div>\t\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>"

/***/ }),

/***/ "./src/app/shared/not-found/not-found/not-found.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/shared/not-found/not-found/not-found.component.ts ***!
  \*******************************************************************/
/*! exports provided: NotFoundComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotFoundComponent", function() { return NotFoundComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var NotFoundComponent = /** @class */ (function () {
    function NotFoundComponent() {
    }
    NotFoundComponent.prototype.ngOnInit = function () {
    };
    NotFoundComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-not-found',
            template: __webpack_require__(/*! ./not-found.component.html */ "./src/app/shared/not-found/not-found/not-found.component.html"),
            styles: [__webpack_require__(/*! ./not-found.component.css */ "./src/app/shared/not-found/not-found/not-found.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], NotFoundComponent);
    return NotFoundComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! hammerjs */ "./node_modules/hammerjs/hammer.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_4__);





if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\pc\web_pos\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map