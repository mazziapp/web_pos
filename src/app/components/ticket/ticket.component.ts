import { Component, OnInit, Inject } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { PosItem } from 'src/app/core/models/posItem';
import { PosService } from 'src/app/core/services/pos.service';
import gql from 'graphql-tag';
import { insertBillMutation } from 'src/app/core/queries/pos.query';
import { UserAuthService } from 'src/app/core/services/userService';
import { User } from 'src/app/core/models/user.model';
import { useAnimation } from '@angular/animations';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatTableDataSource } from '@angular/material';
import { ActivatedRoute } from '@angular/router';

export interface DialogData {
  animal: string;
  name: string;
}

@Component({
  selector: 'app-ticket',
  templateUrl: './ticket.component.html',
  styleUrls: ['./ticket.component.css']
})


export class TicketComponent implements OnInit {
  userId = '5c4936057565dd0012a958f6';
  corporateId = '5ccaa08ab545031b446629af';
  userName = "Davka";
  currentUser: User;
  ticket: PosItem[] = [];
  currentCorporateId;
  cartTotal = 0;
  cartNumItems = 0;
  inputBillItems: any[] = [];

  dataSource: MatTableDataSource<PosItem>;
  displayedColumns = ['quantity', 'name', 'price'];
  constructor(private apollo: Apollo, private ticketSync: PosService, private userService: UserAuthService, public dialog: MatDialog, private route: ActivatedRoute) {
    this.userService.currentUser.subscribe(
      (userData) => {
        this.currentUser = userData;
      }
    );
    this.dataSource = new MatTableDataSource<PosItem>();
  }

  // Sync with ticketSync service on init
  ngOnInit() {
    this.ticketSync.currentTicket.subscribe(data => this.ticket = data);
    this.ticketSync.currentTotal.subscribe(total => this.cartTotal = total);
    this.ticketSync.currentCartNum.subscribe(num => this.cartNumItems = num);
    console.log("userId = ", this.currentUser._id);

  }

  // Add item to ticket.
  addItem(item: PosItem) {
    // If the item already exists, add 1 to quantity
    console.log(item, 'item')
    console.log(this.ticket)
    let index = this.ticket.findIndex(x => x._id == item._id);
    if (index != -1) {
      this.ticket[index].quantity = this.ticket[index].quantity + 1;
    } else {
      this.ticket.push(item);
      this.dataSource.data.push(item);
    }

    this.syncTicket();
    this.calculateTotal();

    // if (this.ticket.includes(item)) {
    //   this.ticket[this.ticket.indexOf(item)].quantity = 1;
    // } else {
    //   this.ticket.push(item);
    //   this.dataSource.data.push(item);
    // }
    // this.syncTicket();
    // this.calculateTotal();
  }

  // Remove item from ticket
  removeItem(item: PosItem) {
    // Check if item is in array
    let index = this.ticket.findIndex(x => x._id == item._id);

    if (index != -1) {
      this.ticket[this.ticket.indexOf(item)].quantity = 1;
      this.ticket.splice(index, 1);
    }

    this.syncTicket();
    this.calculateTotal();

    // if (this.ticket.includes(item)) {
    //   // Splice the element out of the array
    //   const index = this.ticket.indexOf(item);
    //   if (index > -1) {
    //     // Set item quantity back to 1 (thus when readded, quantity isn't 0)
    //     this.ticket[this.ticket.indexOf(item)].quantity = 1;
    //     this.ticket.splice(index, 1);
    //   }
    // }
    // this.syncTicket();
    // this.calculateTotal();
  }

  // Reduce quantity by one
  subtractOne(item: PosItem) {
    // Check if last item, if so, use remove method
    if (this.ticket[this.ticket.indexOf(item)].quantity === 1) {
      this.removeItem(item);
    } else {
      this.ticket[this.ticket.indexOf(item)].quantity = this.ticket[this.ticket.indexOf(item)].quantity - 1;
    }
    this.syncTicket();
    this.calculateTotal();
  }

  // Calculate cart total
  calculateTotal() {
    let total = 0;
    let cartitems = 0;
    // Multiply item price by item quantity, add to total
    this.ticket.forEach(function (item: PosItem) {
      total += (item.price * item.quantity);
      cartitems += item.quantity;
    });
    this.cartTotal = total;
    this.cartNumItems = cartitems;
    // Sync total with ticketSync service.
    this.ticketSync.updateNumItems(this.cartNumItems);
    this.ticketSync.updateTotal(this.cartTotal);
  }

  // Remove all items from cart
  clearCart() {
    console.log('clear called');
    this.inputBillItems = [];
    // Reduce back to initial quantity (1 vs 0 for re-add)
    this.ticket.forEach(function (item: PosItem) {
      item.quantity = 1;
    });
    // Empty local ticket variable then sync
    this.ticket = [];
    this.syncTicket();
    this.calculateTotal();
  }

  syncTicket() {
    this.ticketSync.changeTicket(this.ticket);
  }

  checkout() {
    if (this.ticket.length > 0) {
      console.log('ticket length = ' + this.ticket.length);
      for (let i = 0; i < this.ticket.length; i++) {
        this.inputBillItems.push({ posItemId: this.ticket[i]._id, quantity: this.ticket[i].quantity });
        console.log('posItemId =', this.ticket[i]._id + " quantity =" + this.ticket[i].quantity);
      }
      this.insertBill(this.inputBillItems);
      this.clearCart();
    }
  }

  insertBill(inputBillItems) {
    this.currentCorporateId = this.route.snapshot.paramMap.get('_id');

    this.apollo.mutate({
      mutation: insertBillMutation,
      variables: {
        userId: this.currentUser._id,
        corporateId: this.currentCorporateId,
        inputBillItems: inputBillItems
      }
    }).subscribe(({ data }) => {
      console.log('got dataaaaa ', data);
    }, (error) => {
      console.log('failed', error);
    });
  }

  // openDialog(): void {
  //   const dialogRef = this.dialog.open(CheckoutDialog, {
  //     width: '500px',

  //   });

  //   dialogRef.afterClosed().subscribe(result => {
  //     console.log('The dialog was closed');
  //     this.checkout();
  //   });
  // }

}


