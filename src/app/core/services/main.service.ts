import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { BehaviorSubject } from 'rxjs';
import { distinctUntilChanged } from 'rxjs/operators';
import { searchUserByPhoneQuery, inviteToCorporateQuery, getCorporateTiersQuery, insertCorporateQuery, updateCorporateQuery, getOwnedCorporateQuery, getUsersCorporateMemberListQuery, getCorporateQuery, getSingleCorporateMemberListQuery, getCorporateTierQuery, joinCorporateQuery, getCorporateMemberListQuery, getCorporateMemberPositionsQuery, changeCorporateMemberPositionQuery, kickCorporateMemberListQuery } from '../queries/main.quey';


@Injectable()
export class MainService {
    private currentUserRole = new BehaviorSubject<any>({} as any);
    public currentRole = this.currentUserRole.asObservable().pipe(distinctUntilChanged());

    constructor(
        private apollo: Apollo
    ) {}

    searchUserByPhone(userId, phone) {
        return this.apollo.query<any>({
            query: searchUserByPhoneQuery,
            variables: {
                userId: userId,
                phone: phone
            }
        })
    }

    inviteToCorporate(userId, corporateId, subjectUsersId, corporateMemberPositionId) {
        return this.apollo.mutate({
            mutation: inviteToCorporateQuery,
            variables: {
                userId: userId,
                corporateId: corporateId,
                subjectUsersId: subjectUsersId,
                corporateMemberPositionId: corporateMemberPositionId
            }
        })
    }

    setRole(role: any) {
        // Set current user data into observable
        this.currentUserRole.next(role);
    }

    getCorporateTiers() {
        this.apollo.getClient().cache.reset();
        return this.apollo.query<any>({
            query: getCorporateTiersQuery,
        })    
    }

    insertCorporate(userId, InputCorporate) {
        return this.apollo.mutate({
            mutation: insertCorporateQuery,
            variables: {
                userId: userId,
                InputCorporate: InputCorporate
            }
        })
    }

    updateCorporate(userId, corporateId, UpdateCorporate) {
        return this.apollo.mutate({
            mutation: updateCorporateQuery,
            variables: {
                userId: userId,
                corporateId: corporateId,
                UpdateCorporate: UpdateCorporate
            }
        })
    }

    getOwnedCorporate(userId) {
        return this.apollo.query<any>({
            query: getOwnedCorporateQuery,
            variables: {
               userId: userId 
            }
        })
    }

    getUsersCorporateMemberList(userId) {
        return this.apollo.query<any>({
            query: getUsersCorporateMemberListQuery,
            variables: {
                userId: userId
            }
        })
    }

    getCorporate(userId, corporateId) {
        return this.apollo.query<any>({
            query: getCorporateQuery,
            variables: {
                userId: userId,
                corporateId: corporateId
            }
        })
    }

    getSingleCorporateMemberList(userId, corporateId) {
        return this.apollo.query<any>({
            query: getSingleCorporateMemberListQuery,
            variables: {
                userId: userId,
                corporateId: corporateId
            }
        })
    }

    getCorporateTier(corporateTierId) {
        return this.apollo.query<any>({
            query: getCorporateTierQuery,
            variables: {
                corporateTierId: corporateTierId
            }
        })
    }

    joinCorporate(joinCorporateValues) {
        return this.apollo.mutate({
            mutation: joinCorporateQuery,
            variables: {
                userId: joinCorporateValues.userId,
                corporateCode: joinCorporateValues.corporateCode
            }
        })
    }

    getCorporateMemberList(userId, corporateId) {
        return this.apollo.query<any>({
            query: getCorporateMemberListQuery,
            variables: {
                userId: userId,
                corporateId: corporateId
            }
        })
    }

    getCorporateMemberPositions() {
        return this.apollo.query<any>({
            query: getCorporateMemberPositionsQuery,
        })
    }

    changeCorporateMemberPosition(userId, corporateMemberListId, corporateMemberPositionId) {
        return this.apollo.mutate({
            mutation: changeCorporateMemberPositionQuery,
            variables: {
                userId: userId,
                corporateMemberListId: corporateMemberListId,
                corporateMemberPositionId: corporateMemberPositionId
            }
        })
    }

    kickCorporateMemberList(userId, corporateMemberListId) {
        return this.apollo.mutate({
            mutation: kickCorporateMemberListQuery,
            variables: {
                userId: userId,
                corporateMemberListId: corporateMemberListId,
            }
        })        
    }

}


